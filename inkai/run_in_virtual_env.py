#!/bin/env python
# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This is the Inkscape entry point using a virtual environment installed by poetry.

Note that we can not import anything because we might be in the wrong interpreter.

Look for
- poetry
- .tox
- .venv
"""
import os
import sys
import subprocess


def main():
    """Run this in a virtual environment."""
    p = lambda *args: print(*args, file=sys.stderr)

    HERE = os.path.dirname(__file__) or "."
    # We move so we can execute the module
    CWD = os.path.join(HERE, "..")

    # Generate all possible enviroments
    VENVS = [".pyenv"]
    for n in (7, 8, 9, 10):
        VENVS.append(os.path.join(f".tox", f"py3{n}"))
        VENVS.append(os.path.join(f".tox", f"py3{n}-normal"))

    VENVS = [os.path.join(CWD, env, "bin", "python") for env in VENVS]

    # Check that we do not run into an endless loop
    if sys.executable in VENVS:
        p("We cannot start a subprocess again and again.")
        from inkai.cmd import main

        exit(1)

    p("Checking virtual environments:")
    for env in VENVS:
        p(f"\t{env}")

    VENVS = [env for env in VENVS if os.path.isfile(env)]

    ERROR = """
    If you have the latest version, you might be in luck.
    If not, I recommend following the development setup.
    
        https://gitlab.com/inkscape/extras/extension-ai#development-setup
    
    """
    if VENVS:
        p("Found virtual environments:")
        for env in VENVS:
            p(f"\t{env}")
    else:
        p("No virtual environments installed with tox or virtualenv.")

    if VENVS:
        interpreter = [VENVS[0]]
        p("I will use a virtual environment.")
    elif subprocess.call(["poetry", "--version"], stdout=sys.stderr) == 0:
        interpreter = ["poetry", "run", "python"]
        p("I will poetry. Seems like you used the development setup.")
    else:
        try:
            import inkex
        except ImportError:
            p("Sorry, inkex is required to run this file. Exiting." + ERROR)
            exit(1)
        else:
            inkex.errormsg(
                "I will use the python packages that Inkscape provides." + ERROR
            )
        interpreter = [sys.executable]

    command = interpreter + ["-m", "inkai"] + sys.argv[1:]
    p("Running$ '" + "' '".join(command) + "'")
    exit(subprocess.call(command, cwd=CWD))


if __name__ == "__main__":
    main()
