# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the AI file's Setup Section.

This section contains definitions and meta data that
is reused within the document.
"""
from __future__ import annotations
from typing import List, Dict

from inkai.parser.lazy import AIElement
from .lazy import (
    CollectedSection,
    StartOfLineSection,
)
from .data_hierarchy import DataHierarchyParser


class ArtStyle(CollectedSection):
    """This is not described in the AI Spec.

    The work in this one is reverse-engineered.
    """

    spec = StartOfLineSection("%AI9_BeginArtStyles", "%AI9_EndArtStyles")

    def _parse(self, parser) -> dict:
        """Parse all the content."""
        return DataHierarchyParser.parse_lines(self.lines, all=True)

    @property
    def children(self) -> List[AIElement]:
        """I do not have any children."""
        return []
