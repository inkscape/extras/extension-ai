# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the AI file header comments.


Example:

    %!PS-Adobe-3.0 
    %%Creator: Adobe Illustrator(R) 24.0
    %%AI8_CreatorVersion: 27.1.1
    %%For: (Christopher Rogers) ()
    %%Title: (cmyk_rectangle.ai)
    %%CreationDate: 3/13/2023 7:57 PM
    %%Canvassize: 16383
    %%BoundingBox: 202 412 384 625
    %%HiResBoundingBox: 202.5 412.5 383.5 624.5
    %%DocumentProcessColors: Cyan Magenta Yellow Black
    %AI5_FileFormat 14.0
    %AI12_BuildNumber: 196
    %AI3_ColorUsage: Color
    %AI7_ImageSettings: 0
    %%RGBProcessColor: 0.000136697562994 0.000626976019703 0.000423457007855 ([Registration])
    %AI3_Cropmarks: 0 0 595.275573730469 841.889770507813
    %AI3_TemplateBox: 296.5 420.5 296.5 420.5
    %AI3_TileBox: 0 -0.03021240234375 595.320007324219 841.889770507813
    %AI3_DocumentPreview: None
    %AI5_ArtSize: 14400 14400
    %AI5_RulerUnits: 1
    %AI24_LargeCanvasScale: 1
    %AI9_ColorModel: 1
    %AI5_ArtFlags: 0 0 0 1 0 0 1 0 0
    %AI5_TargetResolution: 800
    %AI5_NumLayers: 1
    %AI17_Begin_Content_if_version_gt:24 4
    %AI10_OpenToVie: -646 893 1 0 0 0 3238 1903 18 0 0 89 170 0 0 0 1 1 0 1 1 0 1
    %AI17_Alternate_Content
    %AI9_OpenToView: -646 893 1 3238 1903 18 0 0 89 170 0 0 0 1 1 0 1 1 0 1
    %AI17_End_Versioned_Content
    %AI5_OpenViewLayers: 7
    %AI17_Begin_Content_if_version_gt:24 4
    %AI17_Alternate_Content
    %AI17_End_Versioned_Content
    %%PageOrigin:-663 -120
    %AI7_GridSettings: 72 8 72 8 1 0 0.800000011920929 0.800000011920929 0.800000011920929 0.899999976158142 0.899999976158142 0.899999976158142
    %AI9_Flatten: 1
    %AI12_CMSettings: 00.MS
    %%EndComments
"""
from typing import List, Dict, Tuple, Optional
from .lazy import (
    CollectedSection,
    SectionWithEnd,
    CommandParser,
    UnidentifiedLine,
    AIElement,
)
import re
from .post_script import find_all
from .lines import Lines
from collections import namedtuple
from .units import header_comments_to_units
from .data_hierarchy import Point
from .data_section import Data

TemplateBox = namedtuple("TemplateBox", ("left", "bottom", "right", "top"))
Cropmarks = namedtuple("Cropmarks", ("left", "bottom", "right", "top"))
ArtSize = namedtuple("ArtSize", ("width", "height"))

HEADER_KEY_REGEX = re.compile(":| ")
UNPROCESSED_HEADER_COMMENTS_KEY = "unprocessed-header-comments"
END_COMMENTS = "%%EndComments"
DSC = "dsc"


class HeaderComments(CollectedSection):
    """Parse the header comments.

        %!PS-Adobe-3.0
        ...
        %%EndComments

    For more information, see the AI Spec page 15+.
    """

    spec = SectionWithEnd(END_COMMENTS)

    @classmethod
    def from_lines(cls, lines: Lines):
        """Create the header comments from lines."""
        dsc = lines.read_line()
        return cls(lines)

    def _parse(self, parser: CommandParser) -> dict:
        """Return the whole header comments as a dict."""
        unprocessed: List[str] = []
        result: Dict[str, str] = {DSC: self.lines.current_line}
        key: str
        while not self.lines.reached_end():
            line = self.lines.read_line()
            kv = HEADER_KEY_REGEX.split(line, 1)
            if kv[0] == "%%+":
                # key is the same
                value = kv[1]
                result[key] += "\n" + value
                continue
            if kv[0] == END_COMMENTS:
                break
            if len(kv) == 1:
                unprocessed += kv
                continue
            key, value = kv
            key = key.lstrip("%")
            key = key[key.find("_") + 1 :]
            if key == "BeginData":
                self.lines.insert_next_line(line)
                self._data = Data.from_lines(self.lines)
                continue
            result[key] = value.strip()
        result[UNPROCESSED_HEADER_COMMENTS_KEY] = "\n".join(unprocessed)
        return result

    @property
    def creator(self) -> str:
        """The Adobe Illustrator version name.

        AI Spec page 15:

            The %%Creator comment identifies the application that generated the
            PostScript language document. The version number (version 6.0 in Figure 1)
            is arbitrary text, terminated by a newline character.
        """
        return self.parsed_content.get("Creator", "")

    @property
    def parsed_content(self) -> Dict[str, str]:
        """The parsed header comments."""
        return super().parsed_content  # type: ignore

    @property
    def creator_version(self) -> str:
        """The Adobe Illustrator version or empty string before AI8."""
        return self.parsed_content.get("CreatorVersion", "")

    @property
    def bounding_box(self) -> List[int]:
        """The bounding box of the document.

        -> [lower_left_x, lower_left_y, top_right_x, top_right_y]

        AI Spec page 16:

            [Required] The %%BoundingBox comment specifies the imaginary box that
            encloses all marks painted on the page. Specify the integer coordinates in the
            default user coordinate system. Negative numbers are allowed.
            If a high-resolution bounding box is specified (as described below), the limits
            of the bounding box are derived from the high-resolution bounding box. To
            generate the lower left and upper right limits of the bounding box, the
            high-resolution bounding box llx and lly values are rounded down, and urx and
            ury values are rounded up.
        """
        return self._int_list("BoundingBox")

    def _int_list(self, key: str) -> List[int]:
        """Return value as a list of ints."""
        return list(map(int, self.parsed_content[key].split()))

    @property
    def canvas_size(self) -> int:
        """The size of the AI canvas.

        See docs/specification_amendments/units.md

        -1 if it is not specified.
        """
        return int(self.parsed_content.get("Canvassize", -1))

    @property
    def unprocessed_header_comments(self) -> List[str]:
        """This is a list of header comments that were not processed."""
        unprocessed = self.parsed_content[UNPROCESSED_HEADER_COMMENTS_KEY]
        if unprocessed:
            return unprocessed.split("\n")
        return []

    @property
    def title(self):
        """The title of the document."""
        return find_all(self.parsed_content.get("Title", "()"))[0]

    @property
    def file_format(self) -> str:
        """The version of the file format or an empty string."""
        return self.parsed_content.get("FileFormat", "")

    @property
    def children(self) -> List[AIElement]:
        """All the unidentified lines."""
        return [
            UnidentifiedLine.from_string(line)
            for line in self.unprocessed_header_comments
        ]

    @property
    def large_canvas_scale(self) -> float:
        """The LargeCanvasScale comment, defaults to 1.

        See docs/specification_amendments/units.md
        """
        return float(self.parsed_content.get("LargeCanvasScale", 1))

    @property
    def template_box(self) -> TemplateBox:
        """The template box of the document. (llx, lly, urx, ury)

        See page 21 AI Spec.
        """
        return TemplateBox(*map(float, self.parsed_content["TemplateBox"].split()))

    @property
    def cropmarks(self) -> Optional[Cropmarks]:
        """The template box of the document. (llx, lly, urx, ury)

        See page 21 AI Spec.
        """
        if "Cropmarks" in self.parsed_content:
            return Cropmarks(*map(float, self.parsed_content["Cropmarks"].split()))
        return None

    @property
    def art_size(self) -> Optional[ArtSize]:
        """This comment specifies the size of the artboard in points. (width, height)"""
        artsize = self.parsed_content.get("ArtSize")
        return None if artsize is None else ArtSize(*map(float, artsize.split()))

    @property
    def page_origin(self) -> Optional[Point]:
        """The PageOrigin header comment."""
        page_origin = self.parsed_content.get("PageOrigin")
        return None if page_origin is None else Point(*map(float, page_origin.split()))

    @property
    def tile_box(self) -> Optional[TemplateBox]:
        """The AI3_TileBox header comment."""
        tile_box = self.parsed_content.get("TileBox")
        return None if tile_box is None else TemplateBox(*map(float, tile_box.split()))

    @property
    def units(self) -> str:
        """The units of this document.

        cm, mm, px, ...

        See docs/specification_amendments/units.md
        """
        ai5 = self.parsed_content.get("RulerUnits", "")
        ai24 = self.parsed_content.get("RulerUnitsLarge", "")
        return header_comments_to_units(ai5, ai24)

    @property
    def data(self) -> Optional[Data]:
        """Binary data if it is present."""
        self.parsed_content
        return self._data


__all__ = ["HeaderComments", "ArtSize", "TemplateBox"]
