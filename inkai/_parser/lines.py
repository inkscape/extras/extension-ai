# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Parse line by line.

A string is split up in lines and you can access for each line:
- the content
- the line number
- the line end (\\r, \\n, \\r\\n)

Also, the data line "%%BeginData: <int>" PostScript directive is considered:
Content after a data line - although it might contain newlines - is not split
into lines. Instead, it is returned as one line.
"""

from __future__ import annotations
from .error import ReachedEndOfFile
import io
from typing import List, Tuple, Callable, Sequence
from abc import ABC, abstractmethod

UntilType = Callable[[str], bool]
LineType = Tuple[str, str, int]  # line, newline, newline_count
FIRST_LINE: LineType = ("", "", 1)


class Lines(ABC):
    """An interface for parsing files line by line.

    You will probably use the LineParser.

    Instance attributes:

    - current_line
    - current_line_end
    - current_line_number
    """

    def __init__(self, current_line_number=0, current_line=FIRST_LINE):
        """Create new Lines."""
        self._has_read_a_line = False
        # stack of line in front of all other lines
        self._next_lines = []
        self._last_line: LineType = FIRST_LINE
        self._current_line = current_line
        self._current_line_number = current_line_number

    @property
    def current_line(self) -> str:
        """The current line we are in."""
        return self._current_line[0]

    @property
    def current_line_end(self) -> str:
        """The end of the current line.

        Should be either '\\r', '\\n' or '\\r\\n'.
        """
        return self._current_line[1]

    @property
    def current_line_number(self) -> int:
        """The number of the current line.

        The number is 0 if no line has been read from the file.
        The first line has number 1.
        """
        return self._current_line_number

    @abstractmethod
    def _read_line(self) -> LineType:
        """Read a line.

        Returns (line, newline)
        """

    def read_line(self) -> str:
        """Read one line.

        This sets:
        - current_line
        - current_line_end
        - current_line_number

        This raises ReachedEndOfFile error if you did not check that lines.reached_end().
        """
        last_line = self._current_line
        if self._next_lines:
            self._current_line = self._next_lines.pop()
        else:
            self._current_line = self._read_line()
        self._current_line_number += last_line[2]
        self._has_read_a_line = True
        self._last_line = last_line
        return self.current_line

    def has_read_a_line(self) -> bool:
        """Whether there is a line that was read.

        If a line was read, these attributes have a meaning:
        - current_line
        - current_line_end
        - current_line_number

        After the object is created, no line has been read.
        If you use read_line() and there are lines, this is True.
        """
        return self._has_read_a_line

    def collect_until(
        self,
        until: UntilType,
        start_at_current_line=False,
        include_last_tested_line=False,
        continue_after_last_tested_line=True,
    ) -> Lines:
        """Collect all lines until(line) is True.

        until(line) will run on all following lines but not in the current_line.

        Use start_at_current_line add the current_line as the first line of the collected lines.
        Use include_last_tested_line to determine if the last line should be included in the result.
        If you continue_after_last_tested_line, read_line will continue on the line after the until has been met.
        """
        lines = []
        if not self.has_read_a_line():
            self.read_line()
        current_line_arg = self._current_line
        line = self._current_line
        start = self._current_line_number - 1
        while not until(line[0]):
            lines.append(line)
            self.read_line()
            line = self._current_line
        if not start_at_current_line and lines:
            lines.pop(0)
        if include_last_tested_line or not lines and start_at_current_line:
            lines.append(line)
        if not continue_after_last_tested_line:
            self._current_line_number -= 1
            self._next_lines.append(self._current_line)
            self._current_line = self._last_line
            self._last_line = FIRST_LINE  # This value should never be used.
        return CollectedLines(lines, start, current_line=current_line_arg)

    @abstractmethod
    def _reached_end(self) -> bool:
        """Same as reached_end()."""

    def reached_end(self) -> bool:
        """Whether the end was reached and there are no more lines to read."""
        return not bool(self._next_lines) and self._reached_end()

    def insert_next_line(self, line: str, newline="\n"):
        """Insert a line to be read as the next line."""
        self._next_lines.append((line, newline, 0))


class CollectedLines(Lines):
    """A collection of lines."""

    def __init__(
        self,
        lines: List[str] | List[LineType],
        current_line_number: int = 0,
        current_line: LineType = FIRST_LINE,
    ):
        """The same interface as the LineParser just for a list.

        current_line_number == 0 means the start of the file.
        """
        super().__init__(
            current_line_number=current_line_number, current_line=current_line
        )

        self.lines: List[LineType] = []
        if lines:
            if isinstance(lines[0], str):
                self.lines = [(line, "\n", 1) for line in lines]  # type: ignore
            else:
                self.lines = [
                    (line if len(line) == 3 else line + (1,)) for line in lines  # type: ignore
                ]

    def _read_line(self) -> LineType:
        if self.reached_end():
            raise ReachedEndOfFile(
                f"Reached end of collected lines after line {self.current_line_number}."
            )
        line = self.lines.pop(0)
        return line

    def _reached_end(self) -> bool:
        return not self.lines


class LineParser(Lines):
    """Parse line by line.

    This line parser takes care of the different types of newlines we might encounter:
    \\r \\r\\n \\n
    Either of the counts as a newline.
    """

    @classmethod
    def from_string(cls, string: str):
        """Create a new LineParser from a string."""
        file = io.StringIO(string)
        return cls(file)

    def __init__(self, file: io.TextIOBase):
        """Create a new linewise parser.

        file is something that has a readline() method.

        Attributes:
        - current_line
        - current_line_end
        - current_line_number
        """
        super().__init__()
        self._file = file
        # the current line to process
        self._remaining_line = ""
        self._remaining_line_used = 0
        # the next line to process sometimes
        self._next_line = ""
        self._next_line_used = 0

    def _ensure_content_is_there(self):
        """Make sure we have content to process into a line."""
        if self._next_line:
            self._remaining_line_used = self._next_line_used
            self._remaining_line = self._next_line
            self._next_line = ""
            self._next_line_used = 0
        if self._remaining_line_used >= len(self._remaining_line):
            self._remaining_line_used = 0
            self._remaining_line = self._file.readline()
        return self._remaining_line_used, self._remaining_line

    def _read_line(self):
        # special case: skip the BeginData Section
        if self.current_line.startswith("%%BeginData:"):
            arguments = self.current_line.split(":")[-1].strip()
            count_type_bytes = arguments.split()
            if len(count_type_bytes) < 3 or count_type_bytes[2] == "Bytes":
                byte_count = int(count_type_bytes[0])
                byte_count -= len(self.current_line_end) - 1
                binary_data = self._file.read(byte_count)
                return binary_data, "", binary_data.count("\n")
        # prepare, read a line
        line_start, remaining_line = self._ensure_content_is_there()
        if remaining_line == "":
            raise ReachedEndOfFile(
                f"Reached end of file after line {self.current_line_number}."
            )
        # find a new line inside
        r_newline_index = remaining_line.find("\r", line_start)
        n_newline_index = remaining_line.find("\n", line_start)
        if r_newline_index == -1:
            if n_newline_index == -1:
                # EOF
                newline_end = line_end = len(remaining_line)
                newline = ""
            else:
                # \n
                line_end = n_newline_index
                newline_end = n_newline_index + 1
                newline = "\n"
        elif n_newline_index > r_newline_index or n_newline_index == -1:
            line_end = r_newline_index
            # there is a \r and there might be a \n behind it
            if r_newline_index + 1 == len(remaining_line):
                # \rEOF or \r as line end read before \n
                self._next_line = next_line = self._file.readline()
                if next_line:
                    if next_line[0] == "\n":
                        self._next_line_used = 1
                        newline = "\r\n"
                    else:
                        newline = "\r"
                        self._next_line_used = 0
                else:
                    # \rEOF
                    self._next_line_used = 0
                    newline = "\r"
                newline_end = r_newline_index + 1
            elif n_newline_index == r_newline_index + 1:
                # \r\n
                newline_end = r_newline_index + 2
                newline = "\r\n"
            else:
                # \r
                newline_end = r_newline_index + 1
                newline = "\r"
        else:
            # \n
            line_end = n_newline_index
            newline_end = n_newline_index + 1
            newline = "\n"
        # end: set all variables
        self._remaining_line_used = newline_end
        return remaining_line[line_start:line_end], newline, 1

    def _reached_end(self) -> bool:
        if (
            len(self._remaining_line) < self._remaining_line_used
            or len(self._next_line) < self._next_line_used
        ):
            return False
        line_start, remaining_line = self._ensure_content_is_there()
        return remaining_line == ""


__all__ = [
    "LineParser",
    "CollectedLines",
    "Lines",
    "ReachedEndOfFile",
    "LineType",
    "UntilType",
]
