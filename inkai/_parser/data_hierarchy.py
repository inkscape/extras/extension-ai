# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the data hierarchy.

See docs/specification_amendments/hierarchy.md for details.
"""

from typing import List, Tuple, Callable, Any, Dict, Union, Optional

from inkai.parser.lines import Lines
from . import parse
from .hierarchy_objects import *
from .lines import LineParser, Lines
import io
from .tokens import Tokenizer, TOKEN_REGEX


ObjectContainers: Dict[str, type] = {
    "KnownStyle": KnownStyle,
    "ActiveStyle": ActiveStyle,
    "SimpleStyle": SimpleStyle,
    "BlendStyle": BlendStyle,
    "FillStyle": FillStyle,
    "StrokeStyle": StrokeStyle,
    "BasicFilter": BasicFilter,
    "CompoundFilter": CompoundFilter,
    "AI11Text": AI11Text,
    "Document": Document,
    "SVGFilter": SVGFilter,
    "GObjRef": GObjRef,
}


class DataHierarchyParser(Tokenizer):
    """Parse the document data."""

    def __init__(self, lines: Lines):
        """Create a new process to parse."""
        super().__init__(lines)

    def read_objects(self):
        """Read all objects until we reach the end."""
        result = []
        while not self._lines.reached_end():
            result.append(self.read_container())
        return result

    def read_container(self):
        token = self.next()[1:]
        self.skip(":")
        if token in ObjectContainers:
            return self.read_object(ObjectContainers[token]())
        if token in ["Dictionary", "ArtDictionary"]:
            return self.read_dictionary(token)
        if token == "XMLNode":
            return self.read_xmlnode()
        if token == "Array":
            return self.read_array()
        if token == "Art":
            return self.read_art()
        if token in ["Binary", "AI11TextDocument", "AI11UndoFreeTextDocument"]:
            return self.read_binary(token)
        if token == "XMLUID":
            return self.read_xmluid()
        else:
            raise ValueError("Unknown container type: " + token)

    def read_object(self, result: HierarchyObject) -> HierarchyObject:
        """Reads the content of a HierarchyObject, including the terminating ;"""
        while True:
            token = self.peek()
            if token == ";":
                # Container finished.
                self.skip(";")
                return result
            if token.startswith("/"):
                # sub-container
                value = self.read_container()
                key = self.next()
                assert key.startswith("/"), (key, value)
            else:
                # primitive value, read tokens until "/"
                values_split, raw_value, key = self.collect_until_slash()
                value = (values_split, raw_value)
            self.skip_optional(",")

            setattr(result, key[1:], value)

    def current_position_in_line(self) -> int:
        """Tokenisation is good for 99% of our use cases. In particular, it's much
        faster than iterating over the string.
        But sometimes -  e.g. line breaks in tokens - it fails. This only happens
        in raw AI data and strings with line breaks. In these cases, we preemtively
        fall back to character-wise processing, but we need the current position for that.
        """

        # We don't want to store the match location for each token, that would be wasteful.
        # We use the same regex used for tokenizing the line in the first place.

        for i, match in enumerate(TOKEN_REGEX.finditer(self._line)):
            if i == self._index - 1:
                return match.span(0)[1]
        return 0

    def current_token_from_position(self, position) -> int:
        for i, match in enumerate(TOKEN_REGEX.finditer(self._line)):
            if match.span(0)[0] <= position <= match.span(0)[1]:
                return i
        raise ValueError("Unable to locate token at current position")

    def collect_until_slash(self) -> Tuple[List[str], str, str]:
        """Collect characters until the next slash.
        Returns them both tokenized and raw (includes newlines), and the slash token"""

        first_line = True
        characters_to_check = ""
        position = 0
        in_string = False
        result = ""
        result_split: List[str] = []
        current_entry = ""
        while True:
            while position == len(
                characters_to_check
            ):  # End of line, possibly multiple
                if first_line:
                    first_line = False
                    characters_to_check = self._line
                    position = self.current_position_in_line()
                else:
                    result += "\n"
                    self._index = len(self._tokens)
                    characters_to_check = self.next_line()
                    if in_string:
                        current_entry += "\n"
                    elif current_entry.strip() != "":
                        result_split.append(current_entry.strip())
                        current_entry = ""
                    position = 0
            current = characters_to_check[position]

            # A space character starts a new group.
            if current == " " and not in_string:
                if current_entry.strip() != "":
                    result_split.append(current_entry.strip())
                current_entry = ""
            # A curly bracked starts a string.
            if current == "(":
                in_string = True
            if current == ")":
                if position > 0:
                    if characters_to_check[position - 1] != "\\":
                        in_string = False
                else:  # a string may terminate immediately in a new line
                    in_string = False
            if current == "/" and not in_string:
                assert position == 0 or characters_to_check[position - 1] == " "
                # We're done. Restore the tokenizer so parsing can resume.
                self._index = self.current_token_from_position(position)
                self._tokens = self.tokenize(self._line)
                next = self.next()
                assert next[0] == "/"
                return result_split, result.strip(), next

            current_entry += current
            result += current
            position += 1

    typemap: Dict[
        str,
        Callable[
            [
                List[str],
            ],
            Any,
        ],
    ] = {
        "/Bool": lambda x: bool(int(x[0])),
        "/Int": lambda x: int(x[0]),
        "/Real": lambda x: float(x[0]),
        "/RealMatrix": lambda x: tuple(float(i) for i in x),
        "/RealPoint": lambda x: Point.from_tokens(*x),
        "/RealPointRelToROrigin": lambda x: PointRelToROrigin.from_tokens(*x),
        "/String": parse_str,
        "/UnicodeString": parse_str,
    }

    def _dict_array_processor(self) -> Optional[Any]:
        token = self.peek()
        if token == ";":
            self.skip(";")
            return None
        if token.startswith("/") and token not in self.typemap:
            value = self.read_container()
        else:
            values, _, type = self.collect_until_slash()
            value = self.typemap[type](values)
        return value

    def read_dictionary(self, type) -> Dict[str, Any]:
        """Reads the content of a dictionary, including the terminating ;"""
        result: Dict[str, Any] = {}
        result["_type"] = type
        if self.skip_optional("/NotRecorded"):
            result["Recorded"] = False
            self.skip(",")
        if self.skip_optional("/Recorded"):
            result["Recorded"] = True
            self.skip(",")
        while True:
            value = self._dict_array_processor()
            if value is None:
                return result
            key = self.next()
            result[key[1:-1]] = value
            self.skip_optional(",")

    def read_array(self) -> List[Any]:
        """Reads the content of an array, including the terminating ;"""
        result: List[Any] = []
        while True:
            value = self._dict_array_processor()
            if value is None:
                return result
            result.append(value)
            self.skip_optional(",")

    def read_xmlnode(self) -> Union[Dict, Tuple[str, str]]:
        """Reads the content of an /XMLNode, including the terminating ;

        Simplifies attribute nodes into dictionary entries."""

        dict = self.read_dictionary("XMLNode")
        type = dict["xmlnode-nodetype"]

        if type in (2, 3):  # Attribute or text
            return (dict["xmlnode-nodename"], dict["xmlnode-nodevalue"])
        elif type in (1, 9):  # node or root node
            return dict
            # TODO: Convert this into something more easy to work with.
            # Can't use etree.element since we might not know the ns prefixes.
            result = etree.Element(dict["xmlnode-nodename"])
            for key in dict["xmlnode-attributes"]:
                k, value = dict["xmlnode-attributes"][key]
                assert key == k
                result.set(key, value)
            for element in dict["xmlnode-children"]:
                if isinstance(element, etree.Element):
                    result.append(element)
                if isinstance(element, tuple):
                    if element[0] == "#text":
                        result.text = element[1]
            return result
        raise ValueError("Unknown xmlnode-nodetype")

    binary_typemap = {
        "Binary": Binary,
        "AI11TextDocument": AI11TextDocument,
        "AI11UndoFreeTextDocument": AI11UndoFreeTextDocument,
    }

    def read_binary(self, type):
        """Read /Binary including the terminating ;"""
        self.skip("/ASCII85Decode")
        self.skip(",")
        data = []
        while True:
            # Binary always begins with %, irrespective of the line start of the
            # surrounding section - "%_" or "".
            line = self.next_line("%")
            data.append(line)

            if line[-2:] == "~>":
                break
        # Continue reading as if this was a normal object.

        result = self.binary_typemap[type](data)
        result._type = type
        return self.read_object(result)

    def read_art(self):
        """Read art. Simply read until the next semicolon on the same level of %_ indentation"""
        data = []
        while True:
            line = self.next_line()
            # This needs to be the first token of the line
            if line.startswith("; "):
                self._index = 1
                self._tokens = self.tokenize(self._line)
                break
            data.append(line)
        return Art(data)

    def read_xmluid(self):
        result = parse.ps(self.next()[1:-1])
        self.skip(";")
        return result

    @classmethod
    def parse_string(cls, string: str, all=False):
        """Parse the data into a hierarchical object structure.

        Only the first object is returned."""
        file = io.StringIO(string)
        return cls.parse_file(file, all)

    @classmethod
    def parse_file(cls, file: io.TextIOBase, all=False):
        """Parse a file. Only the first object is returned."""
        lines = LineParser(file)
        return cls.parse_lines(lines, all)

    @classmethod
    def parse_lines(cls, lines: Lines, all=False):
        """Parse a lines object; only the first object is returned."""
        parser = cls(lines)
        return parser.read_objects() if all else parser.read_container()
