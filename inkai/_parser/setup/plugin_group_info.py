# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Loading plugins."""
from inkai.parser.lazy import AIElement, StartOfLine
from inkai.parser.lines import Lines


class PluginGroupInfo(AIElement):
    """Information about AI plugins.
    This is reverse-engineered.
    """

    spec = StartOfLine("%AI8_PluginGroupInfo")

    def __init__(self, lines: Lines):
        """Create a new PluginGroupInfo."""
        super().__init__(lines)
        self.raw_content = lines.read_line()
