# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Colour palette information."""
from inkai.parser.lazy import (
    CollectedSection,
    CommandParser,
    StartOfLineSection,
    generate_eol,
    AIElement,
    TypeAlias,
)
from inkai.parser.lines import Lines
from .. import parse
from ..color import Color
from typing import Optional, List, Type, NamedTuple

PaletteBeginning = NamedTuple(
    "PaletteBeginning", [("top_left_cell_index", int), ("selected_index", int)]
)


class PaletteCell:
    """A cell in the palette."""

    def __init__(self):
        """Create a new cell from the elements inside."""
        self._color: Optional[Color] = None
        self._name = ""

    def append(self, element: AIElement):
        """Add an element as part of this cell."""
        if isinstance(element, Color):
            self._color = element
            self._name = element.name
        if isinstance(element, PaletteCellName):
            self._name = element.name

    @property
    def color(self) -> Optional[Color]:
        """The color of the cell."""
        return self._color

    @property
    def name(self) -> str:
        """The name of the color or ''."""
        return self._name


PB: Type[AIElement] = generate_eol(
    "PB", "End of the palette definition.\nSee AI Spec page 78.", []
)
Pc: Type[AIElement] = generate_eol(
    "Pc", "End of the palette cell definition.\nSee AI Spec page 79.", []
)
Pb: TypeAlias = generate_eol(  # type: ignore
    "top_left_cell_index selected_index Pb",
    "Beginning of the palette definition.\nSee AI Spec page 78.",
    [parse.int, parse.int],
)


class PaletteParser(CommandParser):
    """Parse the names additionally."""

    def parse_default(self, lines: Lines):
        """Check if we have a name in the line."""
        if lines.current_line.startswith("(") and lines.current_line.endswith(")"):
            return PaletteCellName(lines)
        return super().parse_default(lines)


class Palette(CollectedSection):
    """Color palettes that are used in the AI file.

    See AI Spec page 78.
    """

    InnerParserClass = PaletteParser
    spec = StartOfLineSection("%AI5_BeginPalette", "%AI5_EndPalette")

    example_string = """%AI5_BeginPalette
% This string is taken from the AI Spec page 80.
0 0 Pb
% Begin palette cell entries, starting at upper left corner.
Pn
% Zeroth cell is color “none.”
Pc
% Begin a palette cell entry. First cell is black (gray scale of 1).
1 g
Pc
% Begin a palette cell entry. Second cell is white (gray scale of 0).
0 g
Pc
% Begin a palette cell entry.
0 0 0 0 k
% Third cell is process white (CMYK value of 0, 0, 0, 0).
Pc
% Fourth cell is 75% gray.
0.75 g
...
Pc
% Eighth cell is instance of Black & White gradient.
Bb
% Begin gradient.
2 (Black & White) -4014 4716 0 0 1 0 0 1 0 0 Bg
0 BB
% End gradient.
Pc
% Ninth cell is 25% Cyan.
0.25 0 0 0 k
...
Pc
1 0.5 1 0 k
Pc
% Pc operator follows last cell.
PB
% End palette operator.
%AI5_EndPalette
"""

    @property
    def cells(self) -> List[PaletteCell]:
        """The cells of the palette."""
        cells: List[PaletteCell] = [PaletteCell()]
        for child in self.children:
            cells[-1].append(child)
            if isinstance(child, Pc):
                cells.append(PaletteCell())
        return cells[:-1]

    @classmethod
    def add_to_inner(self, parser: CommandParser) -> None:
        """Add the palette elements."""
        parser.add_spec(PB, Pb, Pc, Color)

    @property
    def beginning(self) -> PaletteBeginning:
        """Where the palette starts."""
        pb: Pb = self.first(Pb)  # type: ignore
        return PaletteBeginning(pb.top_left_cell_index, pb.selected_index)


class PaletteCellName(AIElement):
    """The name of a palette cell."""

    token_parsers = [parse.ps]

    OuterParserClass = PaletteParser

    @property
    def name(self) -> str:
        """The name of the pallette cell."""
        return self.tokens[0]


__all__ = ["Palette", "Pb", "PB", "Pc", "PaletteCellName"]
