# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the AI file's Setup Section.

This section contains definitions and meta data that
is reused within the document.
"""
from __future__ import annotations
from .setup import Setup


__all__ = [
    "Setup",
]
