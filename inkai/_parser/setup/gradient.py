# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Gradients are defined in the setup section of the document and referenced later."""

from inkai.parser.lazy import CommandParser, TokenParsersType
from inkai.parser.lines import Lines
from ..lazy import (
    generate_eol,
    CollectedSection,
    StartOfLineSection,
    EndOfLine,
    StartOfLine,
    SectionIdentifiedByFirstEnd,
)
from .. import parse
from ..util import TypeAlias
from typing import List, Optional
from enum import Enum, unique
from ..color import color_to_rgb_string


Bn: TypeAlias = generate_eol("nblends Bn", "Number if gradients. See page 40 AI Spec.", [parse.int])  # type: ignore


class Bd(
    generate_eol(  # type: ignore
        "name type nColors Bd", "", [parse.ps, parse.int, parse.int]
    )
):
    """Gradient information.

    See page 40 AI Spec.
    """

    def is_radial(self) -> bool:
        """Whether this is a radial gradient."""
        return self.type == 1

    def is_linear(self) -> bool:
        """Whether this is a linear gradient."""
        return self.type == 0


BrBs = SectionIdentifiedByFirstEnd(
    StartOfLine("["),
)


class Br(CollectedSection):
    """Gradient Ramp.

    See AI Spec page 43.
    """


BrBs.add(Br, EndOfLine("%_Br").until)


@unique
class ColorStyle(Enum):
    """The color style.

    See page 42 AI Spec.
    """

    GRAY = 0
    CMYK = 1
    RGB = 2
    CUSTOM_CMYK = 3
    CUSTOM_RGB = 4
    NEW_RGB = 5
    WITH_OPACITY = 6


Bs_spec = EndOfLine("Bs", "%_BS", "%_Bs")


class Bs(CollectedSection):
    """Color Stops.

    See AI Spec page 41.

    This is supposed to offer the same color interface as
    inkai.parser.color.Color.
    """

    spec = Bs_spec

    # colorStyle, see page 42 AI Spec
    token_parsers: TokenParsersType = [  # type: ignore
        [parse.float, "0", parse.float, parse.float, parse.string],  # gray
        [
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            "1",
            parse.float,
            parse.float,
            parse.string,
        ],  # CMYK
        [
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            "2",
            parse.float,
            parse.float,
            parse.string,
        ],  # RGB
        [
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.ps,
            parse.float,
            "3",
            parse.float,
            parse.float,
            parse.string,
        ],  # CMYK custom color
        [
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.ps,
            parse.float,
            parse.string,
            "4",
            parse.float,
            parse.float,
            parse.string,
        ],  # RGB custom color
        [
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.float,
            parse.ps,
            parse.float,
            parse.string,
            "5",
            parse.float,
            parse.float,
            parse.string,
        ],  # color type 5, see docs/specification_amendments/commands.md
    ]
    # add the token parsers for type 6
    for token_parser in token_parsers[:]:
        token_parsers.insert(  # type: ignore
            0, token_parser[:-3] + [parse.float, "6"] + token_parser[-3:]  # type: ignore
        )

    @property
    def color_style(self) -> ColorStyle:
        """The colorStyle argument."""
        return ColorStyle(
            int(self.tokens[-6] if self.has_additional_opacity else self.tokens[-4])
        )

    @property
    def has_additional_opacity(self) -> bool:
        """Whether this is color style 6 defining an additional opacity value.

        This is a modification, see docs/specification_amendments/commands.md.
        """
        return self.tokens[-4] == "6"

    @property
    def ramp_point(self) -> float:
        """The ramp point (0 <= ramp_point <= 100)."""
        return self.tokens[-2]

    @property
    def mid_point(self) -> float:
        """The middle point (0 <= mid_point <= 100)."""
        return self.tokens[-3]

    @property
    def gray(self) -> List[float]:
        """The [gray] value or []."""
        return [self.tokens[0]] if self.color_style == ColorStyle.GRAY else []

    @property
    def rgb(self) -> List[float]:
        """The [red, green, blue] value or []."""
        return (
            self.tokens[4:7]
            if self.color_style
            in [ColorStyle.CUSTOM_RGB, ColorStyle.RGB, ColorStyle.NEW_RGB]
            else []
        )

    @property
    def cmyk(self) -> List[float]:
        """The [cyan, magenta, yellow, black] value or []."""
        return (
            self.tokens[:4]
            if self.color_style
            in [
                ColorStyle.RGB,
                ColorStyle.CMYK,
                ColorStyle.CUSTOM_RGB,
                ColorStyle.CUSTOM_CMYK,
                ColorStyle.NEW_RGB,
            ]
            else []
        )

    @property
    def alpha(self) -> float:
        """The alpha value."""
        if self.has_additional_opacity:
            return 1 - self.tokens[-5]
        return (
            1 - self.tokens[5]
            if self.color_style == ColorStyle.CUSTOM_CMYK
            else 1 - self.tokens[8]
            if self.color_style in (ColorStyle.CUSTOM_RGB, ColorStyle.NEW_RGB)
            else 1
        )

    @property
    def name(self) -> str:
        """The name of the color or ""."""
        return (
            self.tokens[4]
            if self.color_style == ColorStyle.CUSTOM_CMYK
            else self.tokens[7]
            if self.color_style in (ColorStyle.CUSTOM_RGB, ColorStyle.NEW_RGB)
            else ""
        )

    @property
    def rgb_string(self) -> str:
        """Returns #RRGGBB or ''.

        This converts RGB, gray and CMYK colors to an RGB string without transparency (alpha).
        """
        return color_to_rgb_string(self)  # type: ignore

    def __init__(self, lines: Lines):
        """Create a new Bs command."""
        if lines.current_line.strip() == "[":
            lines.read_line()
        super().__init__(lines)

    def __eq__(self, other: object) -> bool:
        """self == other"""
        print(self, "==", other)
        return isinstance(other, Bs) and self.tokens[:-1] == other.tokens[:-1]

    def __hash__(self) -> int:
        """hash(self)"""
        return hash(tuple(self.tokens))

    @property
    def preference(self) -> int:
        """Whether this is the preferred operator.

        We have these lines:

            %_ ... Bs -> 0 # preferred
            ... %_BS -> 1 # less precision
            ... %_Bs -> 2 # specified but obsolete
        """
        return ["Bs", "%_BS", "%_Bs"].index(self.tokens[-1])


BrBs.add(Bs, Bs_spec.until)


class Gradient(CollectedSection):
    """Gradient definitions.

    Example:

        %AI5_BeginGradient: (name)
        ...
        "%AI5_EndGradient"

    See AI Spec page 39+.
    """

    spec = StartOfLineSection("%AI5_BeginGradient:", "%AI5_EndGradient")

    token_parsers = [parse.string, parse.ps]

    @property
    def name(self) -> str:
        """The name of the gradient."""
        return self.tokens[1]

    @classmethod
    def add_to_inner(self, parser: CommandParser) -> None:
        """Add gradient parsing to the parser for the content of the section."""
        parser.add_spec(Bd, Bs, Br, BrBs)
        parser.ignore_line_start("BD")

    @property
    def color_stops(self) -> List[Bs]:
        """The color stops of this gradient.

        If color stops are duplicated, the most preferred one is used.
        """
        result: List[Bs] = []
        for _bs in self.categories.get(Bs, []):
            bs: Bs = _bs  # type: ignore
            if result and bs == result[-1]:
                if bs.preference < result[-1].preference:
                    result[-1] = bs
            else:
                result.append(bs)
        return result

    @property
    def definition(self) -> Bd:
        """The parameter definition of the gradient."""
        return self.first(Bd)  # type: ignore


__all__ = ["Bn", "Gradient", "Bd", "Br", "Bs", "ColorStyle"]
