# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""The text information in the document setup."""
from inkai.parser.data_hierarchy import DataHierarchyParser
from inkai.parser.lazy import CollectedSection, CommandParser, StartOfLineSection
from inkai.parser.text import AI11BasicTextDocumentParser
from inkai.parser.text.definitions import defs
from inkai.parser.text.hierarchy import Frame, Story, TextDocumentObject
from typing import List


class TextDocument(CollectedSection):
    """The text definitions for newstyle texts.
    This is reverse-engineered.
    See docs/specification_amendments/text_documents.md
    Example:
        %AI11_BeginTextDocument
        /AI11TextDocument : /ASCII85Decode ,
        %[... blob ...]~>
        7893 7770 /RulerOrigin ,
        ;
        /AI11UndoFreeTextDocument : /ASCII85Decode ,
        %[... blob ...]~>
        ;
        %AI11_EndTextDocument
    """

    spec = StartOfLineSection("%AI11_BeginTextDocument", "%AI11_EndTextDocument")

    def _parse(self, parser: CommandParser):
        """Parse all the content."""
        text_document = DataHierarchyParser.parse_lines(self.lines)
        self.ruler_origin = text_document.RulerOrigin
        decoded = text_document.Data
        parsed = AI11BasicTextDocumentParser(decoded.decode("latin-1")).parse()
        return TextDocumentObject(defs, parsed)

    parsed_content: TextDocumentObject  # type: ignore

    @property
    def stories(self) -> List[Story]:
        """The list of stories.
        Stories contain information about the individual text elements
        and their paragraph/character style (overrides).
        """
        return self.parsed_content.DocumentObjects.TextObjects  # type: ignore

    @property
    def frames(self) -> List[Frame]:
        """The list of stories.
        Stories contain information about the individual text elements
        and their paragraph/character style (overrides).
        """
        return self.parsed_content.DocumentResources.TextFrameSet.Resources  # type: ignore


__all__ = ["TextDocument"]
