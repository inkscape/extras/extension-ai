# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Parse AI11Texts (references to the TextDocument).

This is reverse-engineered.
"""
from inkai.parser.lazy import (
    AIElement,
    CollectedSection,
    StartOfLineSection,
    generate_eol,
)
from inkai.parser.objects.object_section import ObjectSection
from ..data_hierarchy import DataHierarchyParser
from .. import parse


class AI11Text(CollectedSection):
    """The ArtDictionary.

    This usually contains more information than the paths preceding it.

    Example:

        /AI11Text :
        0 /FreeUndo ,
        0 /FrameIndex ,
        0 /StoryIndex ,
        2 /TextAntialiasing ,
        ;

    """

    spec = StartOfLineSection("/AI11Text", "", start_at_current_line=True)

    def _parse(self, parser) -> dict:
        """Parse all the content."""
        return DataHierarchyParser.parse_lines(self.lines)

    @property
    def story_index(self) -> int:
        """This is the index of the story to show."""
        return self["StoryIndex"]

    @property
    def frame_index(self) -> int:
        """This is the index of the referenced frame."""
        return self["FrameIndex"]


class Xd(
    generate_eol(  # type: ignore
        "a b Xd", "Xd command for text\nExample: 0 0 Xd", [parse.int, parse.int]
    )
):
    pass


class XW(
    generate_eol(  #  type: ignore
        "a b XW", "XW command for text\nExample: 6 () XW", [parse.int, parse.ps]
    )
):
    pass


__all__ = ["AI11Text", "Xd", "XW"]
