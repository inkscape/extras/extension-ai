# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""New object definitions that are not in the AI Spec.

If these have clear place because they do not belong to a particular
type of object or multiple, they can go here.
"""
from ..lazy import generate_eol, StartOfLineSection, CollectedSection
from ..data_hierarchy import DataHierarchyParser
from .live_shapes import handler_name2live_shape, LiveShape
from .. import parse


class ArtDictionary(CollectedSection):
    """The ArtDictionary.

    This usually contains more information than the paths preceding it.

    Example:

        %_/ArtDictionary :
        %_/XMLUID : (Layer_1) ; (AI10_ArtUID) ,
        %_;
        %_

    """

    spec = StartOfLineSection("/ArtDictionary", "%_", start_at_current_line=True)

    def _parse(self, parser) -> dict:
        """Parse all the content."""
        return DataHierarchyParser.parse_lines(self.lines)

    @property
    def live_shape(self) -> LiveShape:
        """Return the live shape described or None if this is not a liveshape."""
        shape = self.parsed_content.get("ai::LiveShape", {})  # type: ignore
        handler = shape.get("ai::LiveShape::HandlerName")
        Shape = handler_name2live_shape[handler]
        return Shape(shape)


Xd = generate_eol(
    "flag1 flag2 XD",
    """See docs/specification_amendments/commands.md""",
    [parse.bool, parse.bool],
)


__all__ = ["ArtDictionary", "Xd"]
