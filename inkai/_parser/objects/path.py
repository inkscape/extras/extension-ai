# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Path operators, geometry and other path specifications.

See page 54+ AI Spec.
"""
from ..lazy import (
    AIElement,
    StartOfLine,
    generate_eol,
    EndOfLine,
    StartOfLineSection,
    TypeAlias,
)
from .. import parse
from typing import Any, List, Optional, Dict, Union, Tuple, Sequence, Type
from ..tokens import tokenize
from .object_section import ObjectSection
from .interfaces import EndOfPath


class PathRender(EndOfPath):
    """Rendering paths.

    See page 54,58 AI Spec.
    """

    spec = StartOfLine("b", "f", "s", "n", "B", "F", "S", "N")
    example_string = "f"

    token_parsers = [parse.string]

    @property
    def close(self) -> bool:
        """Should the path be closed?"""
        return self.tokens[0] in "bfsN"

    @property
    def fill(self) -> bool:
        """Should the path be filled?"""
        return self.tokens[0] in "fFbB"

    @property
    def stroke(self) -> bool:
        """Should there be a stroke around the path?"""
        return self.tokens[0] in "sSbB"


class PathOperator(AIElement):
    """Path rendering positions."""

    command: str

    def is_corner(self) -> bool:
        """Is this a corner?"""
        return self.command.upper() == self.command

    @property
    def x(self) -> Optional[float]:
        """The x coordinate or None if not given."""
        return None

    @property
    def y(self) -> Optional[float]:
        """The y coordinate or None if not given."""
        return None

    @property
    def x1(self) -> Optional[float]:
        """The x1 coordinate or None if not given."""
        return None

    @property
    def y1(self) -> Optional[float]:
        """The y1 coordinate or None if not given."""
        return None

    @property
    def x2(self) -> Optional[float]:
        """The x2 coordinate or None if not given."""
        return None

    @property
    def y2(self) -> Optional[float]:
        """The y2 coordinate or None if not given."""
        return None

    @property
    def x3(self) -> Optional[float]:
        """The x3 coordinate or None if not given."""
        return None

    @property
    def y3(self) -> Optional[float]:
        """The y3 coordinate or None if not given."""
        return None

    @property
    def xy(self) -> Sequence[float]:
        """The x and y coordinates as a tuple. (x, y, ...)"""
        points: List[float] = []
        if self.x is not None and self.y is not None:
            points += [self.x, self.y]
        if self.x1 is not None and self.y1 is not None:
            points += [self.x1, self.y1]
        if self.x2 is not None and self.y2 is not None:
            points += [self.x2, self.y2]
        if self.x3 is not None and self.y3 is not None:
            points += [self.x3, self.y3]
        return points


m: TypeAlias = generate_eol(  # type: ignore
    "x y m",
    """The m operator is equivalent to the PostScript language moveto operator. It
changes the current point to x, y, omitting any connecting line segment. A
path must have m as its first operator.

AI Spec page 56.""",
    [parse.float, parse.float],
    base=PathOperator,
)

l = generate_eol(
    "x y l",
    """The l (lowercase L) operator appends a straight line segment from the current
point to x, y. The new current point is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float],
    base=PathOperator,
)

L = generate_eol(
    "x y L",
    """The L operator is similar to the l operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float],
    base=PathOperator,
)

c = generate_eol(
    "x1 y1 x2 y2 x3 y3 c",
    """The c operator appends a Bézier curve to the path from the current point to
x3, y3 using x1, y1 and x2, y2 as the Bézier direction points. The new current
point is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

C = generate_eol(
    "x1 y1 x2 y2 x3 y3 C",
    """The C operator is similar to the c operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

v = generate_eol(
    "x2 y2 x3 y3 v",
    """The v operator adds a Bézier curve segment to the current path between the
current point and the point x3, y3, using the current point and then x2, y2 as the
Bézier direction points. The new current point is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)


V = generate_eol(
    "x2 y2 x3 y3 V",
    """The V operator is similar to the v operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

y = generate_eol(
    "x1 y1 x3 y3 y",
    """The y operator appends a Bézier curve to the current path between the current
point and the point x3, y3 using x1, y1 and x3, y3 as the Bézier direction points.
The new current point is x3, y3 and is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

Y = generate_eol(
    "x1 y1 x3 y3 Y",
    """The Y operator is similar to the y operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

Attributes = Dict[str, Union[int, float, bool]]


class PathAttributes(AIElement):
    """Attributes of the path.

    This describes other attributes of how to render the path.
    They may occur in one line.

    See page 55+ AI Spec.

    All the attributes defined here are None
    if this command does not set them.
    """

    spec = EndOfLine("d", "i", "D", "j", "J", "M", "w")
    _is_parsed = False
    _line_join: Optional[int] = None
    _line_cap: Optional[int] = None
    _phase: Optional[float] = None
    _line_width: Optional[float] = None
    _miter_limit: Optional[float] = None
    _clockwise: Optional[bool] = None
    _flatness: Optional[float] = None

    def _parse(self):
        """Parse the content once."""
        if self._is_parsed:
            return
        self._is_parsed = True
        args: List[str] = []
        tokens = tokenize(self.first_line)
        for token in tokens:
            if token == "J":
                self._line_cap = parse.int(args.pop())
            elif token == "j":
                self._line_join = parse.int(args.pop())
            elif token == "M":
                self._miter_limit = parse.float(args.pop())
            elif token == "w":
                self._line_width = parse.float(args.pop())
            elif token == "i":
                self._flatness = parse.float(args.pop())
            elif token == "D":
                self._clockwise = not parse.bool(args.pop())
            elif token == "d":
                self._phase = 0  # todo
            else:
                args.append(token)

    @property
    def phase(self) -> Optional[float]:
        """[array] phase d

        See AI Spec page 55.
        """
        self._parse()
        return self._phase

    @property
    def line_width(self) -> Optional[float]:
        """linewidth w

        See AI Spec page 56.
        """
        self._parse()
        return self._line_width

    @property
    def line_join(self) -> Optional[int]:
        """line_join j

        See AI Spec page 55.
        """
        self._parse()
        return self._line_join

    @property
    def line_cap(self) -> Optional[int]:
        """line_cap J

        See AI Spec page 56.
        """
        self._parse()
        return self._line_cap

    @property
    def miter_limit(self) -> Optional[float]:
        """miter_limit M

        See AI Spec page 56.
        """
        self._parse()
        return self._miter_limit

    @property
    def clockwise(self) -> Optional[bool]:
        """counter_clockwise D

        See AI Spec page 55.
        """
        self._parse()
        return self._clockwise

    @property
    def flatness(self) -> Optional[float]:
        """flatness i

        See AI Spec page 55.
        """
        self._parse()
        return self._flatness


class CompoundPath(ObjectSection):
    """Join paths together into one object.

    Example:

        *u
        ...
        *U

    See AI Spec page 59.
    """

    spec = StartOfLineSection("*u", "*U")
    example_string = "*u\n*U"


class Overprint(AIElement):
    """Overprinting for paths.

    See page 63 AI Spec.
    """

    token_parsers = [parse.bool, parse.string]
    spec = EndOfLine("O", "R")

    @property
    def overprint(self):
        """Whether to apply an overprint to the path."""
        return self.tokens[0]

    @property
    def fill(self):
        """Whether to apply the overprint value to the fill of the path."""
        return self.tokens[1] == "O"

    @property
    def stroke(self):
        """Whether to apply the overprint value to the stroke of the path."""
        return self.tokens[1] == "R"


Xy: TypeAlias = generate_eol(  # type: ignore
    "_1 stroke_opacity _3 _4 _5 Xy",
    """The Xy operator specifies some attributes of a path.

    Example:

        0 0.6 0 0 0 Xy

    This is reverse-enginnered.
    See docs/specification_amendments/commands.md
    """,
    [parse.float, parse.float, parse.float, parse.float, parse.float],
)


__all__ = [
    "PathRender",
    "m",
    "l",
    "L",
    "c",
    "C",
    "y",
    "Y",
    "v",
    "V",
    "PathOperator",
    "PathAttributes",
    "CompoundPath",
    "Overprint",
    "Xy",
]
