# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""These definitions can be found in the Adobe Illustrator File Format Specification from 1998.

You can find other specified operators in other modules, too.
This module is the place to put them if they do not belong anywhere else.
"""
from ..lazy import generate_eol, CollectedSection, EndOfLineSection
from .. import parse
from .object_section import ObjectSection

A = generate_eol(
    "locked A",
    """The A operator specifies whether the object defined immediately after the
operator can be selected when editing the document.

See AI Spec page 51.
""",
    [parse.bool],
)


class Group(ObjectSection):
    """A group contains objects.

    Example:

        u
        ...
        U
    """

    spec = EndOfLineSection("u", "U")


XR = generate_eol(
    "fill_rule XR",
    """0 = non-zero, 1 = even-odd

See AI Spec page 58.
""",
    [parse.int],
)

__all__ = ["A", "Group", "XR"]
