# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Images as defined in the AI Spec page 68+."""

from inkai.parser.lines import Lines
from ..lazy import (
    generate_eol,
    EndOfLine,
    AIElement,
    StartOfLine,
    StartOfLineSection,
    CommandParser,
)
from .. import parse
from ..util import TypeAlias
from .object_section import ObjectSection


class XIBase(AIElement):
    """Base class for XI and XF."""

    def is_internal(self) -> bool:
        """The image can be found in the ai file: True"""
        return True


XI: TypeAlias = generate_eol(  # type: ignore
    "[ a b c d tx ty ] llx lly urx ury h w bits image_type alpha_channel_count reserved binary transparent XI",
    "See page 68 AI Spec.",
    [parse.float] * 10 + [parse.int] * 5 + [parse.string] + [parse.bool] * 2,
    base=XIBase,
)


class XF(XI):
    """The XF operator is identical to the XI operator except that it is used when the
    actual image data is not included in the file."""

    spec = EndOfLine("XF")
    command = "XF"

    def is_internal(self) -> bool:
        """The image is referenced by a XG command: False"""
        return False


XG: TypeAlias = generate_eol(  # type: ignore
    "path modified XG", "See page 69 AI Spec.", [parse.ps, parse.bool]
)

XN: TypeAlias = generate_eol(  # type: ignore
    "color_space XN",
    """New command. This names the color mode of the raster image.
    
    Example:
    
        /DeviceRGB XN
        /DeviceCMYK XN
    
    See docs/specification_amendments/commands.md
    """,
    [parse.string],
)

Xh: TypeAlias = generate_eol(  # type: ignore
    "[ a b c d tx ty ] urx ury flag Xh",
    """New command, a subset of XI.
    
    Example:
    
        [ -1 0 0 -1 1508 -772 ] 1469 764 0 Xh
    
    See docs/specification_amendments/commands.md
    """,
    [parse.float] * 8 + [parse.ignore],
)


class File(AIElement):
    """AI5_File - pretty useless.

    See AI Spec 69.
    """

    spec = StartOfLine("%AI5_File:")


class XH(AIElement):
    """XH

    See docs/specification_amendments/commands.md
    """

    spec = StartOfLine("XH")


class DecapitatedXIParser(CommandParser):
    """Sometimes, the XI command is decapitated: XI is missing!
    This parser takes care of this.

    Example:

        [ -1 0 0 -1 1508 -772 ] 0 0 1469 764 1469 764 8 3 0 1 1 0 4 4 0 0
        %%BeginData: 3366952
        XI
    """

    def parse_default(self, lines: Lines):
        """If we do not understand a line, we check if it could be an XI command."""
        if lines.current_line.rsplit(maxsplit=1)[-1].isdigit():
            return XI.from_string(lines.current_line + " XI")
        return super().parse_default(lines)


class Raster(ObjectSection):
    """%AI5_BeginRaster:

    Embedded or linked raster image information.

    See page 68++ AI Spec.
    """

    spec = StartOfLineSection("%AI5_BeginRaster", "%AI5_EndRaster")

    example_string = """%AI5_BeginRaster
() 1 XG
/DeviceRGB XN
[ -1 0 0 -1 1508 -772 ] 1469 764 0 Xh
[ -1 0 0 -1 1508 -772 ] 0 0 1469 764 1469 764 8 3 0 1 1 0 4 4 0 0
%%BeginData: 12
XI
<binary>
%%EndData
XH
%AI5_EndRaster
"""

    @classmethod
    def create_parser(cls) -> CommandParser:
        """Create a new parser for this section."""
        parser = DecapitatedXIParser()
        cls.add_to_inner(parser)
        return parser


__all__ = ["XI", "XF", "XG", "XN", "Xh", "File", "Raster", "XH", "DecapitatedXIParser"]
