# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""These are common interfaces."""
from abc import ABC, abstractproperty
from ..lazy import AIElement


class EndOfPath(AIElement, ABC):
    """This is common for ending a path."""

    @abstractproperty
    def close(self) -> bool:
        """Should the path be closed?"""
        return False

    @abstractproperty
    def fill(self) -> bool:
        """Should the path be filled?"""
        return False

    @abstractproperty
    def stroke(self) -> bool:
        """Should there be a stroke around the path?"""
        return False


__all__ = ["EndOfPath"]
