# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""All the groups, layers and so on can be recursively nested.

This contains a class to use for that purpose.
"""
from ..lazy import CollectedSection, CommandParser
from .. import objects


class ObjectSection(CollectedSection):
    """Base class for sections that contain objects.

    You can parse all objects into a section with this.
    """

    @classmethod
    def add_to_inner(self, parser: CommandParser):
        """Add all object definitions to the parser for the inside of this section."""
        parser.add_spec(objects.objects)  # type: ignore


__all__ = ["ObjectSection"]
