# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create a simple parser for the AI file's Prolog Section.

"""
from .lazy import CollectedSection, StartOfLineSection, AIElement, CommandParser


class IncludeResource(AIElement):
    """%%IncludeResource: procset"""

    spec = StartOfLineSection(
        "%%IncludeResource:",
        lambda line: line.startswith("%"),
        continue_after_last_tested_line=False,
    )


class Prolog(CollectedSection):
    """The prolog

    Example:

        %%BeginProlog
        ...
        %%EndProlog

    See AI Spec page 14.
    """

    spec = StartOfLineSection(
        "%%BeginProlog", "%%EndProlog", include_last_tested_line=True
    )

    @classmethod
    def add_to_inner(self, parser: CommandParser):
        """Add the procsets to the parser."""
        parser.add_spec(IncludeResource)
        parser.ignore_line_start("%%EndProlog")


__all__ = ["Prolog"]
