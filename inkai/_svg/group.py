# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Conversion of AI groups u...U to SVG <g> groups."""

from .interpreter.mapping import on_element
from .interpreter.modification_factory import ModificationFactory
from ..parser.objects.specified_1998 import Group as AIGroup
from inkex import Group as SVGGroup


class Group(ModificationFactory):
    """Group objects."""

    @on_element(AIGroup)
    def enter_the_group(self, ai_group: AIGroup):
        """Enter the group and add all objects in it as children."""
        if not ai_group.children or self.state.is_creating_a_compound_path():
            return
        group = SVGGroup()
        self.recurse(group, ai_group.children)


__all__ = ["Group"]
