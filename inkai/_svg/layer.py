# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Conversion of AI layers to SVG layers."""

from .interpreter.mapping import on_element
from .interpreter.modification_factory import ModificationFactory
from ..parser.objects.layer import Layer as AILayer
from inkex import Layer as SVGLayer


class Layer(ModificationFactory):
    """Create layers."""

    @on_element(AILayer)
    def enter_the_layer(self, ai_layer: AILayer):
        """Enter the layer and add all objects in it as children."""
        self.recurse(SVGLayer.new(ai_layer.name), ai_layer.children)
