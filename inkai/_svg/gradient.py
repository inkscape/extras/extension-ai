# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This creates gradients."""
from inkai._svg.interpreter import ModificationFactory, on_element
from inkai.parser.objects.gradient_instance import Gradient


class Gradients(ModificationFactory):
    """Create gradients."""

    @on_element(Gradient)
    def recurse_into_gradient_instance(self, gradient: Gradient):
        """We need the children of the gradient instance."""
        for child in gradient.children:
            self.interpreter.interpret(child)


__all__ = ["Gradients"]
