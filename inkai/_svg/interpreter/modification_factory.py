# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

""""""
from __future__ import annotations
from inkai.parser.lazy import AIElement
from inkai.parser.objects import ObjectSection  # type: ignore
from inkai._svg import modification
from inkai._svg.interfaces import IInterpreter, ISelector
from inkai._svg.interpreter.interpreters import InterpreterAdapter
from inkai._svg.interpreter.mapping import AnnotatedInterpreterMapElement, on_element
from inkai._svg.interpreter.observer import (
    ModificationSubject,
    PrintModifications,
)
from inkai._svg.interpreter.test import TestInterpretationResult
from inkai._svg.modification import Modification, ModificationChain
from inkai._svg.svg_builder import SVGElement
from inkai._svg.interpreter.ai_state.static import StaticState
from typing import Sequence
from .observer import ObserverProxy


class ModificationFactory(AnnotatedInterpreterMapElement, ModificationSubject):
    """Create SVG modifications from AIElements.
    This factory recieves the AIElements and creates modifications.
    """

    def __init__(self):
        """Create a new modification factory.
        This uses the template method pattern:
        1. initialize_modification() is called once to initialize the state
           that is remembered throughout the whole process of interpretation.
        2. reset_modification() is called to initialize the state that
           is reset whenever a modification occurs.
        When the factory enters 'production':
        - submit_modification(...)
          calls reset_modification
        """
        super(ModificationFactory, self).__init__()

        self.initialize_state()
        self.initialize_modification()
        self.reset_modification()

    def initialize_state(self):
        """Initialize the state for the whole interpretation."""

    def initialize_modification(self):
        """Initialize the state for the modifications of the SVG."""

    def reset_modification(self):
        """This resets the state of the modification."""

    def submit_modification(self, modification: Modification):
        """Send a modification off.
        This also resets the state.
        """
        self.notify_observers(modification)
        self.reset_modification()

    def recurse(self, parent: SVGElement, children: Sequence[AIElement]):
        """Recurse into the children.
        - parent is the parent of the elements created from the children.
        """
        self.submit_modification(modification.AddAndEnterParent(parent))
        # print(f"{self.__class__.__name__}.recurse({parent}, {children})")
        for child in children:
            self.interpreter.interpret(child)
        self.submit_modification(modification.ExitParent(parent))

    def generate_modifications_for(
        self, ai_objects: str, **state
    ) -> TestInterpretationResult:
        """Interpret the AIElement only with this strategy and return the result.
        This is for testing: The factory is tested in isolation.

        The state overrides the IAIState of the interpretation.
            is_a=True means state.is_a() == True
        """
        section = ObjectSection.collect_from_string(ai_objects)
        interpreter = InterpreterAdapter(self, StaticState(state))
        modifications = ModificationChain()
        self.attach(modifications)
        self.attach(PrintModifications())
        for child in section.children:
            interpreter.interpret(child)
        return TestInterpretationResult(modifications.modifications, self)

    def __add__(self, other: ModificationFactory) -> CombinedFactory:
        """Add this factory to another."""
        return other.combined_with(self)

    def combined_with(self, other: ModificationFactory) -> CombinedFactory:
        """Return this factory combined with the other."""
        return CombinedFactory(self, other)


class CombinedFactory(ModificationFactory, ObserverProxy):
    """Combine several factories into one."""

    def __init__(self, *factories: ModificationFactory):
        """Combine modification factories."""
        super().__init__()
        for factory in factories:
            self._category_map.add(factory)
            factory.attach(self)


__all__ = ["ModificationFactory", "CombinedFactory", "on_element"]
