# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

""""""
from __future__ import annotations
from inkai.parser.lazy import AIElement, Category
from ..interfaces import IInterpreter, IElementInterpreter, IAIState
from .ai_state.invalid import InvalidAIState
from .selector import StateSelector


class InvalidInterpreter(IInterpreter):
    """This raises a RuntimeError."""

    def interpret(self, ai_element: AIElement):
        """This one cannot be used for interpretation."""
        raise RuntimeError("You can not use this outside of the interpretation.")


class InterpreterAdapter(IInterpreter):
    """Adapt the interpretation strategy."""

    def __init__(
        self,
        element_interpreter: IElementInterpreter,
        ai_state: IAIState = InvalidAIState(),
    ):
        """Create a new svg interpreter."""
        self._element_interpreter = element_interpreter
        self._state = ai_state

    def interpret(self, ai_element: AIElement):
        """Interpret the AI element."""
        for category in ai_element.interpreter_categories:
            self._element_interpreter.interpret_element(
                StateSelector(category, self._state), ai_element, self
            )


__all__ = ["InvalidInterpreter", "InterpreterAdapter"]
