# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This module contains the selectors to match AIElements and state
to the methods being executed during interpretation.
"""
from inkai.parser.lazy import Category
from inkai._svg.interfaces import IAIState
from ..interfaces import ISelector
from .ai_state.invalid import InvalidAIState


class CategorySelector(ISelector):
    """This does not really have a state."""

    def __init__(self, category: Category):
        """Create a new category selector."""
        self._category = category

    @property
    def ai_state(self) -> IAIState:
        """The state is a null state."""
        return InvalidAIState()

    @property
    def category(self) -> Category:
        """This is the category this is initialized with."""
        return self._category


class StateSelector(CategorySelector):
    """This is a selector with a valid state."""

    def __init__(self, category: Category, state: IAIState):
        super().__init__(category)
        self._state = state

    @property
    def ai_state(self) -> IAIState:
        """A valid state of the AI state machine."""
        return self._state


__all__ = ["CategorySelector", "InvalidAIState", "StateSelector"]
