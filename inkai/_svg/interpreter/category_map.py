# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

""""""
from __future__ import annotations
from inkai._svg.interfaces import IElementInterpreter, IInterpreter, ISelector
from collections import defaultdict
from typing import Dict
from inkai.parser.lazy import AIElement, Category
from inkai._svg.interpreter.chain import ChainElementInterpreter
from ..interfaces import IElementInterpreter, ICategoryMap


class CategoryMap(ICategoryMap):
    """This maps AI element interpretation to modifications from a ModificationFactory."""

    def __init__(self) -> None:
        """Create a mapping."""
        super().__init__()
        self.map: Dict[Category, ChainElementInterpreter] = defaultdict(
            ChainElementInterpreter
        )

    def add_to_category(
        self, category: Category, element_interpreter: IElementInterpreter
    ) -> None:
        """Add a mapping to handle interpretation."""
        print("add_to_category", category, element_interpreter)
        self.map[category].add(element_interpreter)

    def interpret_element(
        self, selector: ISelector, ai_element: AIElement, interpreter: IInterpreter
    ) -> None:
        """Choose the correct interpreters to give the interpretation to."""
        element_interpreter = self.map[selector.category]
        element_interpreter.interpret_element(selector, ai_element, interpreter)

    def add_to_map(self, interpreter_map: ICategoryMap) -> None:
        """Add this factory to the mapping."""
        for category, element_interpreter in self.map.items():
            interpreter_map.add_to_category(category, element_interpreter)


__all__ = ["CategoryMap"]
