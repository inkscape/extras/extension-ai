# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This is an adapter to make the state machine compatible to subscribe to AIElements."""
from statemachine.event import Event
from statemachine.exceptions import TransitionNotAllowed
from inkai.parser.lazy import AIElement
from inkai._svg.interfaces import IElementInterpreter, IInterpreter, ISelector
from .stm import AISTM, TransitionMap
from ...interfaces import IElementInterpreter
from ...exceptions import IncompleteStatemachine


class AISTMAdapter(IElementInterpreter):
    """An adapter for the state machine."""

    def __init__(self, stm: AISTM, transitions: TransitionMap):
        """Create an adapter to invoke change in the state machine when receiving AI elements."""
        self._stm = stm
        self._transitions = transitions

    def interpret_element(
        self, selector: ISelector, ai_element: AIElement, interpreter: IInterpreter
    ) -> None:
        """Interpret the element."""
        transition = self._transitions.get(selector.category)
        if transition is not None:
            if self._stm.debug:
                print(f"transition from {ai_element}", end=":\t")
            try:
                self._stm.do(transition)
            except TransitionNotAllowed as transition_not_allowed:
                raise IncompleteStatemachine(
                    ai_element,
                    Event(transition_not_allowed.event),
                    transition_not_allowed.state,
                )


__all__ = ["AISTMAdapter", "TransitionMap"]
