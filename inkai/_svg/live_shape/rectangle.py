# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create SVG paths."""
from ..interpreter import on_element, ModificationFactory
from typing import List, Optional
from ..coordinates import CoordinateTransform
import inkex
from inkai.parser.objects.new import ArtDictionary
from inkai.parser.objects.live_shapes import Rectangle
import math
from ..modification import ReplaceLastChild, AddDef


class LiveRectangle(ModificationFactory):
    """Create SVG paths and shapes."""

    def __init__(self, transform: CoordinateTransform):
        """Create paths from AIElements."""
        super().__init__()
        self.transform = transform
        self.id = 0

    @on_element(ArtDictionary)
    def convert_path_to_live_shape(self, art_dict: ArtDictionary):
        """Paths are created as a fallback if we do not know the special shape.

        If we however encounter a live shape,
        we might be able to enhance the path or replace it.
        """
        art_dict.live_shape.convert(self)

    def convert_to_rectangle(self, rect: Rectangle):
        """Convert this path into a rectangle."""
        if rect.has_equal_corners():
            self.submit_modification(
                ReplaceLastChild(self.create_regular_rectangle(rect))
            )
        else:
            svg_rect = self.create_irregular_rectangle(rect)
            path_effect = self.create_irregular_rectangle_path_effect(rect)
            svg_rect.set("inkscape:path-effect", "#" + path_effect.get_id())
            self.submit_modification(ReplaceLastChild(svg_rect))
            self.submit_modification(AddDef(path_effect))

    def _set_rect_attributes(self, element: inkex.BaseElement, rect: Rectangle):
        """Set the attributes all results should have."""
        top_left = self.transform.from_canvas(
            rect.center.x - rect.width / 2, rect.center.y - rect.height / 2
        )
        element.set("x", top_left[0])
        element.set("y", top_left[1])
        element.set("width", self.transform.large_canvas_scale * rect.width)
        element.set("height", self.transform.large_canvas_scale * rect.height)
        if rect.angle != 0:
            center = self.transform.from_canvas(*rect.center)
            element.set(
                "transform",
                inkex.Transform(
                    f"rotate({math.degrees(rect.angle)} {center[0]} {center[1]})"
                ),
            )

    def create_regular_rectangle(self, rect: Rectangle) -> inkex.Rectangle:
        """Create a rectangle with all corners the same."""
        result = inkex.Rectangle()
        self._set_rect_attributes(result, rect)
        if rect.corners[0].CornerRadius != 0:
            result.set(
                "ry", rect.corners[0].CornerRadius * self.transform.large_canvas_scale
            )
        return result

    def create_irregular_rectangle(self, rect: Rectangle) -> inkex.Rectangle:
        r"""Create a rectangle shape where the corners differ.

        Create a rectangle:

           +-----+
         /        \
        +          +
        |          |
        +          +
         \        /
          +------+
        """
        result = inkex.PathElement()
        self._set_rect_attributes(result, rect)
        result.set("sodipodi:type", "rect")
        corners = rect.corners
        x, y = self.transform.from_canvas(*rect.center)
        height = rect.height * self.transform.large_canvas_scale
        width = rect.width * self.transform.large_canvas_scale
        R = [
            corner.CornerRadius * self.transform.large_canvas_scale
            for corner in corners
        ]
        # print(width, height, R)
        result.set(
            "d",
            f"m {round(x - width/2 + R[0], 8)},{round(y - height/2, 8)} "
            f"h {width - R[0] - R[1]} "
            f"a {R[1]},{R[1]} 45 0 1 {R[1]},{R[1]} "
            f"v {height - R[1] - R[2]} "
            f"a {R[2]},{R[2]} 135 0 1 {-R[2]},{R[2]} "
            f"h -{width - R[2] - R[3]} "
            f"a {R[3]},{R[3]} 45 0 1 {-R[3]},{-R[3]} "
            f"v -{height - R[3] - R[0]} "
            f"a {R[0]},{R[0]} 135 0 1 {R[0]},{-R[0]} "
            f"z ",
        )
        # print(result.get("d"))
        return result

    def create_irregular_rectangle_path_effect(
        self, rect: Rectangle
    ) -> inkex.PathEffect:
        """Create the path effect for the irregular rectangle.

        This path effect is used to edit the corners in Inkscape.
        """
        radius = [
            self.transform.large_canvas_scale * corner.CornerRadius
            for corner in rect.corners
        ]
        params = f"F,0,0,1,0,{radius[0]},0,1 @ F,0,0,1,0,{radius[1]},0,1 @ F,0,0,1,0,{radius[2]},0,1 @ F,0,0,1,0,{radius[3]},0,1"
        self.id += 1
        return inkex.PathEffect.new(
            effect="fillet_chamfer",
            is_visible="true",
            lpeversion="1",
            satellites_param=params,  # Inkscape 1.2
            nodesatellites_param=params,  # Inkscape 1.3
            unit="px",
            method="auto",
            mode="F",
            radius="0",
            chamfer_steps="1",
            flexible="false",
            use_knot_distance="true",
            apply_no_radius="true",
            apply_with_radius="true",
            only_selected="false",
            hide_knots="false",
            id=f"rectangle-path-effect-{self.id}",
        )


__all__ = ["PathCommands", "Path"]
