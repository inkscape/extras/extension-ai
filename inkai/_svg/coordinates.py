# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Transform coordinates from one system to another."""

from __future__ import annotations
from typing import Union, Sequence, Tuple
from ..parser.document import AIDocumentInformation
from collections import namedtuple

Coordinates = Sequence[float]


class TestTransform:
    """Static example transformation of the coordinates."""

    @staticmethod
    def from_drawing(*xy: float) -> Coordinates:
        """Drawing coordinates -> SVG coordinates"""
        xy_iter = iter(xy)
        result = []
        for x in xy_iter:
            y = next(xy_iter)
            result.append(-x if x != 0 else 0)
            result.append(-y if y != 0 else 0)
        return result

    def from_canvas(self, *xy: float) -> Coordinates:
        """coordinate transform from canvas coordinate system to SVG coordinate system."""
        xy_iter = iter(xy)
        result = []
        for x in xy_iter:
            y = next(xy_iter)
            result.append(x * self.large_canvas_scale + 1000)
            result.append(y * self.large_canvas_scale + 1000)
        return result

    large_canvas_scale = 2


class SVGTransform:
    """Transform the coordinate systems to the one used in the SVG file."""

    def __init__(self, info: AIDocumentInformation):
        """Create an SVGTransform."""
        self._large_canvas_scale = info.large_canvas_scale
        page = info.drawing_first_page_position
        self._ruler_x = page.left
        self._ruler_y = page.top
        cro = info.canvas_ruler_origin
        if cro is None:
            self._has_canvas = False
            self._canvas_ruler_x = self._canvas_ruler_y = 0
        else:
            self._has_canvas = True
            self._canvas_ruler_x = cro.x
            self._canvas_ruler_y = cro.y
        print(page)

    def from_drawing(self, *xy: float) -> Coordinates:
        """Drawing coordinates -> SVG coordinates"""
        return [
            f * self._large_canvas_scale * (-1 if i % 2 else 1)
            - (self._ruler_y if i % 2 else self._ruler_x)
            for i, f in enumerate(xy)
        ]

    @property
    def large_canvas_scale(self) -> float:
        """The scaling of e.g. width and height from canvas coordinates to SVG coordinates."""
        return self._large_canvas_scale

    def from_canvas(self, *xy: float) -> Coordinates:
        """Canvas coordinates -> SVG coordinates. SVG places the first page at 0,0"""
        if not self._has_canvas:
            raise ValueError("This file does not have a canvas_ruler_origin.")
        return [
            (
                f
                - (
                    self._canvas_ruler_y + self._ruler_y
                    if i % 2
                    else self._canvas_ruler_x + self._ruler_x
                )
            )
            * self._large_canvas_scale
            for i, f in enumerate(xy)
        ]


CoordinateTransform = Union[TestTransform, SVGTransform]

__all__ = ["TestTransform", "CoordinateTransform", "SVGTransform"]
