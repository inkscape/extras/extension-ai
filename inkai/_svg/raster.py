# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Place raster images in SVG files."""
import io
import base64
from .interpreter.mapping import on_element

import inkex
from PIL import Image as pimage

from inkai._warnings import NotWorthImplementingWarning
from .interpreter import ModificationFactory, on_element
from inkai.parser.objects.image import XI, Raster as RasterParsed  # type: ignore
from inkai.parser.objects import Data  # type: ignore
from inkai._svg.modification import AddChild


class Raster(ModificationFactory):
    xi: XI

    def __init__(
        self,
    ):
        super().__init__()

    @on_element(RasterParsed)
    def enter_the_raster(self, raster: RasterParsed):
        """Enter the layer and add all objects in it as children."""
        for child in raster.children:
            self.interpreter.interpret(child)

    @on_element(XI)
    def parse_xi(self, xi: XI):
        self.result = inkex.Image()
        self.xi = xi

    def get_type(self) -> str:
        if self.xi.image_type == 3:
            if self.xi.alpha_channel_count == 1:
                format = "RGBA"
            else:
                format = "RGB"
        elif self.xi.image_type == 1:
            format = "L"
        return format

    @on_element(Data)
    def parse_data(self, data: Data):
        # Perform a few safety checks first.
        # This must be an 8-bit per channel image.
        if self.xi.bits != 8:
            raise NotWorthImplementingWarning("non-8 bit per channel image")
        # The data needs to have purpose "XI".
        if data.purpose != "XI":
            return

        h, w = self.xi.h, self.xi.w
        type = self.get_type()
        # We expect the size to be correct.
        assert h * w * len(type) == len(
            data.data
        ), f"Bad bitmap size, {len(data.data)} / {h * w * len(type)}"

        # Create a pillow image and save to PNG (lossless)
        image = pimage.frombytes(type, (h, w), data.data)
        with io.BytesIO() as output:
            with io.BytesIO() as encoded:
                image.save(output, format="PNG")
                output.seek(0)

                base64.encode(output, encoded)
                self.result.set(  # type: ignore
                    "xlink:href", "data:image/png;base64," + encoded.getvalue().decode()
                )

        self.result.transform = (  # type: ignore
            inkex.Transform(scale=(1, -1))
            @ inkex.Transform(
                matrix=(
                    self.xi.a,
                    self.xi.b,
                    self.xi.c,
                    self.xi.d,
                    self.xi.tx,
                    self.xi.ty,
                )
            )
            @ inkex.Transform(scale=(1, -1))
        )
        self.submit_modification(AddChild(self.result))
        self.result = None


__all__ = ["Raster"]
