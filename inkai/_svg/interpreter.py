# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This is an interpreter for AIElements coming from the parser to create SVGs."""

from typing import Union, List, Sequence, Optional

try:
    from typing import TypeAlias  # type: ignore
except ImportError:
    from typing_extensions import TypeAlias
from inkai.parser.lazy import AIElement
from ..interpreter import Strategy, AIInterpreter
from inkex import BaseElement, SvgDocumentElement
from inkex.base import SvgOutputMixin
from lxml import etree

SVGResult: TypeAlias = etree.ElementBase


class State:
    """This is the common interpreter state that can be accessed by all SVGStrategies."""

    def __init__(self):
        """Create a new SVG state."""
        self._result: SVGResult = SvgOutputMixin.get_template(width=0, height=0)
        self._parents: List[BaseElement] = [self.svg]

    def append_child(self, child: BaseElement):
        """Add the child to the current container (SVG, Layer, Group, ...)."""
        self._parents[-1].append(child)

    def append_def(self, definition: BaseElement):
        """Add a definition to the SVG defs Section."""
        self.svg.defs.append(definition)

    @property
    def last_child(self) -> BaseElement:
        """The last child element."""
        if len(self._parents[-1]):
            return self._parents[-1][-1]
        return self._parents[-1]

    @last_child.setter
    def last_child(self, child: BaseElement):
        """Replace the last child with another element."""
        if len(self._parents[-1]):
            self._parents[-1][-1].replace_with(child)
        else:
            self._parents[-1].append(child)

    def enter(self, container: BaseElement):
        """Set the container as the current container to add elements into."""
        self._parents.append(container)

    def exit(self):
        """Leave the current parent and go one level higher."""
        self._parents.pop()
        assert self._parents, "We always need a parent for the children!"

    @property
    def svg(self) -> SvgDocumentElement:
        """The SVG root document."""
        return self._result.getroot()


class SVGInterpreter(AIInterpreter):
    """An interpreter of the AIElements specially designed for SVG files."""

    def __init__(self):
        """Create a new svg interpreter."""
        super().__init__()
        self._state = State()

    @property
    def state(self) -> State:
        """The state of the current interpretation."""
        return self._state


class SVGStrategy(Strategy):
    """Create SVG elements from AIElements."""

    interpreter: SVGInterpreter

    @property
    def state(self) -> State:
        return self.interpreter.state

    def interpret(self, ai_element: str, interpreter: Optional[AIInterpreter] = None):
        """Interpret the AIElement only with this strategy and return the result.

        If the first argument is a string, the collector will collect its content from it.
        """
        return super().interpret(
            ai_element,
            interpreter=(SVGInterpreter() if interpreter is None else interpreter),
        )


class SVGElement(SVGStrategy):
    """This is a strategy that creates SVG elements."""

    defs: List[BaseElement] = []

    def recurse(self, parent: BaseElement, children: Sequence[AIElement]):
        """Recurse into the children.

        - parent is the parent of the elements created from the children.
        """
        self.state.enter(parent)
        for child in children:
            self.interpreter.interpret(child)
        self.state.exit()
        self.result = parent
        self.add_child_to_svg()

    def add_child_to_svg(self):
        """This adds the child to the SVG state.

        This also calls init() again to get ready for construction again.
        """
        self.state.append_child(self.child)
        self.init()

    @property
    def child(self) -> BaseElement:
        """Return the constructed SVG element."""
        return self.result


__all__ = [
    "SVGElement",
    "SVGStrategy",
    "SVGInterpreter",
    "Strategy",
    "State",
    "AIInterpreter",
]
