# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Create SVG paths."""
from inkai.interpreter import AIInterpreter
from .interpreter import SVGStrategy, Strategy, SVGElement
from typing import List, Sequence, Optional
from .coordinates import CoordinateTransform
from ..parser.objects.path import (
    PathOperator,
    PathRender,
    CompoundPath,
    PathAttributes,
    Xy,
)
from inkex import PathElement, paths, Style
import inkex
from inkai.parser.color import Color


class PathCommands(SVGStrategy):
    """Convert AI Path Construction Operators for SVG Path commands.

    See AI Spec page 56.
    See https://gitlab.com/inkscape/extras/extension-ai/-/merge_requests/61#note_1384351623
    See https://inkscape.gitlab.io/extensions/documentation/_modules/inkex/paths.html#Path

    Attributes:

    - operator_mapping - a mapping from AI operator to inkex path command

    Equivalence:

        x y m             = x y m
        x y x2 y2 x3 y3 c = x2 y2 x3 y3 v

        x y m               = x y m
        x1 y1 x3 y3 x3 y3 c = x1 y1 x3 y3 y

    """

    def __init__(self, transform: CoordinateTransform):
        """Create path commands in a transformed space."""
        super().__init__()
        self.transform = transform
        self.operator_mapping = {
            "c": paths.Curve,
            "m": paths.Move,
            "l": paths.Line,
            "y": lambda x1, y1, x3, y3: paths.Curve(x1, y1, x3, y3, x3, y3),
            "v": self.add_v_path_command,
        }

    def init(self):
        """Initialize the PathCommands."""
        self.commands = paths.Path()
        self._last_point: Sequence[float] = (0.0, 0.0)  # never used

    @property  # type: ignore
    def result(self) -> paths.Path:
        """The collected commands."""
        return self.commands

    @SVGStrategy.on(PathOperator)
    def add_as_path_command(self, operator: PathOperator):
        """Populate the list of path commands."""
        points = self.transform.from_drawing(*operator.xy)
        Command = self.operator_mapping[operator.command.lower()]
        self.commands.append(Command(*points))
        self._last_point = points[-2:]

    def add_v_path_command(self, x2, y2, x3, y3):
        """The v path command uses the last point for bezier calculation."""
        return paths.Curve(self._last_point[0], self._last_point[1], x2, y2, x3, y3)

    @SVGStrategy.on(PathRender)
    def close(self, render: PathRender):
        """Close the path if necessary."""
        if render.close:
            self.commands.append(paths.ZoneClose())


class NodeTypes(Strategy):
    """Collect the sodipodi:nodetypes attribute.

    c = corner node
    s = smooth node
    z = symmetric
    a = auto smooth
    """

    def init(self):
        """Initialize."""
        self.nodetypes = ""

    @Strategy.on(PathOperator)
    def add_node_type(self, operator: PathOperator):
        """Add the corner/smooth information."""
        self.nodetypes += "c" if operator.is_corner() else "s"

    @Strategy.on(PathRender)
    def close(self, render: PathRender):
        """Close the path if necessary."""
        if render.close:
            self.nodetypes += self.nodetypes[:1]

    @property  # type: ignore
    def result(self) -> str:  # type: ignore
        """Return the nodetypes (corner/smooth)."""
        return self.nodetypes


class Path(SVGElement):
    """Create SVG paths and shapes."""

    def __init__(self, transform: CoordinateTransform):
        """Create paths from AIElements."""
        super().__init__()
        self.commands = PathCommands(transform)
        self.nodetypes = NodeTypes()
        self.style = PathStyle()
        self.transform = transform

    def init(self):
        """Initialize the current parsing state."""
        self.merge = (
            0  # counter and boolean value for whether to merge the paths into one
        )

    def add_to(self, interpreter: AIInterpreter):
        """Add to the interpreter."""
        # The inner ones have to go first.
        # This way they are evaluated before and we can use their result.
        self.commands.add_to(interpreter)
        self.nodetypes.add_to(interpreter)
        self.style.add_to(interpreter)
        super().add_to(interpreter)

    @property  # type: ignore
    def result(self) -> PathElement:
        """Return the SVG path."""
        path = PathElement.new(self.commands.result)
        path.set("sodipodi:nodetypes", self.nodetypes.result)
        path.set("style", self.style.result)
        return path

    @Strategy.on(CompoundPath)
    def merge_paths_into_one(self, element: CompoundPath):
        """Merge several paths into one.

        See AI Spec page 59.
        """
        self.merge += 1
        for child in element.children:
            self.interpreter.interpret(child)
        self.merge -= 1
        if not self.merge:
            self.add_child_to_svg()

    @Strategy.on(PathRender)
    def finish_path(self, render: PathRender):
        """Finish the path off and add it to the SVG."""
        if not self.merge:
            self.add_child_to_svg()


class PathStyle(Strategy):
    """Create the inkex.Style of a path."""

    def init(self):
        """Initialize the style."""
        self.style = Style(
            {
                "fill": "none",
                "stroke": "#000000",
                "stroke-opacity": "1",
                "stroke-width": "0.264583px",
                "stroke-linejoin": "miter",
                "stroke-linecap": "butt",
            }
        )

    @Strategy.on(PathAttributes)
    def set_path_attributes(self, pa: PathAttributes):
        """Set the attributes of a path."""
        if pa.line_join is not None:
            self.style["stroke-linejoin"] = ["miter", "round", "bevel"][pa.line_join]
        if pa.line_cap is not None:
            self.style["stroke-linecap"] = ["butt", "round", "square"][pa.line_cap]
        if pa.miter_limit is not None:
            self.style["stroke-miterlimit"] = str(pa.miter_limit)
        if pa.line_width is not None:
            self.style["stroke-width"] = pa.line_width

    @Strategy.on(PathRender)
    def fill_and_stroke(self, render: PathRender):
        """Fill and/or close the path."""
        self.style["fill-opacity"] = "01"[render.fill]
        if not render.stroke:
            self.style["stroke-opacity"] = "0"

    @Strategy.on(Xy)
    def set_opacity(self, xy: Xy):
        """Set the opacity of the path."""
        self.style["stroke-opacity"] = xy.stroke_opacity

    @Strategy.on(Color)
    def color_the_path(self, color: Color):
        """Color the path's stroke or fill."""
        attr = ("fill", "stroke")[color.is_stroke()]
        print(f"alpha = {color.alpha}")
        rgb = color.rgb_string
        if rgb:
            self.style[attr] = rgb

    @property  # type: ignore
    def result(self) -> Style:  # type: ignore
        """Return the style of the path."""
        return self.style


__all__ = ["PathCommands", "NodeTypes", "Path"]
