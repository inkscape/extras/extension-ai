# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This contains interfaces for decoupling."""
from __future__ import annotations
from abc import ABC, abstractmethod, abstractproperty
from inkai.parser.lazy import AIElement, Category
from .svg_builder import SVGBuilder
from typing import List, Iterator


class IAIState(ABC):
    """This is the base class for the state of the interpretation for AI."""

    @abstractmethod
    def is_creating_a_path(self) -> bool:
        """Whether the interpreter is inside a path."""

    @abstractmethod
    def is_creating_a_raster_image(self) -> bool:
        """Whether the interpreter is inside a raster image."""

    @abstractmethod
    def is_creating_a_compound_path(self) -> bool:
        """Whether the interpreter is inside a compound path."""


class ISelector(ABC):
    """This is a selector for matching code execution to AIElements and interpreter state."""

    @abstractproperty
    def category(self) -> Category:
        """The category that this selector matches."""

    @abstractproperty
    def ai_state(self) -> IAIState:
        """The state of AI interpretation."""

    def __eq__(self, other) -> bool:
        """self == other"""
        return isinstance(other, ISelector) and self.category == other.category

    def __hash__(self) -> int:
        """hash(self)"""
        return hash(self.category)

    def __repr__(self):
        """repr(self)"""
        return f"{self.__class__.__name__}({self.category.__name__})"


class IInterpreter(ABC):
    """An interpreter for AIElements."""

    @abstractmethod
    def interpret(self, ai_element: AIElement) -> None:
        """Interpret the AIElement."""


class IElementInterpreter(IInterpreter):
    """This is the basic interface to handle AIElements."""

    @abstractmethod
    def interpret_element(
        self, selector: ISelector, ai_element: AIElement, interpreter: IInterpreter
    ) -> None:
        """Interpret an AIElement given the selector.

        The execution can be passed back to the interpreter.
        """

    def interpret(self, ai_element: AIElement) -> None:
        """Use an interpreter adapter to interpret this element."""
        from .interpreter.interpreters import InterpreterAdapter

        InterpreterAdapter(self).interpret(ai_element)


class IInterpreterMapElement(IElementInterpreter):
    """Handle interpretation but dispatch it more efficiently."""

    def add_to_map(self, interpreter_map: ICategoryMap) -> None:
        """Add this element to the interpreter map."""
        self.as_category_map().add_to_map(interpreter_map)

    @abstractmethod
    def as_category_map(self) -> ICategoryMap:
        """Return this as a mapping."""


class ICategoryMap(IInterpreterMapElement):
    """This is a mapping to select the interpreter."""

    def add(self, element: IInterpreterMapElement) -> None:
        """Add an element."""
        element.add_to_map(self)

    @abstractmethod
    def add_to_category(
        self, category: Category, element_interpreter: IElementInterpreter
    ) -> None:
        """Add an element interpreter for a specific selector."""

    def as_category_map(self) -> ICategoryMap:
        """This is an interpreter map."""
        return self

    @abstractmethod
    def add_to_map(self, interpreter_map: ICategoryMap) -> None:
        """Add all the content to the other interpreter map."""


class IModificationObserver(ABC):
    """This is a observer of the factory which receives the modifications.
    This follows the observer pattern,
    see https://en.wikipedia.org/wiki/Observer_pattern.
    """

    def new_modification(self, modification: IModification) -> None:
        """Receive a new notification."""


class IModification(ABC):
    """Modify the SVG Builder.

    This follows the Command Pattern.
    See https://en.wikipedia.org/wiki/Command_pattern.

    The changes are accumulated and when ready, all done
    to the SVG.
    """

    @abstractmethod
    def modify(self, svg_builder: SVGBuilder) -> None:
        """Modify the SVG Builder"""

    def append_to(self, modifications: IModificationChain):
        """Append this to a chain of modifications."""
        modifications.new_modification(self)


class IModificationChain(IModification, IModificationObserver):
    """A chain of modifications to apply."""

    @abstractproperty
    def modifications(self) -> List[IModification]:
        """Return the series of modifications."""

    def append(self, modification: IModification) -> None:
        """Append a modification."""
        modification.append_to(self)

    def append_to(self, modifications: IModificationChain) -> None:
        """Append this chain to the end of the modifications."""
        for modification in self.modifications:
            modifications.append(modification)

    def modify(self, svg_builder: SVGBuilder) -> None:
        """Apply all collected modifications."""
        for modification in self.modifications:
            modification.modify(svg_builder)

    def __iter__(self) -> Iterator:
        """Iterate over all the elements."""
        return iter(self.modifications)


class IModificationSubject(ABC):
    """This is observed to get new modifications."""

    @abstractmethod
    def attach(self, modification_observer: IModificationObserver) -> None:
        """Attach a mofification observer."""

    @abstractmethod
    def notify_observers(self, modification: IModification) -> None:
        """Notify the observers about a new modification."""


__all__ = [
    "IModification",
    "IModificationChain",
    "IModificationObserver",
    "IModificationSubject",
    "IElementInterpreter",
    "IInterpreter",
    "ICategoryMap",
    "IInterpreterMapElement",
    "ISelector",
]
