# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Style paths.

This is mostly defined by the AI Spec.
The old formats include the path styling.
"""
from inkai.parser.color import Color
from inkai.parser.objects.path import PathAttributes, PathRender, Xy
from inkai._svg.interpreter import ModificationFactory, on_element
from inkex import Style
from ..modification import AddChildAttributes


class PathStyle(ModificationFactory):
    """Create the inkex.Style of a path."""

    def initialize_modification(self):
        """Initialize the style.

        Styles never get reset.
        Their state always exist for the whole document,
        only being modified.
        """
        self.style = Style(
            {
                "fill": "none",
                "stroke": "#000000",
                "stroke-opacity": "1",
                "stroke-width": "0.264583px",
                "stroke-linejoin": "miter",
                "stroke-linecap": "butt",
            }
        )

    @on_element(PathAttributes)
    def set_path_attributes(self, pa: PathAttributes):
        """Set the attributes of a path."""
        if pa.line_join is not None:
            self.style["stroke-linejoin"] = ["miter", "round", "bevel"][pa.line_join]
        if pa.line_cap is not None:
            self.style["stroke-linecap"] = ["butt", "round", "square"][pa.line_cap]
        if pa.miter_limit is not None:
            self.style["stroke-miterlimit"] = str(pa.miter_limit)
        if pa.line_width is not None:
            self.style["stroke-width"] = pa.line_width

    @on_element(PathRender)
    def fill_and_stroke(self, render: PathRender):
        """Fill and/or close the path."""
        self.style["fill-opacity"] = "01"[render.fill]
        if not render.stroke:
            self.style["stroke-opacity"] = "0"
        self.submit_modification(AddChildAttributes(style=self.style))

    @on_element(Xy)
    def set_opacity(self, xy: Xy):
        """Set the opacity of the path."""
        self.style["stroke-opacity"] = xy.stroke_opacity

    @on_element(Color)
    def color_the_path(self, color: Color):
        """Color the path's stroke or fill."""
        attr = ("fill", "stroke")[color.is_stroke()]
        print(f"alpha = {color.alpha}")
        rgb = color.rgb_string
        if rgb:
            self.style[attr] = rgb


__all__ = ["PathStyle"]
