# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later


"""Lean structure around operators."""

from typing import Optional, Sequence, List, Callable, Any, Type

try:
    from typing import TypeAlias
except ImportError:  # fallback for python < 3.10
    from typing_extensions import TypeAlias
from .streamreader import dec
from . import parse


class Operator:
    def __init__(self, command: bytes, tokens=None, is_pseudo_comment=False):
        self.is_pseudo_comment = is_pseudo_comment
        self.command: str = dec(command)
        self.tokens = tokens or []
        self.parse_tokens()

    def parse_tokens(self):
        pass


TokenParserType = Callable[[str], Any]
TokenParserList = Sequence[TokenParserType]


def generate_op(
    spec: str, doc: str, parsers=TokenParserList, base=Operator
) -> Type[Operator]:
    """Generate a command that is placed at the end of a line.

    - spec is the specification.
      Example: "name Ln"
    - doc is the docstring.
    - parsers is a list of parsers e.g. from .parse
    - base is the base class to use.

    Example for a one-line new command:

        Ln : TypeAlias = generate_op("name Ln", "See page 71 AI Spec.", [parse.ps])  # type: ignore

    Example for a more complex command:

        class Command(generate_op("name Ln", "See page 71 AI Spec.", [parse.ps]):
            "An example command."

            def method(self):
                return self.name.upper()

    """
    split_spec = spec.strip().split()
    command = split_spec[-1]
    names = split_spec[:-1]
    parsers = parsers[:]
    for i, name in enumerate(names):
        if name in ("[", "]"):
            parsers.insert(i, parse.ignore)
    inner = {
        "__doc__": f"{spec}\n\n{doc}",
        "token_parsers": parsers,
        "command": command,
    }
    assert len(names) == len(
        parsers
    ), f"The command should have the same number of parsers as it has named arguments in the spec: {len(names)} names != {len(parsers)} parsers"
    for index, name, parser in zip(range(len(names)), names, parsers):

        def get_from_tokens(self, parser=parser, token_index=index):
            return parser(self.tokens[token_index])

        get_from_tokens.__doc__ = f"the {name} argument of {command}"
        get_from_tokens.__name__ = name
        get_from_tokens.__annotations__["return"] = getattr(
            parser, "__annotations__", {}
        ).get("return", parser)
        inner[name] = property(get_from_tokens)
        assert not hasattr(
            Operator, name
        ), f"Operator.{name} exists. Please rename the argument."
    return type(command, (base,), inner)


class PathRenderOperator(Operator):
    """Rendering paths.

    See page 54,58 AI Spec.
    """

    @property
    def close(self) -> bool:
        """Should the path be closed?"""
        return self.command in "bfsN"

    @property
    def fill(self) -> bool:
        """Should the path be filled?"""
        return self.command in "fFbB"

    @property
    def stroke(self) -> bool:
        """Should there be a stroke around the path?"""
        return self.command in "sSbB"


class PathOperator(Operator):
    """Path rendering positions."""

    command: str

    def is_corner(self) -> bool:
        """Is this a corner?"""
        return self.command.isupper()

    def to_cubic(self, prev) -> Sequence[float]:
        if self.command in "lL":
            return [*prev, self.x, self.y, self.x, self.y]  # type: ignore
        if self.command in "cC":
            return [self.x1, self.y1, self.x2, self.y2, self.x3, self.y3]  # type: ignore
        if self.command in "vV":
            return [*prev, self.x2, self.y2, self.x3, self.y3]  # type: ignore
        if self.command in "yY":
            return [self.x1, self.y1, self.x3, self.y3, self.x3, self.y3]  # type: ignore
        raise ValueError("Bad path command")


MoveOperator: TypeAlias = generate_op(  # type: ignore
    "x y m",
    """The m operator is equivalent to the PostScript language moveto operator. It
changes the current point to x, y, omitting any connecting line segment. A
path must have m as its first operator.

AI Spec page 56.""",
    [parse.float, parse.float],
    base=PathOperator,
)

lineOperator = generate_op(
    "x y l",
    """The l (lowercase L) operator appends a straight line segment from the current
point to x, y. The new current point is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float],
    base=PathOperator,
)

LineOperator = generate_op(
    "x y L",
    """The L operator is similar to the l operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float],
    base=PathOperator,
)

curveOperator = generate_op(
    "x1 y1 x2 y2 x3 y3 c",
    """The c operator appends a Bézier curve to the path from the current point to
x3, y3 using x1, y1 and x2, y2 as the Bézier direction points. The new current
point is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

CurveOperator = generate_op(
    "x1 y1 x2 y2 x3 y3 C",
    """The C operator is similar to the c operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

Smooth1Operator = generate_op(
    "x2 y2 x3 y3 v",
    """The v operator adds a Bézier curve segment to the current path between the
current point and the point x3, y3, using the current point and then x2, y2 as the
Bézier direction points. The new current point is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)


Smooth2Operator = generate_op(
    "x2 y2 x3 y3 V",
    """The V operator is similar to the v operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

Smooth3Operator = generate_op(
    "x1 y1 x3 y3 y",
    """The y operator appends a Bézier curve to the current path between the current
point and the point x3, y3 using x1, y1 and x3, y3 as the Bézier direction points.
The new current point is x3, y3 and is a smooth point.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

Smooth4Operator = generate_op(
    "x1 y1 x3 y3 Y",
    """The Y operator is similar to the y operator, but the new current point is a
corner.

AI Spec page 57.""",
    [parse.float, parse.float, parse.float, parse.float],
    base=PathOperator,
)

LineJoinOperator = generate_op(
    "line_join j",
    """AI Spec page 55+.""",
    [parse.int],
)

LineCapOperator = generate_op(
    "line_cap J",
    """AI Spec page 55+.""",
    [parse.int],
)

MiterLimitOperator = generate_op(
    "miter_limit M",
    """AI Spec page 55+.""",
    [parse.float],
)

LineWidthOperator = generate_op(
    "line_width w",
    """AI Spec page 55+.""",
    [parse.float],
)

FlatnessOperator = generate_op(
    "flatness i",
    """AI Spec page 55+.""",
    [parse.float],
)

WindingOrderOperator = generate_op(
    "clockwise D",
    """AI Spec page 55+.""",
    [lambda token: not parse.int(token)],
)

FillRuleOperator = generate_op(
    "rule XR",
    """AI Spec page 59""",
    [lambda token: parse.int(token) == 1],
)


class StrokeDashOperator(Operator):
    """Example: [0 1] 0.5 d"""

    def parse_tokens(self):
        tokens = (
            b" ".join(self.tokens).replace(b"[", b"[ ").replace(b"]", b" ] ").split()
        )

        assert tokens[0] == b"[", tokens
        self.dashes = []
        for token in tokens[1:]:
            if token == b"]":
                break
            self.dashes.append(parse.float(token))
        self.phase = parse.float(tokens[-1])


class OverprintOperator(Operator):
    """Overprinting for paths.

    See page 63 AI Spec.
    """

    @property
    def overprint(self):
        """Whether to apply an overprint to the path."""
        return parse.bool(self.tokens[0])

    @property
    def fill(self):
        """Whether to apply the overprint value to the fill of the path."""
        return self.command == "O"

    @property
    def stroke(self):
        """Whether to apply the overprint value to the stroke of the path."""
        return self.command == "R"


LayerNameOperator = generate_op("name Ln", "See page 71 AI Spec.", [parse.string])
LayerPropertiesOperator = generate_op(
    "visible preview enabled printing dimmed has_multi_layer_masks color_index red green blue Lb",
    "See page 71 AI Spec.",
    [
        parse.bool,
        parse.bool,
        parse.bool,
        parse.bool,
        parse.bool,
        parse.bool,
        int,
        int,
        int,
        int,
    ],
)


# Fill Color Operators
FillBlackInkOnlyOperator = generate_op("gray g", "", [parse.float])
FillProcessInkOperator = generate_op(
    "cyan magenta yellow black k",
    "",
    [parse.float, parse.float, parse.float, parse.float],
)
FillCustomInkOperator = generate_op(
    "cyan magenta yellow black name gray x",
    "",
    [parse.float, parse.float, parse.float, parse.float, parse.string, parse.float],
)


class FillRGBColorOperator(Operator):
    """
    cyan magenta yellow black red green blue Xa
    Example: cyan magenta yellow black red green blue Xa
             red green blue Xa
    """

    def parse_tokens(self):
        assert len(self.tokens) in [3, 7], self.tokens
        self.cyan: Optional[float] = None
        self.magenta: Optional[float] = None
        self.yellow: Optional[float] = None
        self.black: Optional[float] = None

        if len(self.tokens) == 3:
            self.red, self.green, self.blue = [parse.float(t) for t in self.tokens]
        else:
            (
                self.cyan,
                self.magenta,
                self.yellow,
                self.black,
                self.red,
                self.green,
                self.blue,
            ) = [parse.float(t) for t in self.tokens]


class FillGenericCustomColorOperator(Operator):
    """
    comp1 … compn name tint type Xx
    Example: C M Y K name tint 0 Xx
             R G B name tint 1 Xx
    """

    def parse_tokens(self):
        self.type = parse.int(self.tokens[-1])
        assert self.type in (0, 1), self.tokens

        self.name = parse.string(self.tokens[-3])
        self.tint = parse.int(self.tokens[-2])

        if self.type == 0:
            self.components = [parse.float(t) for t in self.tokens[:4]]
        else:
            self.components = [parse.float(t) for t in self.tokens[:3]]


class FillPatternOperator(Operator):
    """Example: patternname px py sx sy angle rf r k ka [ a b c d tx ty] p"""

    def parse_tokens(self):
        tokens = (
            b" ".join(self.tokens).replace(b"[", b"[ ").replace(b"]", b" ] ").split()
        )
        self.patternname = parse.string(tokens[0])
        self.px = parse.float(tokens[1])
        self.py = parse.float(tokens[2])
        self.sx = parse.float(tokens[3])
        self.sy = parse.float(tokens[4])
        self.angle = parse.float(tokens[5])
        self.rf = parse.float(tokens[6])
        self.r = parse.float(tokens[7])
        self.k = parse.float(tokens[8])
        self.ka = parse.float(tokens[9])

        assert tokens[10] == b"[", tokens
        self.tfm = []
        for token in tokens[11:17]:
            self.tfm.append(parse.float(token))
        assert tokens[17] == b"]", tokens


ApplyKnownStyleOperator = generate_op(
    "integer stylename XW", "", [parse.int, parse.string]
)


# TODO:
PathModifierOperator = generate_op(
    "integer opacity 0 0 0 Xy",
    "",
    [parse.int, parse.float, parse.int, parse.int, parse.int],
)
