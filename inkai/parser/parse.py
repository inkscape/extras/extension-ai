# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Common parser functions."""

from inkai.parser.streamreader import dec


_bool = bool
_float = float
_int = int


def bool(flag: bytes) -> _bool:
    """Parse a flag which is "0" or "1"."""
    return flag != b"0"


def float(string: bytes) -> _float:
    """Parse a float."""
    return _float(string)


def int(string: bytes) -> _int:
    """Parse an integer."""
    return _int(string)


def string(string: bytes) -> str:
    """Return the string that was passed to this."""
    return dec(string)


def ignore(string: bytes) -> None:
    """Ignore this value."""


__all__ = ["ps", "bool", "int", "float", "string", "ignore", "examples"]
