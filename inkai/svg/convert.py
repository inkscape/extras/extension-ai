# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

from typing import Union
from ..parser.fileparser import FileParser, parse_bytes
from ..parser.consts import AIDocument
from inkex import SvgDocumentElement
from .contextmanager import DocumentContextManager


def ai2svg(file: Union[bytes, AIDocument]) -> SvgDocumentElement:
    """Gets passed an extracted AI document"""
    if isinstance(file, bytes):
        parsed: AIDocument = parse_bytes(file, FileParser.read_document)
    else:
        parsed = file
    cm = DocumentContextManager(parsed)
    for section in parsed.trailer:
        cm.interpret(section)
    return cm.root_element
