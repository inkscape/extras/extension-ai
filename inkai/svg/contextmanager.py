# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

# SPDX-License-Identifier: GPL-2.0-or-later

import warnings
from typing import List, Literal, Optional

import inkex

from inkai.parser.consts import AIDocument, Section
from inkai.parser.hierarchy_objects import (
    ActiveStyle,
    BasicFilter,
    BlendStyle,
    CompoundFilter,
    FillStyle,
    FilterBase,
    KnownStyle,
    StrokeStyle,
)
from inkai.parser.operators import (
    ApplyKnownStyleOperator,
    FillBlackInkOnlyOperator,
    FillCustomInkOperator,
    LineJoinOperator,
    LineCapOperator,
    LineWidthOperator,
    FillGenericCustomColorOperator,
    FillPatternOperator,
    FillProcessInkOperator,
    FillRGBColorOperator,
    FillRuleOperator,
    LayerNameOperator,
    LayerPropertiesOperator,
    MiterLimitOperator,
    Operator,
    PathOperator,
    PathModifierOperator,
    PathRenderOperator,
    StrokeDashOperator,
    WindingOrderOperator,
)
from inkai.svg.live_shape.lscontext import LiveShapeContextManager

from .coordinates import SVGTransform
from .icontextmanager import IContextManager, on


class DocumentContextManager(IContextManager):
    def __init__(self, file: AIDocument):
        self.root = self
        super().__init__()
        self.transform = SVGTransform(file)

        self.root_element = self.setup_document(file)

        self.file = file

        self.current_container: inkex.BaseElement = self.root_element

        self.last_was_duplicate: bool = False
        self.duplicate_was_ls: bool = False
        self.duplicate_ls_success = False
        self.xw_found: bool = False

        self.pathcm = PathContextManager(self)
        self.stylecm = StyleContextManager(self)
        self.lscm = LiveShapeContextManager(self)

    def setup_document(self, file: AIDocument) -> inkex.SvgDocumentElement:
        # Setup the page
        page = file.drawing_first_page_position
        width = self.transform.from_canvas_length(page.width)
        height = self.transform.from_canvas_length(page.height)

        result = inkex.base.SvgOutputMixin.get_template(
            width=width, height=height, unit=file.units
        ).getroot()

        # Add the remaining pages
        dd = file._document_data
        if not dd:
            return result
        artboards = dd["Recorded"].get("ArtboardArray", None)
        if not artboards or len(artboards) == 1:
            return result

        for artboard in artboards[1:]:
            x1, y1 = self.transform.from_drawing(*artboard["PositionPoint1"])
            x2, y2 = self.transform.from_drawing(*artboard["PositionPoint2"])

            result.namedview.new_page(
                *map(str, [x1, y1, x2 - x1, y2 - y1]), label=artboard["Name"]
            )

        # Also add the name of the first page
        result.namedview.get_pages()[0].set("inkscape:label", artboards[0]["Name"])

        return result

    # Layer operators

    @on("%AI5_BeginLayer")
    def start_layer(self, layer: Section):
        lr = inkex.Layer()

        self._add(lr)
        self.current_container = lr

        for child in layer.children:
            self.interpret(child)

        self.deduplicate()
        self.current_container = lr.getparent()

    @on("Lb")
    def layer_properties(self, op: LayerPropertiesOperator):  # type: ignore
        layer: inkex.Layer = self.current_container
        if not op.visible:  # type: ignore
            layer.style["display"] = "none"
        if not op.enabled:  # type: ignore
            layer.set("sodipodi:insensitive", "true")

    @on("Ln")
    def layer_name(self, op: LayerNameOperator):  # type: ignore
        layer: inkex.Layer = self.current_container
        layer.set("inkscape:label", op.name)  # type: ignore

    # Group operators

    @on("u")
    def start_group(self, _: Operator):
        g = inkex.Group()
        self._add(g)
        self.current_container = g

    @on("U")
    def end_group(self, _: Operator):
        self.deduplicate()
        self.current_container = self.current_container.getparent()
        self.stylecm.apply_blend_style()

    def _add(self, element: inkex.BaseElement):
        self.deduplicate()
        self.current_container.append(element)

    def _remove(self, element: inkex.BaseElement):
        try:
            self.current_container.remove(element)
        except ValueError:
            warnings.warn(f"Failed to remove {element} from {self.current_container}")

    @staticmethod
    def _zone_close_path(path: inkex.Path, match_path: inkex.Path) -> inkex.Path:
        m_paths = match_path.break_apart()
        o_paths = path.break_apart()
        p = inkex.Path()
        if len(m_paths) != len(o_paths):
            # match_path is a stroke
            for op in o_paths:
                if op[0].args == op[-1].args:
                    op.close()
                p.extend(op)
            return p

        for gp, op in zip(m_paths, o_paths):
            if inkex.paths.ZoneClose() in gp:
                op.close()
            elif op and isinstance(op[-1], inkex.paths.ZoneClose):
                op.pop()

            p.extend(op)
        return p

    def deduplicate(self):
        if self.duplicate_was_ls and not self.duplicate_ls_success:
            # <g><path/><path/></g> <failed shape/> <path?/> <failed shape/>
            # In case live shape conversion failed, we must still remove the duplicate path if present.
            # So, we need to retain the state of last_was_duplicate
            self.deduplicate_live_shape(False)
            self.duplicate_was_ls = False
            self.duplicate_ls_success = False

        if not self.last_was_duplicate:
            return
        if self.duplicate_was_ls:
            self.deduplicate_live_shape(self.duplicate_ls_success)
        else:
            self.deduplicate_object()

        self.last_was_duplicate = False
        self.duplicate_was_ls = False
        self.duplicate_ls_success = False
        self.xw_found = False

    def deduplicate_live_shape(self, success: bool):
        objects = list(self.current_container)

        if not success:
            if (
                len(objects) >= 2
                and isinstance(objects[-1], inkex.Group)
                and len(objects[-1]) == 1
                and isinstance(objects[-1][0], inkex.Group)
            ):
                # <g><g><path/><path/></g></g> -> <g><path/><path/></g>
                objects[-1][0].style = objects[-1].style
                objects[-1].replace_with(objects[-1][0])
            return

        if (
            len(objects) >= 4
            and isinstance(objects[-3], type(objects[-1]))
            and isinstance(objects[-4], inkex.Group)
        ):
            # <g><path/><path/></g> <shape/> <path/> <shape/> -> <shape/>
            self._remove(objects[-2])
            self._remove(objects[-3])
            if not self.xw_found:
                self._remove(objects[-1])
                if isinstance(objects[-4][0], inkex.Group):
                    objects[-4][0].style = objects[-4].style
                    objects[-4].replace_with(objects[-4][0])
                return
            self._remove(objects[-4])
        else:
            self.deduplicate_object()

    def deduplicate_object(self):
        objects = list(self.current_container)

        if len(objects) >= 2 and isinstance(objects[-2], inkex.Group):
            if isinstance(objects[-2][0], inkex.Group):
                # <g><g><path/><path/></g></g> <shape or path/> -> <shape or path/>
                g: inkex.Group = objects[-2][0]
            else:
                # <g><path/><path/></g> <shape or path/> -> <shape or path/>
                g = objects[-2]

            if not self.xw_found:
                self._remove(objects[-1])
                g.style = objects[-2].style
                objects[-2].replace_with(g)
                return
            self._remove(objects[-2])

            if isinstance(objects[-1], inkex.PathElement):
                # Close the path if the fill part of the group is closed
                objects[-1].path = self._zone_close_path(objects[-1].path, g[0].path)

        elif (
            len(objects) >= 2
            and isinstance(objects[-2], inkex.PathElement)
            and objects[-2].get("sodipodi:type") != "star"
        ):
            # <path/> <shape or path/> -> <shape or path/>
            self._remove(objects[-2])

            if isinstance(objects[-1], inkex.PathElement):
                # Close the path if the duplicate path is closed
                objects[-1].path = self._zone_close_path(
                    objects[-1].path, objects[-2].path
                )

        else:
            warnings.warn(f"Pseudo object {objects[-1]} has no duplicates to remove.")
            return

    # Stuff that gets distributed based on content
    @on(dict)
    def entry_art_dictionary(self, element: dict):
        if element["_type"] == "ArtDictionary":
            ls: Optional[dict] = element.get("ai::LiveShape", None)
            if ls is not None:
                success = self.lscm.convert_live_shape(ls)

                if ls.get("Recorded", True) and element.get("AI10_ArtUID") is None:
                    assert len(
                        self.current_container
                    ), "Live shape converted but not present in the SVG document"
                    self.duplicate_was_ls = True
                    self.duplicate_ls_success = success
                    self.xw_found = False
                    if not success:
                        # Retain the state of last_was_duplicate
                        return
                    self.last_was_duplicate = True


class StyleContextManager(IContextManager):
    def __init__(self, root: DocumentContextManager):
        self.func_mapping = root.func_mapping
        super().__init__(root)
        self.root: DocumentContextManager = root
        self._fill: Optional[inkex.Color] = None
        self._s_fill: Optional[inkex.Color] = None
        self._stroke_align: int = 0
        self._stroke_dash: List[int] = []
        self._stroke_linejoin: Literal["miter", "round", "bevel"] = "miter"
        self._stroke_linecap: Literal["butt", "round", "square"] = "butt"
        self._stroke_miterlimit = 0.0
        self._stroke_width = 0.0
        self._opacity = 1.0

    def apply_blend_style(self) -> None:
        current_object = self.root.current_container[-1]
        current_object.style["opacity"] = self._opacity

    def apply_stroke_style(self) -> None:
        current_object = self.root.current_container[-1]
        current_object.style["stroke"] = self._s_fill
        current_object.style["stroke-dasharray"] = " ".join(
            [f"{x}" for x in self._stroke_dash]
        )
        current_object.style["stroke-linejoin"] = self._stroke_linejoin
        current_object.style["stroke-linecap"] = self._stroke_linecap
        current_object.style["stroke-miterlimit"] = self._stroke_miterlimit
        current_object.style["stroke-width"] = self._stroke_width

        if self._stroke_align != 0:
            path_effect = inkex.PathEffect.new(
                effect="offset",
                is_visible="true",
                lpeversion="1",
                offset=f"{self._stroke_align * self._stroke_width/2}",
                unit="px",
                linejoin_type=self._stroke_linejoin,
                miter_limit=self._stroke_miterlimit,
                attempt_force_join="false",
                update_on_knot_move="true",
            )
            self.root.root_element.defs.append(path_effect)
            current_object.set("inkscape:path-effect", "#" + path_effect.get_id())

    def apply_fill_style(self) -> None:
        current_object = self.root.current_container[-1]
        current_object.style["fill"] = self._fill

    def apply_style(self):
        """
        Applies the current style to the last created element.
        The element is possibly duplicated during this.
        """
        self.apply_fill_style()
        self.apply_blend_style()
        self.apply_stroke_style()

    @on("XW")
    def apply_known_style(self, op: ApplyKnownStyleOperator) -> None:  # type: ignore
        styles = self.root.file.setup.art_styles
        if op.stylename not in styles:  # type: ignore
            return
        self.root.xw_found = True
        style = styles[op.stylename]  # type: ignore
        if not isinstance(style.Def, ActiveStyle):
            return
        if style.Def.Execution is not None:
            self.apply_filter_compound(style.Def.Execution)

    def apply_filter_compound(self, filter: CompoundFilter) -> None:
        if filter.Visible is not None and not filter.Visible:
            return

        for f in filter.Part:
            if isinstance(f, CompoundFilter):
                self.apply_filter_compound(f)
            else:
                self.apply_filter(f)

    def apply_filter(self, filter: FilterBase) -> None:
        if filter.Visible is not None and not filter.Visible:
            return
        if isinstance(filter, BasicFilter):
            self.apply_basic_filter(filter)

    def apply_basic_filter(self, filter: BasicFilter) -> None:
        if filter.Dict is None:
            return

        for style in (
            filter.Dict.get("BlendStyle"),
            filter.Dict.get("StrokeStyle"),
            filter.Dict.get("FillStyle"),
        ):
            if not isinstance(style, (BlendStyle, StrokeStyle, FillStyle)):
                continue

            if isinstance(style, FillStyle):
                self._fill = None
            elif isinstance(style, StrokeStyle):
                self._s_fill = None

            for op in style.Def or []:
                self.root.interpret(op)

            if isinstance(style, FillStyle) and filter.Dict.get("UseEvenOdd"):
                self.root.current_container.style["fill-rule"] = "evenodd"

            if isinstance(style, BlendStyle):
                self.apply_blend_style()
            elif isinstance(style, FillStyle):
                self.apply_fill_style()
            elif isinstance(style, StrokeStyle):
                self._stroke_align = (
                    -1
                    if filter.Dict.get("StrokeOffsetInside") is True
                    else 0
                    if filter.Dict.get("StrokeOffsetInside") is None
                    else 1
                )
                self.apply_stroke_style()

                # Should be applied by known styles only, not by inline styles
                self._stroke_align = 0

    @staticmethod
    def _cmyk(cyan: float, magenta: float, yellow: float, black: float) -> inkex.Color:
        return inkex.Color(
            (
                255 * (1 - min(1, cyan + black)),
                255 * (1 - min(1, magenta + black)),
                255 * (1 - min(1, yellow + black)),
            )
        )

    def _rgb(self, red: float, green: float, blue: float) -> inkex.Color:
        return inkex.Color((red * 255, green * 255, blue * 255))

    # Fill

    @on("Xa")
    def fill_rgb(self, op: FillRGBColorOperator) -> None:
        self._fill = self._rgb(op.red, op.green, op.blue)  # type: ignore

    @on("k")
    def fill_cmyk(self, op: FillProcessInkOperator) -> None:  # type: ignore
        self._fill = self._cmyk(op.cyan, op.magenta, op.yellow, op.black)  # type: ignore

    @on("g")
    def fill_gray(self, op: FillBlackInkOnlyOperator) -> None:  # type: ignore
        self._fill = self._rgb(op.gray, op.gray, op.gray)  # type: ignore

    @on("Xx")
    def fill_generic_custom_color(self, op: FillGenericCustomColorOperator) -> None:
        # TODO: op.tint
        self._fill = (
            self._cmyk(*op.components) if op.type == 0 else self._rgb(*op.components)  # type: ignore
        )

    @on("p")
    def fill_pattern(self, op: FillPatternOperator) -> None:
        pass  # TODO: pattern fill

    # Stroke Fill

    @on("XA")
    def stroke_rgb(self, op: FillRGBColorOperator) -> None:
        self._s_fill = self._rgb(op.red, op.green, op.blue)  # type: ignore

    @on("K")
    def stroke_cmyk(self, op: FillProcessInkOperator) -> None:  # type: ignore
        self._s_fill = self._cmyk(op.cyan, op.magenta, op.yellow, op.black)  # type: ignore

    @on("G")
    def stroke_gray(self, op: FillBlackInkOnlyOperator) -> None:  # type: ignore
        self._s_fill = self._rgb(op.gray, op.gray, op.gray)  # type: ignore

    @on("XX")
    def stroke_generic_custom_color(self, op: FillGenericCustomColorOperator) -> None:
        # TODO: op.tint
        self._s_fill = (
            self._cmyk(*op.components) if op.type == 0 else self._rgb(*op.components)  # type: ignore
        )

    @on("P")
    def stroke_pattern(self, op: FillPatternOperator) -> None:
        pass  # TODO: pattern fill

    # Stroke styles

    @on("d")
    def stroke_dash(self, op: StrokeDashOperator) -> None:
        # TODO: op.phase. AI does not provide UI for this
        self._stroke_dash = op.dashes  # type: ignore

    @on("j")
    def line_join(self, op: LineJoinOperator) -> None:  # type: ignore
        if op.line_join > 2 or op.line_join < 0:  # type: ignore
            warnings.warn(f"Unknown line join: {op.line_join}")  # type: ignore

        self._stroke_linejoin = (
            "miter",
            "round",
            "bevel",
        )[
            op.line_join  # type: ignore
        ]

    @on("J")
    def line_cap(self, op: LineCapOperator) -> None:  # type: ignore
        if op.line_cap > 2 or op.line_cap < 0:  # type: ignore
            warnings.warn(f"Unknown line cap: {op.line_cap}")  # type: ignore

        self._stroke_linecap = (
            "butt",
            "round",
            "square",
        )[
            op.line_cap  # type: ignore
        ]

    @on("M")
    def miter_limit(self, op: MiterLimitOperator) -> None:  # type: ignore
        self._stroke_miterlimit = op.miter_limit  # type: ignore

    @on("w")
    def line_width(self, op: LineWidthOperator) -> None:  # type: ignore
        self._stroke_width = op.line_width  # type: ignore

    @on("Xy")
    def path_modifier(self, op: PathModifierOperator) -> None:  # type: ignore
        self._opacity = op.opacity  # type: ignore


class PathContextManager(IContextManager):
    root: DocumentContextManager

    def __init__(self, root: DocumentContextManager):
        self.func_mapping = root.func_mapping
        super().__init__(root)
        # State variables
        self.geometry = inkex.Path()
        self.stroke: bool = False
        self.fill: bool = False
        self.clockwise_winding_order: bool = False
        self.in_compound_path: bool = False
        self.last_point = [0, 0]
        self._fill_rule: str = "nonzero"

    @on(*list("cCvVyY"))
    def curve_like(self, op: PathOperator):
        pts = self.root.transform.from_drawing(*op.to_cubic(self.last_point))
        self.last_point = [op.x3, op.y3]  # type: ignore
        self.geometry.append(inkex.paths.Curve(*pts))
        # TODO node type

    @on("l", "L")
    def line_like(self, op: PathOperator):
        pts = self.root.transform.from_drawing(op.x, op.y)  # type: ignore
        self.last_point = [op.x, op.y]  # type: ignore
        self.geometry.append(inkex.paths.Line(*pts))
        # TODO node type

    @on("m")
    def move(self, op: PathOperator):
        pts = self.root.transform.from_drawing(op.x, op.y)  # type: ignore
        self.last_point = [op.x, op.y]  # type: ignore
        self.geometry.append(inkex.paths.Move(*pts))

    @on("D")
    def winding_order(self, op: WindingOrderOperator):  # type: ignore
        self.clockwise_winding_order = op.clockwise  # type: ignore

    @on("*u", "*U")
    def compound_path(self, op: Operator):
        """Tell the context manager to collect the following paths"""
        self.in_compound_path = op.command == "*u"
        if op.command == "*U":
            # Attach the compound path

            self.attach_path(op.is_pseudo_comment)

    @on("XR")
    def fill_rule(self, op: FillRuleOperator):  # type: ignore
        if self.in_compound_path and len(self.geometry) > 0:
            warnings.warn(
                "Changing the fill rule mid-compound path is not supported in SVG"
            )
        self._fill_rule = "evenodd" if op.rule else "nonzero"  # type: ignore

    @on(*list("NnFfsSBb"))
    def paint_path(self, op: PathRenderOperator):
        if op.close:
            self.geometry.append(inkex.paths.ZoneClose())
        if not self.clockwise_winding_order:
            self.geometry.reverse()
        if self.in_compound_path and len(self.geometry) > 0:
            if self.fill != op.fill or self.stroke != op.stroke:
                warnings.warn(
                    "Changing the fill/stroke properties mid-compound path is not supported in SVG"
                )
        self.fill = op.fill
        self.stroke = op.stroke
        if not self.in_compound_path:
            self.attach_path(op.is_pseudo_comment)

    # TODO process guides here

    def attach_path(self, is_pseudo: bool = False):
        pel = inkex.PathElement(d=str(self.geometry))
        self.root._add(pel)

        pel.style["fill-rule"] = self._fill_rule

        if not self.fill:
            self.root.stylecm._fill = None
        if not self.stroke:
            self.root.stylecm._s_fill = None

        self.root.stylecm.apply_style()

        # NOTE: Fill and stroke are intentionally not reset here.
        # This allows "no-fill" or "no-stroke" to take effect on live shapes without known styles (XW)
        # (caveat): Fill/stroke might remain None for subsequent objects if not explicitly set

        self.geometry = inkex.Path()

        # Remove duplicates
        if is_pseudo:
            assert len(
                self.root.current_container
            ), "Path converted but not present in the SVG document"
            self.root.last_was_duplicate = True
            self.root.duplicate_was_ls = False
            self.root.xw_found = False
