# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This file contains common warnings.

I.e. to inform the user about the state of the document conversion.
"""


class NotWorthImplementingWarning(UserWarning):
    """This feature is not worth implementing.

    We can just ignore it. If you need it, please open an issue.
    """


__all__ = ["NotWorthImplementingWarning"]
