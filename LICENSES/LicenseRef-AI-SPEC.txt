This is the license from the AI Spec

Copyright (c) 1998 by Adobe Systems Incorporated. All rights reserved.
NOTICE: All information contained herein is the property of Adobe Systems Incorporated. Many of
the intellectual and technical concepts contained herein are proprietary to Adobe, are protected as trade
secrets, and are made available only to Adobe licensees for their internal use.
No part of this publication (whether in hardcopy or electronic form) may be reproduced or transmitted,
in any form or by any means, electronic, mechanical, photocopying, recording, or otherwise, without
the prior written consent of the publisher. Any information referred to herein is furnished under license
with Adobe and may only be used, copied, transmitted, stored, or printed in accordance with the terms
of such license, or in the accompanying Materials Release Form from Adobe.
PostScript is a registered trademark of Adobe Systems Incorporated. All instances of the name
PostScript in the text are references to the PostScript language as defined by Adobe Systems
Incorporated unless otherwise stated. The name PostScript also is used as a product trademark for
Adobe Systems' implementation of the PostScript language interpreter.
Any references to a “PostScript printer,” a “PostScript file,” or a “PostScript driver” refer to printers,
files, and driver programs (respectively) which are written in or support the PostScript language. The
sentences in this document that use “PostScript language” as an adjective phrase are so constructed to
reinforce that the name refers to the standard language definition as set forth by Adobe Systems
Incorporated.
Acrobat, Adobe, Adobe Illustrator, Adobe Garamond, the Adobe logo, Carta, Display PostScript,
Distiller, FrameMaker, Lithos, Sonata, and TranScript are registered trademarks and Acrobat
Exchange, Acrobat Reader, and the PostScript logo are trademarks of Adobe Systems Incorporated.
AppleTalk, LocalTalk, Macintosh, and LaserWriter are registered trademarks of Apple Computer, Inc.
IBM is a registered trademark of International Business Machines Corporation. ITC Stone is a
registered trademark of International Typeface Corporation. C EXECUTIVE is a registered trademark
and CE-VIEW is a trademark of JMI Software Consultants, Inc. Helvetica, Palatino, and Times are
trademarks of Linotype-Hell AG and/or its subsidiaries. X Window System is a trademark of the
Massachusetts Institute of Technology. Microsoft and MS-DOS are registered trademarks and
Windows is a trademark of Microsoft Corporation. Times New Roman is a registered trademark of The
Monotype Corporation PLC. NeXT is a trademark of NeXT Computer, Inc. Sun, Sun-3 and SunOS
are trademarks of Sun Microsystems, Inc. SPARC is a registered trademark of SPARC International,
Inc. Products bearing the SPARC trademark are based on an architecture developed by Sun
Microsystems, Inc. SPARCstation is a registered trademark and is a trademark of SPARC
International, Inc., licensed exclusively to Sun Microsystems, Inc. UNIX is a trademark registered in
the United States and other countries, licensed exclusively through X/Open Company, Limited. Other
brand or product names are the trademarks or registered trademarks of their respective holders.
This publication and the information herein is furnished AS IS, is subject to change without notice, and
should not be construed as a commitment by Adobe Systems Incorporated. Adobe Systems Incorpo-
rated assumes no responsibility or liability for any errors or inaccuracies, makes no warranty of any
kind (express, implied or statutory) with respect to this publication, and expressly disclaims any and
all warranties of merchantability, fitness for particular purposes and noninfringement of third party
rights.
