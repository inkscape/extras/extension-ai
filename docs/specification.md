<!--
SPDX-FileCopyrightText: 2022 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Specification

The implementation follows the [Adobe Illustrator File Format Specification][ai].
Later versions (after 1998) of the file format are not specified in the document.
These have to be reverse-engineered.
Here, you can read what we found out about the differences.

## Reading

These documents are a valuable resource to solidify the extension:

- [PostScript Language Document Structuring Conventions Specification][ps]
- [Adobe Illustrator File Format Specification][ai]

## Relevance of entries in the [AI specification][ai]

|**BNF entry**|**relevant**|**what to convert**|**target objects/attributes**|**importance**
|-----|-----|-----|-----|-----|
|`<header>`|yes| | | 
|`<header comments>`|partially|Creator|Metadata|medium
| | |For|Metadata|medium
| | |Title|Metadata|medium
| | |HiResBoundingBox|svg:viewbox|high
| | |CMYK headers|ICC profile (not very well supported by Inkscape)|low
| | |AI5\_RulerUnits|namedview: inkscape:document-units|low
| | |AI5\_OpenToView|namedview properties|low
| | |RGBCustomColor|reuse when reference|high
| | | |maybe convert to swatch (special kind of one-stop gradient)|low
| | |AI3\_PaperRect|(unsure) Page settings|?
| | |AI7\_GridSettings|`<inkscape:grid>`|medium
|`<setup>`|yes| | | 
|`<gradient defs>`|yes|all except Bm / Bc|Lineargradient / radialgradient|high
|`<color palette>`|yes|all|swatches (special kind of one-stop gradient)|low
|`<pattern defs>`|yes|all|pattern|high
|`<font encoding>`|?| | | 
|`<guide>`|yes|all| |medium
|`<mask>`|yes|all|clipPath|high
|`<paint style>`|yes|all|overprint might be a blend mode? [Quora](https://www.quora.com/Does-SVG-support-CMYK?share=1): CMYK seems to related to overprint|high
|`<path object>`|yes|all|<p>|high
|`<compound path>`|yes|all|<p> element with multiple subpaths|high
|`<path operator>`|yes|all|capitalization: sodipodi:nodetypes|high
|`<wraparound text>`|maybe| |Shape-inside for SVG2 texts|low
|`<text>`|no| |has changed in AI11| 
|`<placed art object>`|maybe| |embedded SVG (need to convert the embedded doc too)|low
|`<graph object>`|no| | | 
|`<layer>`|maybe|all|g groupmode=layer. Syntax doesn’t support sublayers so prob out of date||medium
|%AI5\_BeginRaster|probably|all|`<image>`|high


[ai]: http://www.idea2ic.com/File_Formats/Adobe%20Illustrator%20File%20Format.pdf
[ps]: http://www.lprng.com/RESOURCES/ADOBE/5001.DSC_Spec.pdf
