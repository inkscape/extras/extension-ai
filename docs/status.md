
<!--
SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>

SPDX-License-Identifier: GPL-2.0-or-later
-->
<style>img {vertical-align: bottom;}</style>

Implementation Status
=====================

We do not implement everything, there are amendmends of the specification and we want to know what is done and what is not. Basically, this is a feature list.
The purpose of this document is to show the implementation status.

Features can be implemented. Their status is marked like this:

| Badge | Implementation Status |
| --- | --- |
| ![done] | This is implemented. |
| ![high] | high priority. This is likely to be done by the developers. |
| ![low] | low priority. This might be done by chance. If you need it, open an issue. |
| ![obsolete] | This is likely to be not in use any more. |
| ![no] | This does not make sense to implement. This could be because Inkscape does not support it. |

We can use different specifications or reverse-engineer the files. Some features are specified but also changed later.

| Badge | Specification |
| --- | --- |
| ![ai] | This featue is specified in the [Adobe Illustrator Specification] from 1998. |
| ![amended] | This feature is not documented and requires reverse-engineering. |

If you like to work on something here or need a feature,
please open an issue or a pull request.

Path
----

![ai] page 54

- ![done] path geometry
- ![high] path render
- ![high] path attribtues
- ![high] fill rule
- ![done] compound path
- ![high] color
- ![low] note

![ai] page 63

- ![low] overprint

Color
-----


- ![done] ![ai] page 60
- ![low] ![amended] [documentation](specification_amendmends/colors_and_paletters.md)


Container
---------

![ai] page 60

- ![high] groups
- ![high] clipping

Text
----

![ai] page 60 ![obsolete]  
The text implementation has changed so it does not make sense to put a lot of effort in it now.

![amended] ![high]  
The text impementation has changed and should be supported.

Layer
-----

![ai] page 71

- ![high] several layers
- ![high] name
- ![low] visible preview enabled printing dimmed
- ![no] color




[ai]: images/badges/ai.svg
[done]: images/badges/done.svg
[high]: images/badges/high.svg
[low]: images/badges/low.svg
[obsolete]: images/badges/obsolete.svg
[no]: images/badges/no.svg
[amended]: images/badges/amended.svg
[Adobe Illustrator Specification]: 
