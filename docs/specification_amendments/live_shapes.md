<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->
# Live shapes

Live shapes (added in V9) are similar to the different Shape tools in Inkscape. 
They are effectively paths with additional data that allows for easy editing (say, change the arc start and end angle). 
For some shapes (rectangles and polygons), AI additionally allows to change the corner
radius (globally for polygons, individually for rectangles). This is conceptually similar to Inkscape's Fillet/Chamfer LPE.

## Data format

AI stores the parameters of the Live shape in an `ArtDictionary`. 
We can use those to construct the shape and the applied Fillet/Chamfer LPE.

Interestingly, those parameters are not read back in: 
instead, assuming `ai::LiveShape::HandlerName` is defined in the dictionary, 
the parameters of the Live shape (as displayed in the UI) are reconstructed from the path data
(so changing or deleting them in the file has no effect). If the path data is changed such that
no shape is found which matches the path, the object is converted to a plain path 
(preserving the Fillet/Chamfer handles for the corners).
This is probably done to avoid changing the visual shape in newer versions because the 
implementation of a certain Handler was changed to result in a different path.

For backwards compatibility, AI stores (additionally to the path data (the outline) and the `ArtDictionary`) whatever is necessary to draw the shape in AI versions which don't understand live shapes or modern `/KnownStyle`s: fill(s) / stroke(s) (centered) as paths and inset/outset stroke(s) as compound paths. This data can likely be discarded; 
if we do inset/outset stroke, it will be implemented as Offset LPE.

### No Style / only a single Fill

For a shape which only has a fill or no style at all, AI can reconstruct the path from the fill shape directly.

Structure:

```
Path and fill
%_/ArtDictionary
```

Example:

```
0 A                                                           # not locked
0 Xw                                                          # ?
1 Ap                                                          # Show path center point
4 As                                                          # Next path has 4 nodes
0 O                                                           # No fill overprint
0 0.751 0.888 0 0.945 0.352 0.141 Xa                          # Set color to orange
0 1 0 0 0 Xy                                                  # ?
0 J 0 j 1 w 10 M []0 d                                        # Stroke style
0 XR                                                          # Fill rule
562 -405.5 m                                                  # Outline of the path. Stored in d=""
562 -489.7234243445 486.1122650911 -558 392.5 -558 c
298.8877349089 -558 223 -489.7234243445 223 -405.5 c
223 -321.2765756555 298.8877349089 -253 392.5 -253 c
486.1122650911 -253 562 -321.2765756555 562 -405.5 c
f                                                             # Close and fill path
%_/ArtDictionary :    
%_/XMLUID : (one_fill) ; (AI10_ArtUID) ,                      # Set shape name to one_fill
%_/Dictionary :
%_(ai::Ellipse) /UnicodeString (ai::LiveShape::HandlerName) , # HandlerName is important
%_/Dictionary :
%_1 /Int (ai::Ellipse::InitialQuadrant) ,                     # These values are ignored, but accurate
%_0 /Real (ai::Ellipse::PieEndAngle) ,
%_0 /Real (ai::Ellipse::PieStartAngle) ,
%_0 /Real (ai::Ellipse::RotationAngle) ,
%_7623.5 /Real (ai::Ellipse::CenterX) ,                       # Center point is given in 
%_8056.5 /Real (ai::Ellipse::CenterY) ,                       # Canvas coordinates
%_339 /Real (ai::Ellipse::Width) ,
%_305 /Real (ai::Ellipse::Height) ,
%_1 /Bool (ai::Ellipse::Clockwise) ,
%_; (ai::LiveShape::Params) ,
%_; (ai::LiveShape) ,
%_;
%_
```

### One or more strokes or more than one fill

Structure:

```
u
[All fills and strokes]
U
%_/ArtDictionary: NotRecorded
6 () XW
%_base path data
%_/ArtDictionary
1 (KnownStyleName) XW
```


All fills and strokes are grouped together before the `ArtDictionary.`

The raw path is stored as a comment before the second `ArtDictionary`. The first `ArtDictionary` (`NotRecorded`) is ignored, and can even be deleted from the file.

Example:

```
0 Ae                                                          # Group not expanded
u                                                             # Begin group
4 As                                                          # Next path has 4 nodes
0 0.751 0.888 0 0.945 0.352 0.141 Xa                          # Set color of first fill
753.5 -880 m                                                  # Path of first fill
663.345703125 -880 590 -814.2802734375 590 -733.5 c
590 -652.7197265625 663.345703125 -587 753.5 -587 c
843.654296875 -587 917 -652.7197265625 917 -733.5 c
917 -814.2802734375 843.654296875 -880 753.5 -880 C
f                                                             # Close and fill path
4 As                                                          # Next path has 4 nodes
(Alyssa) 0 0 1 1 0 0 0 0 0 [1 0 0 1 0 0] p                    # Set pattern of second fill
753.5 -880 m                                                  # Path of second fill 
663.345703125 -880 590 -814.2802734375 590 -733.5 c           # (identical path data)
590 -652.7197265625 663.345703125 -587 753.5 -587 c
843.654296875 -587 917 -652.7197265625 917 -733.5 c
917 -814.2802734375 843.654296875 -880 753.5 -880 C
f                                                             # Close and fill path
0 Ae                                                          # Compound path not expanded
*u                                                            # Begin compound path
9 As                                                          # Next path has 9 nodes
0.220 0.272 0.388 0.061 0.780 0.698 0.6 Xa                    # Set color of stroke
753.5 -593 m                                                  # inner path of stroke
795.806 -593 835.521 -607.769 865.328 -634.587 C
894.780 -661.084 911 -696.212 911 -733.5 C
911 -770.787 894.780 -805.915 865.328 -832.412 C
835.521 -859.230 795.806 -874 753.5 -874 C
711.193 -874 671.478 -859.230 641.671 -832.412 C
612.219 -805.915 596 -770.787 596 -733.5 C
596 -696.212 612.219 -661.084 641.671 -634.587 C
671.478 -607.769 711.193 -593 753.5 -593 C
F                                                             # Fill path
5 As                                                          # Next path has 5 nodes
1 D                                                           # Winding order
753.5 -581 m                                                  # outer path of stroke
659.887756347656 -581 584 -649.276550292969 584 -733.5 C
584 -817.723449707031 659.887756347656 -886 753.5 -886 C
847.112243652344 -886 923 -817.723449707031 923 -733.5 C
923 -649.276550292969 847.112243652344 -581 753.5 -581 C
753.5 -581 L
f                                                             # fill and close path
*U                                                            # Leave compound path
U                                                             # Leave group
%_/ArtDictionary :                                            # Redundant ArtDictionary
%_/XMLUID : (two_fills_00000173845534891713128520000007639562742719675288_) ; (AI10_ArtUID) ,
%_/Dictionary : /NotRecorded ,
%_(ai::Ellipse) /UnicodeString (ai::LiveShape::HandlerName) ,
%_/Dictionary : /NotRecorded ,
%_1 /Int (ai::Ellipse::InitialQuadrant) ,
%_0 /Real (ai::Ellipse::PieEndAngle) ,
%_0 /Real (ai::Ellipse::PieStartAngle) ,
%_0 /Real (ai::Ellipse::RotationAngle) ,
%_7984.5 /Real (ai::Ellipse::CenterX) ,
%_8384.5 /Real (ai::Ellipse::CenterY) ,
%_339 /Real (ai::Ellipse::Width) ,
%_305 /Real (ai::Ellipse::Height) ,
%_1 /Bool (ai::Ellipse::Clockwise) ,
%_; (ai::LiveShape::Params) ,
%_; (ai::LiveShape) ,
%_;
%_
0 0 Xd                                                        # ?
6 () XW                                                       # ?
%_1 Ap                                                        # Show path center point
%_4 As                                                        # Next path has 4 nodes
%_0 D                                                         # Winding order
%_923 -733.5 m                                                # Relevant path data
%_923 -817.7234243445 847.1122650911 -886 753.5 -886 c
%_659.8877349089 -886 584 -817.7234243445 584 -733.5 c
%_584 -649.2765756555 659.8877349089 -581 753.5 -581 c
%_847.1122650911 -581 923 -649.2765756555 923 -733.5 c
%_n
%_/ArtDictionary :
%_/XMLUID : (two_fills) ; (AI10_ArtUID) ,                     # Name of the object
%_/Dictionary :
%_(ai::Ellipse) /UnicodeString (ai::LiveShape::HandlerName) , # HandlerName is important
%_/Dictionary :
%_1 /Int (ai::Ellipse::InitialQuadrant) ,                     # These values are ignored, but accurate
%_0 /Real (ai::Ellipse::PieEndAngle) ,
%_0 /Real (ai::Ellipse::PieStartAngle) ,
%_0 /Real (ai::Ellipse::RotationAngle) ,
%_7984.5 /Real (ai::Ellipse::CenterX) ,
%_8384.5 /Real (ai::Ellipse::CenterY) ,
%_339 /Real (ai::Ellipse::Width) ,
%_305 /Real (ai::Ellipse::Height) ,
%_1 /Bool (ai::Ellipse::Clockwise) ,
%_; (ai::LiveShape::Params) ,
%_; (ai::LiveShape) ,
%_;
%_
1 (Anon McemVsHzhk8=) XW                                      # Reference to a /KnownStyle
```

In this case, all style data should be extracted from the `/KnownStyle`, so everything before `%_base path data` can be ignored.

## Types of `LiveShape`s and their attributes

### `ai::Rectangle`

```
/ArtDictionary :
  (angle in radians) /String (BBAccumRotation) , 
     Value is optional, appears to be identical to ai::Rectangle::Angle
  /Dictionary :
    (ai::Rectangle) /UnicodeString (ai::LiveShape::HandlerName) ,
    /Dictionary :
      0|1 /Bool (ai::Rectangle::Clockwise) ,
        Specifies the order of corners.
        By default: 1, mirroring a rectangle inverts this value.
      3 /Int (ai::Rectangle::InitialQuadrant) ,
        Specifies that the corner 0 is the bottom right corner.
        Always 3.
      (CornerType) /UnicodeString (ai::Rectangle::CornerType::i) ,
        This parameter is specified for every corner.
        Possible values: Chamfer, Normal, Inverted
      (RoundingType) /UnicodeString (ai::Rectangle::RoundingType::i) ,
        This parameter is specified for every corner.
        Possible values: Absolute, Relative
        If Absolute, the radius is constant (save for bezier approximation errors).
        If Relative, the rounding becomes a bit "pointier" for acute angles
        and a bit shallower for obtuse angles, the difference is negligible
        for 90° angles.
      float /Real (ai::Rectangle::CornerRadius::i) ,
        Corner radius
      float /Real (ai::Rectangle::Width) ,
      float /Real (ai::Rectangle::Height) ,
      float /Real (ai::Rectangle::CenterX) ,
        Specified in canvas coordinates.
      float /Real (ai::Rectangle::CenterY) ,
        Specified in canvas coordinates.
      float /Real (ai::Rectangle::Angle) ,
    ; (ai::LiveShape::Params) ,
  ; (ai::LiveShape) ,
;
```

This shape should be parsed into a Rectangle with Fillet/Chamfer LPE if all corners are absolute, and a plain path if one or more corners are relative.

### `ai::Ellipse`

```
/ArtDictionary :
  (number) /String (BBAccumRotation) ,
    Value is optional, appears to be identical to ai::Ellipse::RotationAngle
  /Dictionary :
    (ai::Ellipse) /UnicodeString (ai::LiveShape::HandlerName) ,
    /Dictionary :
      0|1 /Bool (ai::Ellipse::Clockwise) ,
        Specifies the sense of rotation.
        By default: 1, mirroring an ellipse inverts this value.
        Value appears to have no influence?
      1 /Int (ai::Ellipse::InitialQuadrant) ,
      float /Real (ai::Ellipse::CenterX) ,
        Specified in canvas coordinates.
      float /Real (ai::Ellipse::CenterY) ,
        Specified in canvas coordinates.
      float /Real (ai::Ellipse::Width) ,
      float /Real (ai::Ellipse::Height) ,
      float /Real (ai::Ellipse::PieStartAngle) ,
      float /Real (ai::Ellipse::PieEndAngle) ,
      float /Real (ai::Ellipse::RotationAngle) ,
    ; (ai::LiveShape::Params) ,
  ; (ai::LiveShape) ,
;
```

This shape should be parsed into a `path` with arc attributes, i.e. `sodipodi:arc-type` etc.
The plain path should be set as `d` to allow rendering in browsers.

### `ai::Polygon`

```
/ArtDictionary :
  (number) /String (BBAccumRotation) ,
    Value is optional, appears to be identical (up to a difference of 2 pi) to ai::Polygon::RotationAngle
  /Dictionary : 
    (ai::Polygon) /UnicodeString (ai::LiveShape::HandlerName) ,
    /Dictionary : 
      0|1 /Bool (ai::Polygon::Clockwise) ,
        Specifies the sense of rotation.
        By default: 1, mirroring a polygon inverts this value.
        Value appears to have no influence?
      0|1 /Int (ai::Polygon::CornerRoundingType) ,
        0: Sharp corners, 1: Any other corner type
      float /Real (ai::Polygon::CornerRadius) ,
      0|1|2|3 /Int (ai::Polygon::CornerType) ,
        0: Sharp corners, 1: Rounded, 2: Inverted, 3: Chamfer
      integer /Int (ai::Polygon::NumSides) ,
        
      float /Real (ai::Polygon::CenterX) ,
        Specified in canvas coordinates.
      float /Real (ai::Polygon::CenterY) ,
        Specified in canvas coordinates.
      float /Real (ai::Polygon::RotationAngle) ,
      float /Real (ai::Polygon::Radius) ,
      1 /Bool (ai::Polygon::IsRegular) ,
    ; (ai::LiveShape::Params) ,
  ; (ai::LiveShape) ,
;
```

This shape should be parsed into a `path` with polygon attributes. 
If the `CornerType` is not 0, a Fillet/Chamfer LPE should be applied.
The plain path should be set as `d` to allow rendering in browsers.

### `ai::Line`

```
/ArtDictionary :
  (number) /String (BBAccumRotation) ,
  /Dictionary : 
    (ai::Line) /UnicodeString (ai::LiveShape::HandlerName) ,
    /Dictionary : 
      float /Real (ai::Line::Length) ,
      float /Real (ai::Line::RotationAngle) ,
      float /Real (ai::Line::CenterX) ,
        Specified in canvas coordinates.
      float /Real (ai::Line::CenterY) ,
        Specified in canvas coordinates.
    ; (ai::LiveShape::Params) ,
  ; (ai::LiveShape) ,
;
```

While we could parse this as an SVG `<line>` element, Inkscape doesn't provide additional
functionality for this element. We should just use the original path data.

### `ai::Star`

```
/ArtDictionary :
  /Dictionary : /NotRecorded ,
    /Dictionary : /NotRecorded ,
      int /Int (ai::Star::NumPoints) ,
      int /Int (ai::Star::Corner2Type) ,
      float /Real (ai::Star::Radius1) ,
      bool /Bool (ai::Star::IsRegular) ,
      float /Real (ai::Star::RotationAngle) ,
      float /Real (ai::Star::CenterY) ,
      bool /Bool (ai::Star::Clockwise) ,
      float /Real (ai::Star::Corner1Radius) ,
      int /Int (ai::Star::Corner1Type) ,
      float /Real (ai::Star::Radius2) ,
      float /Real (ai::Star::Corner2Radius) ,
      float /Real (ai::Star::CenterX) ,
      int /Int (ai::Star::Corner2RoundingType) ,
      int /Int (ai::Star::Corner1RoundingType) ,
    ; (ai::LiveShape::Params) ,
    (ai::Star) /UnicodeString (ai::LiveShape::HandlerName) ,
  ; (ai::LiveShape) ,
  (float) /String (BBAccumRotation) ,
;
```

This shape should be parsed into a `path` with arc attributes, i.e. `sodipodi:arc-type` etc.
The plain path should be set as `d` to allow rendering in browsers.

The corner radii for the Corners LPE is calculated as follows:
![star_shape.svg](star_shape.svg)