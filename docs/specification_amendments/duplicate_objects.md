<!--
SPDX-FileCopyrightText: 2025 Manpreet Singh <manpreet.singh.dev@proton.me>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Duplicate Objects

This file contains sequences of SVG elements for the detection of the presence of duplicate elements.

## 2020 and CC

```xml
<g>
    <path d="M 269.5 398.5 L 269.5 149.5 L 718.5 149.5 L 718.5 398.5 L 269.5 398.5 Z"
        style="fill-rule:nonzero;fill:#ffffff;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
    <path
        d="M 718 150 L 718 398 L 270 398 L 270 150 L 718 150 M 719 149 L 269 149 L 269 399 L 719 399 L 719 149 L 719 149 Z"
        style="fill-rule:nonzero;fill:#000000;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
</g>
<rect x="269.0" y="149.0" width="450.0" height="250.0" />
<path d="M 719 399 L 269 399 L 269 149 L 719 149 L 719 399"
    style="fill-rule:nonzero;fill:none;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
<rect x="269.0" y="149.0" width="450.0" height="250.0" />
```

### Fail Case or No Live Shape

```xml
<g>
    <path d="M 269.5 398.5 L 269.5 149.5 L 718.5 149.5 L 718.5 398.5 L 269.5 398.5 Z"
        style="fill-rule:nonzero;fill:#ffffff;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
    <path
        d="M 718 150 L 718 398 L 270 398 L 270 150 L 718 150 M 719 149 L 269 149 L 269 399 L 719 399 L 719 149 L 719 149 Z"
        style="fill-rule:nonzero;fill:#000000;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
</g>
<!-- Live Shape not present -->
<path d="M 719 399 L 269 399 L 269 149 L 719 149 L 719 399"
    style="fill-rule:nonzero;fill:none;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
<!-- Live Shape not present -->
```

## CS

```xml
<g>
    <g>
        <path d="M 269.5 398.5 L 269.5 149.5 L 718.5 149.5 L 718.5 398.5 L 269.5 398.5 Z"
            style="fill-rule:nonzero;fill:#ffffff;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
        <path
            d="M 718 150 L 718 398 L 270 398 L 270 150 L 718 150 M 719 149 L 269 149 L 269 399 L 719 399 L 719 149 L 719 149 Z"
            style="fill-rule:nonzero;fill:#000000;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
    </g>
    <rect x="269.0" y="149.0" width="450.0" height="250.0"
        style="fill:#000000;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
</g>
<rect x="269.0" y="149.0" width="450.0" height="250.0" />
```

### Fail Case or No Live Shape

```xml
<g>
    <g>
        <path d="M 269.5 398.5 L 269.5 149.5 L 718.5 149.5 L 718.5 398.5 L 269.5 398.5 Z"
            style="fill-rule:nonzero;fill:#ffffff;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
        <path
            d="M 718 150 L 718 398 L 270 398 L 270 150 L 718 150 M 719 149 L 269 149 L 269 399 L 719 399 L 719 149 L 719 149 Z"
            style="fill-rule:nonzero;fill:#000000;opacity:1.0;stroke:none;stroke-dasharray:;stroke-linejoin:miter;stroke-linecap:butt;stroke-miterlimit:10.0;stroke-width:1.0" />
    </g>
</g>
```
