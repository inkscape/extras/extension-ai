<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Units and coordinate systems

All data in AI documents are stored in pixels / points (equivalent). 

## Unit list

The unit is specified in the header attributes `AI5_RulerUnits` and `AI24_RulerUnitsLarge`, which takes precedence over the former if specified.

| Unit      | 1 unit = x px          | `AI5_RulerUnits` | `AI24_RulerUnitsLarge` |
| --------- | ---------------------- | -----------------| ---------------------- |
| pt (*)    | 1                      | 2                |                        |
| px (**)   | 1                      | 6                |                        |
| in        | 72                     | 0                |                        |
| pc        | 12                     | 3                |                        |
| ft        | 864  = 12 * 72         | 2                | 10                     |
| ft, in    | 864 (72)               | 2                | 7                      |
| yd        | 2592 = 36 * 72         | 2                | 9                      |
| mm        | 2.8346 = 72 / 25.4     | 1                |                        |
| cm        | 28.346 = 72 / 2.54     | 4                |                        |
| m         | 283.46 = 72 / 0.254    | 2                | 8                      |
| (?) (***) | 0.7086 = 72 / 25.4 / 4 | 5                |                        |

(*): pt is the base unit for paper-based formats, whereas px is the base unit for digital formats.

(**): The only difference between px and pt is that pts are shown in steps of 3 (6, 12, 36, 72, 144, 288, 1152, 2304), and px in steps of 1 (2, 5, 10, 20, 50, 100, 200, 500, 1000, 2000) in the ruler.

(***): This unit can not be set in the UI. It is equivalent to 40 units / cm. Might be related to HPGL (base unit: 40 / millimeter).

## Coordinate system

(Additionally to Section 2.2 of the specification)

Large-canvas documents have the same `%%Canvassize`, but an `%AI24_LargeCanvasScale: 10` instead of `1.` This acts as an additional scale transform onto canvas and drawing coordinate system.

### Canvas coordinate system

* Origin in the top left corner of the dark gray background area
* Size of this "drawing area" given by `%%Canvassize`, which is `16383` in both directions (largest value of a signed 16 bit number). 
Objects cannot be moved to fall completely outside this region. 
* Y axis points down
* Examples: `CenterX` and `CenterY` of live shapes

### Drawing coordinate system

* The origin is at the `ruler origin`. The global ruler origin is still determined by the `AI3_TemplateBox`, `%%PageOrigin` is ignored since the option of multiple artboards. 
* Y axis points up
* Same unit size as the canvas coordinate system

### Display coordinate system

* Origin is the currently selected ruler origin
* y axis points up
* Unit size determined by `AI5_RulerUnits` / `AI24_RulerUnitsLarge`

## Notes for SVG conversion

* The origin (position `0,0`) will be the global ruler origin.
* The viewbox will be coincident with the first page. The width and height attributes are the width/height of the viewbox, with the appropriate unit appended (e.g. `viewBox="0 0 210 297" width="210mm" height="210mm"` for an A4 page with mm units, whose top left corner is at the origin).
* All Canvas coordinates are translated using the ruler origin, and scaled for the Large Canvas:
  ```canvas_to_doc = lambda x, y:  (x - RulerOrigin.x) * LargeCanvasScale, (y - RulerOrigin.y) * LargeCanvasScale```
* All Drawing coordinates are flipped and scaled: 
  ```drawing_to_doc = lambda x,y: x * LargeCanvasScale, -y * LargeCanvasScale ```




