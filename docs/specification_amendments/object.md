<!--
SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Object Modifications

Inside the `<object>` specification, new values were added.
This is an attempt to describe their use.

The following new operators have been added and
seem to be opening/closing operators:

- `Ae` & `AE`
- `Xw` & `XW`

These seem to be one-line operators:

- `As`
- `Ap`

Also, [XML definitions] appear where `<object>` is:

- Layer with XML definitions:
  ```
  %AI5_BeginLayer
  0 1 1 1 0 0 0 3 79 79 255 0 50 0 Lb
  (Sublayer 1_2) Ln
  0 AE
  %_/ArtDictionary :
  %_/XMLUID : (Sublayer_1_x5F_2) ; (AI10_ArtUID) ,
  %_;
  %_
  ```
- Path with XML definitions:
  ```
  4 As
  0 D
  0 O
  0 0 0 0 k
  0 R
  0 0 0 1 K
  0 1 0 0 0 Xy
  0 J 0 j 1 w 10 M []0 d
  0 XR
  46.7144090041866 -149.2474807117 m
  54.2838763462923 -182.301822547631 L
  61.853343688399 -215.356164383562 l
  254.001360834169 -214.580695953397 l
  46.7144090041866 -149.2474807117 l
  b
  %_/ArtDictionary :
  %_;
  %_
  ```

[XML Definitions]: document_data.md