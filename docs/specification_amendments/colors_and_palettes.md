<!--
SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>

SPDX-License-Identifier: GPL-2.0-or-later
-->

# Colors and Palette

## New / changed color types or references

### `Xz`

```
0.9 0.7 0.6 0.9 0 0 0 ([Name]) 0 1 Xz
```

Meaning: unclear

### `Xa`

```
0 0 0 0 1 1 1 Xa
```

According to the AI Spec, p. 61, `Xa` is a only an RGB color. 

**Hypothesis:** the first four colors are CMYK while the last three are RGB, so the color above is process-white.

### Gradient geometry in the gradient reference 

```
2 (Gradient Name) 0 0 0 1 1 0 0 1 0 0 1 Bg
```

The specification has only `flag name xOrigin yOrigin angle length a b c d tx ty Bg`. Since `1 0 0 1 0 0` looks like a unit transformation matrix, there is one extra flag at the end of (until now) unknown origin.

## Palette

Here's a current palette definition:

``` %AI5_BeginPalette
0 0 Pb
0.911711 0.786862 0.619532 0.974487 0 0 0 ([Registration]) 0 1 Xz
([Registration])
Pc
... other entries
1 (Grays) 1 Pg
0.911711 0.786862 0.619532 0.974487 0 0 0 Xa
(R=0 G=0 B=0)
Pc
0.762295 0.667903 0.606867 0.828611 0.101961 0.101961 0.101961 Xa
(R=26 G=26 B=26)
Pc
PB
%AI5_EndPalette
```

The following differences are observed:
- The `Pc` command may be ommitted before the first palette entry.
- Palette entries are named after their color definition / gradient reference (in parentheses).
- Palette entries are grouped with `flag1 (Grays) flag2 Pg` ("palette group"). The meaning of the flags is not yet known.