%%BeginProcSet: Adobe_ColorImage_AI6 1.3 0
userdict /Adobe_ColorImage_AI6 known not
{
	userdict /Adobe_ColorImage_AI6 53 dict put 
} if
userdict /Adobe_ColorImage_AI6 get begin
/initialize { 
	Adobe_ColorImage_AI6 begin
	Adobe_ColorImage_AI6 {
		dup type /arraytype eq {
			dup xcheck {
				bind
			} if
		} if
		pop pop
	} forall
} def
/terminate { end } def
currentdict /Adobe_ColorImage_AI6_Vars known not {
	/Adobe_ColorImage_AI6_Vars 41 dict def
} if
Adobe_ColorImage_AI6_Vars begin
	/plateindex -1 def
	/_newproc null def
	/_proc1 null def
	/_proc2 null def
	/sourcearray 4 array def
	/_ptispace null def
	/_ptiname null def
	/_pti0 0 def
	/_pti1 0 def
	/_ptiproc null def
	/_ptiscale 0 def
	/_pticomps 0 def
	/_ptibuf 0 string def
	/_gtigray 0 def
	/_cticmyk null def
	/_rtirgb null def
	/XIEnable true def
	/XIType 0 def
	/XIEncoding 0 def
	/XICompression 0 def
	/XIChannelCount 0 def
	/XIBitsPerPixel 0 def
	/XIImageHeight 0 def
	/XIImageWidth 0 def
	/XIImageMatrix null def
	/XIRowBytes 0 def
	/XIFile null def
	/XIBuffer1 null def
	/XIBuffer2 null def
	/XIBuffer3 null def
	/XIDataProc null def
	/XIColorSpace /DeviceGray def
	/XIColorValues 0 def
	/XIPlateList false def
end
/ci6colorimage /colorimage where {/colorimage get}{null} ifelse def
/ci6image systemdict /image get def
/ci6curtransfer systemdict /currenttransfer get def
/ci6curoverprint /currentoverprint where {/currentoverprint get}{{_of}} ifelse def
/ci6foureq {
	4 index ne {
		pop pop pop false
	}{
		4 index ne {
			pop pop false
		}{
			4 index ne {
				pop false
			}{
				4 index eq
			} ifelse
		} ifelse
	} ifelse
} def
/ci6testplate {
	Adobe_ColorImage_AI6_Vars begin
		/plateindex -1 def
		/setcmykcolor where {
			pop
			gsave
			1 0 0 0 setcmykcolor systemdict /currentgray get exec 1 exch sub
			0 1 0 0 setcmykcolor systemdict /currentgray get exec 1 exch sub
			0 0 1 0 setcmykcolor systemdict /currentgray get exec 1 exch sub
			0 0 0 1 setcmykcolor systemdict /currentgray get exec 1 exch sub
			grestore
			1 0 0 0 ci6foureq { 
				/plateindex 0 def
			}{
				0 1 0 0 ci6foureq { 
					/plateindex 1 def
				}{
					0 0 1 0 ci6foureq {
						/plateindex 2 def
					}{
						0 0 0 1 ci6foureq { 
							/plateindex 3 def
						}{
							0 0 0 0 ci6foureq {
								/plateindex 5 def
							} if
						} ifelse
					} ifelse
				} ifelse
			} ifelse
			pop pop pop pop
		} if
		plateindex
 end
} def
/ci6concatprocs {
	/packedarray where {
		pop dup type /packedarraytype eq 2 index type
		/packedarraytype eq or
	}{
		false
	} ifelse
	{
		/_proc2 exch cvlit def
		/_proc1 exch cvlit def
		_proc1 aload pop
		_proc2 aload pop
		_proc1 length
		_proc2 length add
		packedarray cvx
	}{
		/_proc2 exch cvlit def
		/_proc1 exch cvlit def
		/_newproc _proc1 length _proc2 length add array def
		_newproc 0 _proc1 putinterval
		_newproc _proc1 length _proc2 putinterval
		_newproc cvx
	} ifelse
} def
/ci6istint {
	type /arraytype eq 
} def
/ci6isspot {
	dup type /arraytype eq {
		dup length 1 sub get /Separation eq
	}{
		pop false
	} ifelse
} def
/ci6spotname {
	dup ci6isspot {dup length 2 sub get}{pop ()} ifelse
} def
/ci6altspace {
	aload pop pop pop ci6colormake
} def
/ci6numcomps {
	dup /DeviceGray eq {
		pop 1
	}{
		dup /DeviceRGB eq {
			pop 3
		}{
			/DeviceCMYK eq {
				4
			}{
				1
			} ifelse
		} ifelse
	} ifelse
} def
/ci6marksplate {
	dup /DeviceGray eq {
		pop plateindex 3 eq
	}{
		dup /DeviceRGB eq {
			pop plateindex 5 ne
		}{
			dup /DeviceCMYK eq {
				pop plateindex 5 ne
			}{
				dup ci6isspot {
					/findcmykcustomcolor where {
						pop
						dup length 2 sub get
						0.1 0.1 0.1 0.1 5 -1 roll
						findcmykcustomcolor 1 setcustomcolor
						systemdict /currentgray get exec
						1 ne
					}{
						pop plateindex 5 ne
					} ifelse
				}{
					pop plateindex 5 ne
				} ifelse
			} ifelse
		} ifelse
	} ifelse
} def
/ci6colormake {
	dup ci6numcomps
	exch 1 index 2 add 1 roll
	dup 1 eq {pop}{array astore} ifelse
	exch
} def
/ci6colorexpand {
	dup ci6spotname exch
	dup ci6istint {
		ci6altspace
		exch 4 1 roll
	}{
		1 3 1 roll
	} ifelse
} def
/ci6colortint {
	dup /DeviceGray eq {
		3 1 roll 1 exch sub mul 1 exch sub exch
	}{
		dup /DeviceRGB eq {
			3 1 roll {1 exch sub 1 index mul 1 exch sub exch} forall pop 3 array astore exch
		}{
			dup /DeviceCMYK eq {
				3 1 roll {1 index mul exch} forall pop 4 array astore exch
			}{
				3 1 roll mul exch
			} ifelse
		} ifelse
	} ifelse
} def
/ci6colortocmyk {
	dup /DeviceGray eq {
		pop 1 exch sub 0 0 0 4 -1 roll 4 array astore
	}{
		dup /DeviceRGB eq {
			pop aload pop _rgbtocmyk 4 array astore
		}{
			dup /DeviceCMYK eq {
				pop
			}{
				ci6altspace ci6colortint ci6colortocmyk
			} ifelse
		} ifelse
	} ifelse
} def
/ci6makeimagedict {
	7 dict begin
		/ImageType 1 def
		/Decode exch def
		/DataSource exch def
		/ImageMatrix exch def
		/BitsPerComponent exch def
		/Height exch def
		/Width exch def
	currentdict end
} def
/ci6stringinvert {
	0 1 2 index length 1 sub {
		dup 2 index exch get 255 exch sub 2 index 3 1 roll put
	} for
} def
/ci6stringknockout {
	0 1 2 index length 1 sub {
		255 2 index 3 1 roll put
	} for
} def
/ci6stringapply {
	0 1 4 index length 1 sub {
		dup
		4 index exch get
		3 index 3 1 roll
		3 index exec
	} for
	pop exch pop
} def
/ci6walkrgbstring {
	0 3 index
	dup length 1 sub 0 3 3 -1 roll {
		3 getinterval {} forall
		5 index exec
		3 index
	} for
	
	 5 {pop} repeat
} def
/ci6walkcmykstring
{
	0 3 index
	dup length 1 sub 0 4 3 -1 roll {
		4 getinterval {} forall
		
		6 index exec
		
		3 index
		
	} for
	
	5 { pop } repeat
	
} def
/ci6putrgbtograystr
{
	.11 mul exch
	
	.59 mul add exch
	
	.3 mul add
	
	cvi 3 copy put
	
	pop 1 add
} def
/ci6putcmyktograystr
{
	exch .11 mul add
	
	exch .59 mul add
	
	exch .3 mul add
	
	dup 255 gt { pop 255 } if
	
	255 exch sub cvi 3 copy put
	
	pop 1 add
} def
/ci6rgbtograyproc {	
	Adobe_ColorImage_AI6_Vars begin 
		sourcearray 0 get exec
		XIBuffer3
		dup 3 1 roll 
		
		/ci6putrgbtograystr load exch
		ci6walkrgbstring
 end
} def
/ci6cmyktograyproc {	
	Adobe_ColorImage_AI6_Vars begin
		sourcearray 0 get exec
		XIBuffer3
		dup 3 1 roll 
		
		/ci6putcmyktograystr load exch
		ci6walkcmykstring
 end
} def
/ci6separatecmykproc {	
	Adobe_ColorImage_AI6_Vars begin
		sourcearray 0 get exec
		
		XIBuffer3
		
		0 2 index
		
		plateindex 4 2 index length 1 sub {
			get 255 exch sub
			
			3 copy put pop 1 add
			
			2 index
		} for
		pop pop exch pop
 end
} def
	
/ci6compositeimage {
	dup 1 eq {
		pop pop image
	}{
		/ci6colorimage load null ne {
			ci6colorimage
		}{
			3 1 roll pop
			sourcearray 0 3 -1 roll put
			3 eq {/ci6rgbtograyproc}{/ci6cmyktograyproc} ifelse load
			image
		} ifelse
	} ifelse
} def
/ci6knockoutimage {
	gsave
	0 ci6curtransfer exec 1 ci6curtransfer exec
	eq {
		0 ci6curtransfer exec 0.5 lt
	}{
		0 ci6curtransfer exec 1 ci6curtransfer exec gt
	} ifelse
	{{pop 0}}{{pop 1}} ifelse
	systemdict /settransfer get exec
	ci6compositeimage
	grestore
} def
/ci6drawimage {
	ci6testplate -1 eq {
		pop ci6compositeimage
	}{
		dup type /arraytype eq {
			dup length plateindex gt {plateindex get}{pop false} ifelse
		}{
			{
				true
			}{
				dup 1 eq {plateindex 3 eq}{plateindex 3 le} ifelse
			} ifelse
		} ifelse
		{
			dup 1 eq {
				pop pop ci6image
			}{
				dup 3 eq {
					ci6compositeimage
				}{
					pop pop
					sourcearray 0 3 -1 roll put
					/ci6separatecmykproc load
					ci6image
				} ifelse
			} ifelse
		}{
			ci6curoverprint {
				7 {pop} repeat
			}{
				ci6knockoutimage
			} ifelse
		} ifelse
	} ifelse
} def
/ci6proctintimage {
	/_ptispace exch store /_ptiname exch store /_pti1 exch store /_pti0 exch store /_ptiproc exch store
	/_pticomps _ptispace ci6numcomps store
	/_ptiscale _pti1 _pti0 sub store
	level2? {
		_ptiname length 0 gt version cvr 2012 ge and {
			[/Separation _ptiname _ptispace {_ptiproc}] setcolorspace
			[_pti0 _pti1] ci6makeimagedict ci6image
		}{
			[/Indexed _ptispace 255 {255 div _ptiscale mul _pti0 add _ptiproc}] setcolorspace
			[0 255] ci6makeimagedict ci6image
		} ifelse
	}{
		_pticomps 1 eq {
			{
				dup
				{
					255 div _ptiscale mul _pti0 add _ptiproc 255 mul cvi put
				} ci6stringapply
			} ci6concatprocs ci6image
		}{
			{
				dup length _pticomps mul dup _ptibuf length ne {/_ptibuf exch string store}{pop} ifelse
				_ptibuf {
					exch _pticomps mul exch 255 div _ptiscale mul _pti0 add _ptiproc
					_pticomps 2 add -2 roll
					_pticomps 1 sub -1 0 {
						1 index add 2 index exch
						5 -1 roll
						255 mul cvi put
					} for
					pop pop
				} ci6stringapply
			} ci6concatprocs false _pticomps
			/ci6colorimage load null eq {7 {pop} repeat}{ci6colorimage} ifelse
		} ifelse
	} ifelse
} def
/ci6graytintimage {
	/_gtigray 5 -1 roll store
	{1 _gtigray sub mul 1 exch sub} 4 1 roll
	/DeviceGray ci6proctintimage
} def
/ci6cmyktintimage {
	/_cticmyk 5 -1 roll store
	{_cticmyk {1 index mul exch} forall pop} 4 1 roll
	/DeviceCMYK ci6proctintimage
} def
/ci6rgbtintimage {
	/_rtirgb 5 -1 roll store
	{_rtirgb {1 exch sub 1 index mul 1 exch sub exch} forall pop} 4 1 roll
	/DeviceRGB ci6proctintimage
} def
/ci6tintimage {
	ci6testplate -1 eq {
		ci6colorexpand
		3 -1 roll 5 -1 roll {0}{0 exch} ifelse 4 2 roll
		dup /DeviceGray eq {
			pop ci6graytintimage
		}{
			dup /DeviceRGB eq {
				pop ci6rgbtintimage
			}{
				pop ci6cmyktintimage
			} ifelse
		} ifelse
	}{
		dup ci6marksplate {
			plateindex 5 lt {
				ci6colortocmyk plateindex get
				dup 0 eq ci6curoverprint and {
					7 {pop} repeat
				}{
					1 exch sub
					exch {1 0}{0 1} ifelse () ci6graytintimage
				} ifelse
			}{
				pop exch {0}{0 exch} ifelse 0 3 1 roll () ci6graytintimage
			} ifelse
		}{
			ci6curoverprint {
				8 {pop} repeat
			}{
				pop pop pop
				{pop 1} 0 1 () /DeviceGray ci6proctintimage
			} ifelse
		} ifelse
	} ifelse
} def
/XINullImage {
} def
/XIImageMask {
	XIImageWidth XIImageHeight false
	[XIImageWidth 0 0 XIImageHeight neg 0 0]
	/XIDataProc load
	imagemask
} def
/XIImageTint {
	XIImageWidth XIImageHeight XIBitsPerPixel
	[XIImageWidth 0 0 XIImageHeight neg 0 0]
	/XIDataProc load
	XIType 3 eq XIColorValues XIColorSpace ci6tintimage
} def
/XIImage {
	XIImageWidth XIImageHeight XIBitsPerPixel
	[XIImageWidth 0 0 XIImageHeight neg 0 0]
	/XIDataProc load
	false XIChannelCount XIPlateList ci6drawimage
} def
/XG {
	pop pop
} def
/XF {
	13 {pop} repeat
} def
/Xh {
	Adobe_ColorImage_AI6_Vars begin
		gsave
		/XIType exch def
		/XIImageHeight exch def
		/XIImageWidth exch def
		/XIImageMatrix exch def
		0 0 moveto
		XIImageMatrix concat
		XIImageWidth XIImageHeight scale
		
		/_lp /null ddef
		_fc
		/_lp /imagemask ddef
 end
} def
/XH {
	Adobe_ColorImage_AI6_Vars begin
		grestore
 end
} def
/XIEnable {
	Adobe_ColorImage_AI6_Vars /XIEnable 3 -1 roll put
} def
/XC {
	Adobe_ColorImage_AI6_Vars begin
		ci6colormake
		/XIColorSpace exch def
		/XIColorValues exch def
 end
} def
/XIPlates {
	Adobe_ColorImage_AI6_Vars begin
		/XIPlateList exch def
 end
} def
/XI
{
	Adobe_ColorImage_AI6_Vars begin
		gsave
		/XIType exch def
		cvi dup
		256 idiv /XICompression exch store
		256 mod /XIEncoding exch store
		pop pop
		/XIChannelCount exch def
		/XIBitsPerPixel exch def
		/XIImageHeight exch def
		/XIImageWidth exch def
		pop pop pop pop
		/XIImageMatrix exch def
		XIBitsPerPixel 1 eq {
			XIImageWidth 8 div ceiling cvi
		}{
			XIImageWidth XIChannelCount mul
		} ifelse
		/XIRowBytes exch def
		XIEnable {
			/XIBuffer3 XIImageWidth string def
			XICompression 0 eq {
				/XIBuffer1 XIRowBytes string def
				XIEncoding 0 eq {
					{currentfile XIBuffer1 readhexstring pop}
				}{
					{currentfile XIBuffer1 readstring pop}
				} ifelse
			}{
				/XIBuffer1 256 string def
				/XIBuffer2 XIRowBytes string def
				{currentfile XIBuffer1 readline pop (%) anchorsearch {pop} if}
				/ASCII85Decode filter /DCTDecode filter
				/XIFile exch def
				{XIFile XIBuffer2 readstring pop}
			} ifelse
			/XIDataProc exch def
			
			XIType 1 ne {
				0 setgray
			} if
			XIType 1 eq {
				XIImageMask
			}{
				XIType 2 eq XIType 3 eq or {
					XIImageTint
				}{
					XIImage
				} ifelse
			} ifelse
		}{
			XINullImage
		} ifelse
		/XIPlateList false def
		grestore
 end
} def
end
%%EndProcSet
