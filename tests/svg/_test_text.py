# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""This tests the creation of text SVG objects."""
import re
import pytest
from inkex import TextElement
from inkai._svg.ai11_text import AIText
from inkai._svg.coordinates import TestTransform
from inkex import (
    SvgDocumentElement,
    TextElement,
    Layer,
    Transform,
    Path,
    PathElement,
    CubicSuperPath,
    ShapeElement,
    TextPath,
)
from inkex.paths import Move, Line


def test_the_last_element_is_a_text_element(svgs):
    """The last element in this document is a text element."""
    svg: SvgDocumentElement = svgs.simple_v2020
    layer_1: Layer = svg[-1]
    assert layer_1.tag_name == "g"
    assert len(layer_1)
    text: TextElement = layer_1[-1]
    print(text)
    assert text.tag_name == "text"
    assert text.get_text() == "Hellow mom\r"


TEXT_EXAMPLE = """/AI11Text :
0 /FreeUndo ,
0 /FrameIndex ,
0 /StoryIndex ,
2 /TextAntialiasing ,
;
%_/ArtDictionary :
%_(be529c8a-ddbf-4f63-b7f1-09f55a88c6e8) /UnicodeString (AI24 TextStoryRawDataUUID) ,
%_;
%_
0 0 Xd
6 () XW
"""


@pytest.mark.parametrize(
    "document",
    ["simple_v2020", "simple_cs"],
)
def test_position_simple_text(svgs, document):
    """The position is specified in canvas coordinates inside the TextDocument"""
    svg: SvgDocumentElement = svgs[document]
    layer_1: Layer = svg[-1]
    text: TextElement = layer_1[-1]
    assert pytest.approx(float(text.get("x"))) == 823.375 + 7231
    assert pytest.approx(float(text.get("y"))) == 299.64844 + 7651
    assert text.transform == Transform(translate=(-228 - 7231, 53 - 7651))


@pytest.mark.parametrize(
    "document",
    ["flowtext"],
)
def test_position_flowtext(svgs, document):
    """The flowtext shape inside is correctly set"""
    text: TextElement = svgs[document][-1][-1]
    pel: PathElement = svgs[document].getElementById(text.style["shape-inside"][5:-1])
    t = pel.path.transform(text.transform)
    # copied from the "Art" section of the main document
    p = Path(
        "m 128.153587543131 97.6853932584263 L 26.2210032734674 97.6853932584263 L 26.2210032734674 37.2808988764036 L 128.153587543131 37.2808988764036 L 128.153587543131 97.6853932584263"
    )
    assert pytest.approx(
        list([i for sublist in t.end_points for i in sublist]), abs=0.05
    ) == list([i for sublist in p.end_points for i in sublist])


def test_convert_ai_to_svg_text():
    """Convert the text."""


def test_transforms_logic():
    """Test the logic behind text transforms"""
    # Text transforms are applied to the (canvas) points of the shape,
    # and then translated into the page coordinate system.
    a = Transform(matrix=(1.0, 0.0, 0.5, 0.86603, 626.0, 553.0))
    pts = (
        3170.30273,
        9335.39355,
        3068.79834,
        9335.39453,
        3068.79834,
        9155.26074,
        3170.30273,
        9155.26074,
    )
    result = []
    expected = [1232.9, 986.6, 1131.4, 986.6, 1041.4, 830.6, 1142.9, 830.6]
    p = Path()
    p.append(Move(*pts[:2]))
    res = Transform(translate=(-7231, -7651), scale=(1, 1)) @ a
    for i in range(len(pts) // 2):
        pt1 = a.apply_to_point((pts[i * 2], pts[i * 2 + 1]))
        pt1 = [pt1[0] - 7231, pt1[1] - 7651]
        pt2 = res.apply_to_point((pts[i * 2], pts[i * 2 + 1]))
        if i < len(pts) // 2 - 2:
            print(pts[2 * i + 2 : 2 * i + 4])
            p.append(Line(*pts[2 * i + 2 : 2 * i + 4]))
        assert pytest.approx(pt1[0]) == pt2[0]
        assert pytest.approx(pt1[1]) == pt2[1]
        assert pytest.approx(pt1[0], 1e-1) == expected[i * 2]
        assert pytest.approx(pt1[1], 1e-1) == expected[i * 2 + 1]


def assert_paths_equal_superpaths(p1, p2):
    """Check that the superpaths are equal, up to small numeric differences"""
    cs1 = CubicSuperPath(Path(p1)).to_path()
    cs2 = CubicSuperPath(Path(p2)).to_path()
    assert len(cs1) == len(cs2), str(cs1)
    for c1, c2 in zip(cs1, cs2):
        assert pytest.approx(c1.args, 1e-4) == c2.args
        assert c1.letter == c2.letter


@pytest.mark.parametrize(
    "element_index,story_index,final_path",
    [
        (0, 0, "m 571 891 L 410 891 L 410 598 L 571 598 L 571 891"),
        (0, 1, "m 983 941 L 741 941 L 741 751 L 983 751 L 983 941 L"),
        (
            2,
            0,
            """m 1038.75 384.281 C 1038.75 384.281 1068.44 419.637 1116.53 306.499 
                C 1164.61 193.361 1092.49 129.721 1179.46 138.914 
                C 1266.44 148.106 1531.6 141.742 1428.36 228.01 
                C 1325.13 314.277 1209.87 423.879 1274.21 450.042 
                C 1338.56 476.205 1298.96 561.058 1182.29 549.038 
                C 1065.62 537.017 998.441 676.317 965.913 560.351 
                C 933.386 444.385 1038.75 384.281 1038.75 384.281""",
        ),
        (
            2,
            1,
            "m 1643.1 641.994 L 1404.095 880.997 L 1293.08 769.981 L 1532.082 530.978 Z",
        ),
        (
            4,
            1,
            "m 315.583 687.952 L 220.983 653.507 L 220.983 423.407 L 315.583 457.852 Z",
        ),
    ],
)
def test_flowtext_final_coords(svgs, element_index, story_index, final_path):
    element: ShapeElement = svgs.text_transforms[-1][element_index]
    assert isinstance(element, TextElement)

    urlmatch = list(
        re.finditer(r"url\(#([\w\d]*)\)", element.style.get("shape-inside"))
    )
    pe1: PathElement = element.root.getElementById(urlmatch[story_index].group(1))
    p = pe1.path.transform(pe1.transform @ element.transform)
    assert_paths_equal_superpaths(p, final_path)


@pytest.mark.parametrize(
    "element_index,final_path",
    [
        # The two stories have different transforms, only the first is used.
        (
            5,
            """M 365.774 513.691 
            C 448.121 370.291 420.055 331.054 525.876 270.531 
            C 631.61 210.057 829.978 -30.2931 780.856 76.5328 
            C 731.709 183.455 689.106 426.875 689.106 426.875 
            L 806.641 264.71""",
        ),
        # The transforms only differ by a translate, so we can just
        # append the path.
        (6, "M 84 79 L 343 79 M 176.61 141.22 L 435.61 141.22"),
        # Same, but with additional rotate
        (
            7,
            "M 254 324 L 221.13 257 L 294 188 M 350.24 207.04 L 383.11 274.04 L 310.24 343.04",
        ),
    ],
)
def test_textpath_final_coords(svgs, element_index, final_path):
    element: ShapeElement = svgs.text_transforms[-1][element_index]
    assert isinstance(element, TextElement)
    assert isinstance(element[0], TextPath)

    urlmatch = element[0].get("xlink:href")

    pe1: PathElement = element.root.getElementById(urlmatch)
    p = pe1.path.transform(pe1.transform @ element.transform)
    assert_paths_equal_superpaths(p, final_path)


@pytest.mark.parametrize(
    "element_index,transform",
    [
        (2, "rotate(-45)"),
        (4, "matrix(0.94619 -0.32362 0 1 -7534 -7475)"),
        (5, "matrix(0.86603 -0.5 -0.25882 0.96593 -10012.9 -2614.69)"),
        (6, ""),
        (7, "translate(-7231, -7651)"),
        (10, "matrix(-0.5 -0.86603 0.86603 -0.5 -2360.04 10676.3)"),
        (11, "matrix(1 0 0.34202 0.93969 -7231 -7651)"),
    ],
)
def test_text_transform(svgs, element_index, transform):
    """Translate is already checked in the previous two tests"""
    element: ShapeElement = svgs.text_transforms[-1][element_index]
    assert isinstance(element, TextElement)
    t = Transform(transform)
    print(t)
    print(element.transform)
    assert pytest.approx(element.transform.a, 1e-4) == t.a
    assert pytest.approx(element.transform.b, 1e-4) == t.b
    assert pytest.approx(element.transform.c, 1e-4) == t.c
    assert pytest.approx(element.transform.d, 1e-4) == t.d
