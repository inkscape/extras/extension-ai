# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test paths."""

from typing import Union

import pytest
from inkex import Group, PathElement, SvgDocumentElement


def is_closed(path: Union[PathElement, Group]) -> bool:
    if isinstance(path, Group):
        return path[0][0].path[-1].letter in "zZ"
    return path.path[-1].letter in "zZ"


@pytest.mark.parametrize(
    "file, expected",
    [
        ["stroke_styles.ai", [False] * 3],
        ["stroke_styles_cs.ai", [None] * 8 + [False] * 3],
        ["fill_stroke_test.ai", [False] * 3 + [None] + ([True] * 3 + [None]) * 2],
        ["fill_stroke_test_cc.ai", [False] * 3 + [None] + ([True] * 3 + [None]) * 2],
        [
            "fill_stroke_test_cs.ai",
            [None] * 4
            + [False] * 3
            + [None]
            + [True] * 3
            + [None]
            + [True] * 3
            + [None],
        ],
    ],
)
def test_path_closed(svgs, file, expected):
    """Check if paths are closed/open."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]

    path_elements = [
        el for el in layer if isinstance(el, PathElement) or el.xpath(".//svg:path")
    ]

    assert len(path_elements) == len(expected), path_elements

    for path, expect in zip(path_elements, expected):
        if expect is None:
            continue
        assert is_closed(path) == expect
