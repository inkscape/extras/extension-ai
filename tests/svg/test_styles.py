# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test the styles."""

import pytest
from conftest import assertAttributesAlmostEqual
from inkex import Color, Group, PathElement, Rectangle, SvgDocumentElement


def test_styles(svgs):
    """Check the styles."""
    svg: SvgDocumentElement = svgs.rgb_rectangle
    rect: Rectangle = svg[-1][0]
    print(rect.tostring())
    assert rect.style("fill") == Color((236, 31, 35))
    assert rect.style("stroke") == Color((0, 0, 0))
    assert rect.style("stroke-width") == "1.0"
    assert rect.style("opacity") == 1


def test_style_state_based(svgs):
    """Check if styles are state based.
    ie. once set, the styles remain set for all subsequent elements until changed."""
    svg: SvgDocumentElement = svgs.style_state
    p1 = svg[0][0]
    p2 = svg[0][1]
    assert p1.style("fill") == p2.style("fill") == Color((255, 0, 0))


@pytest.mark.parametrize(
    "file",
    [
        "stroke_styles.ai",
        pytest.param(
            "stroke_styles_cs.ai",
            marks=pytest.mark.xfail(reason="Known styles not present."),
        ),
    ],
)
def test_stroke_styles(svgs, file):
    """Check stroke styles."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]

    assert len(layer) == 12
    assert all(isinstance(o, Rectangle) for o in layer[:9]), [
        type(o).__name__ for o in layer[:9]
    ]
    assert all(isinstance(o, PathElement) for o in layer[9:]), [
        type(o).__name__ for o in layer[9:]
    ]

    assert all(float(o.style("stroke-width")) == 10 for o in layer), [
        o.style("stroke-width") for o in layer
    ]
    assert all(o.style("stroke") == Color((0.0, 1.0, 1.0)) for o in layer), [
        o.style("stroke") for o in layer
    ]

    assert layer[0].get("inkscape:path-effect") is None
    assertAttributesAlmostEqual(
        svg.getElementById(layer[1].get("inkscape:path-effect")),
        {
            "effect": "offset",
            "is_visible": "true",
            "lpeversion": "1",
            "offset": -5.0,
            "unit": "px",
            "linejoin_type": "miter",
            "miter_limit": 10,
            "attempt_force_join": "false",
            "update_on_knot_move": "true",
        },
    )
    assertAttributesAlmostEqual(
        svg.getElementById(layer[2].get("inkscape:path-effect")),
        {
            "effect": "offset",
            "is_visible": "true",
            "lpeversion": "1",
            "offset": 5.0,
            "unit": "px",
            "linejoin_type": "miter",
            "miter_limit": 10,
            "attempt_force_join": "false",
            "update_on_knot_move": "true",
        },
    )

    assert layer[3].style("stroke-linejoin") == "miter"
    assert layer[3].style("stroke-miterlimit") == "1.0"
    assert layer[4].style("stroke-linejoin") == "round"
    assert layer[4].style("stroke-miterlimit") == "10.0"
    assert layer[5].style("stroke-linejoin") == "bevel"
    assert layer[5].style("stroke-miterlimit") == "10.0"

    assert layer[6].style["stroke-dasharray"] == "12.0"
    assert layer[7].style["stroke-dasharray"] == "12.0 12.0"
    assert layer[8].style["stroke-dasharray"] == "12.0 6.0 12.0"

    assert layer[9].style("stroke-linecap") == "butt"
    assert layer[10].style("stroke-linecap") == "round"
    assert layer[11].style("stroke-linecap") == "square"


@pytest.mark.parametrize(
    "file, expected",
    [
        ["stroke_styles.ai", [0, -5.0, 5.0] + [5.0] * 6 + [0] * 3],
        pytest.param(
            "stroke_styles_cs.ai",
            [0, -5.0, 5.0] + [5.0] * 6 + [0] * 3,
            marks=pytest.mark.xfail(reason="Known styles not present."),
        ),
        ["fill_stroke_test.ai", [0] * 4 + [-5, None] * 2 + [0] * 8 + [-5, None] * 2],
        ["fill_stroke_test_cc.ai", [0] * 4 + [-5, None] * 2 + [0] * 8 + [-5, None] * 2],
        pytest.param(
            "fill_stroke_test_cs.ai",
            [0] * 4 + [-5] * 4 + [0] * 8 + [-5] * 4,
            marks=pytest.mark.xfail(reason="Known styles not present."),
        ),
    ],
)
def test_stroke_align(svgs, file, expected):
    """Check stroke alignment."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]

    elements = [el for el in layer if not isinstance(el, Group)]

    assert len(elements) == len(expected), elements

    for ele, expect in zip(elements, expected):
        if expect is None:
            continue
        if expect == 0:
            assert (
                ele.get("inkscape:path-effect") is None
            ), f"offset: {expect} != {svg.getElementById(ele.get('inkscape:path-effect')).get('offset')}"
            continue

        assertAttributesAlmostEqual(
            svg.getElementById(ele.get("inkscape:path-effect")),
            {
                "effect": "offset",
                "is_visible": "true",
                "lpeversion": "1",
                "offset": expect,
                "unit": "px",
                "attempt_force_join": "false",
                "update_on_knot_move": "true",
            },
        )


@pytest.mark.parametrize(
    "file",
    [
        "opacity.ai",
        "opacity_cs.ai",
    ],
)
def test_opacity(svgs, file):
    """Check that the opacity is applied correctly to all elements and groups."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]

    for ele in layer:
        assert ele.style("opacity") == 0.25
