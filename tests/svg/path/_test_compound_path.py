# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test compound paths."""

from inkai._svg.coordinates import TestTransform
from inkex import PathElement
from inkex.paths import Line, Move, ZoneClose
from inkai._svg.path.commands import PathCommands
from inkai._svg.interpreter.state import StatefulInterpreter
from inkai.parser.objects.path import CompoundPath, m, PathRender
from inkai._svg.modification import AddChildAttributes
import pytest
from inkai._svg.path import PathElement
from inkai._svg.modification import AddChild
from inkai._svg.path import PathElement as InkAIPathElement
from inkai._svg.interpreter import CollectAIElements


TRANSPARENT_MIDDLE = """0 0 m
10 0 L
10 10 L
0 10 L
s
5 5 m
15 5 L
15 15 L
5 15 L
s"""

COMPOUND_PATH_WITH_TRANSPARENT_MIDDLE = f"""*u
{TRANSPARENT_MIDDLE}
*U"""


def test_path_element_is_created_for_compound_path_once():
    """m creates a path element."""
    mods = PathElement().generate_modifications_for(CompoundPath.example_string)
    mods.assert_matches([AddChild])
    assert mods.child.tag_name == "path"


def test_path_element_is_not_created_inside_of_a_compound_path():
    """Only one path element is created for a compound path."""
    mods = PathElement().generate_modifications_for(
        "m", is_creating_a_compound_path=True
    )
    mods.assert_matches([])


@pytest.mark.parametrize("i", [0, 1])
def test_compound_path_content_overlaps_and_is_transparent(i):
    """Merge to paths with overlap so they are transparent in the middle.
    > A compound path is a group of two or more paths that are painted so that
    > overlapping paths can appear transparent.
    See page 59 AI Spec.
    Discussion: https://gitlab.com/inkscape/extras/extension-ai/-/issues/43
    """
    modifications = PathCommands(TestTransform()).generate_modifications_for(
        TRANSPARENT_MIDDLE, is_creating_a_compound_path=True
    )
    modifications.assert_matches([AddChildAttributes, AddChildAttributes])
    mod: AddChildAttributes = modifications.modifications[i]
    expected = [
        [
            Move(0, 0),
            Line(-10, 0),
            Line(-10, -10),
            Line(0, -10),
            ZoneClose(),
        ],
        [
            Move(-5, -5),
            Line(-15, -5),
            Line(-15, -15),
            Line(-5, -15),
            ZoneClose(),
        ],
    ]
    assert mod.attributes["d"] == expected[i]


cp = CompoundPath.example()


def test_transition_to_compound_path(stmi: StatefulInterpreter):
    """Check that the compond path is transitioned properly."""
    assert not stmi.state.is_creating_a_compound_path()
    stmi.before.interpret(cp)
    assert stmi.state.is_creating_a_compound_path()
    stmi.after.interpret(cp)
    assert not stmi.state.is_creating_a_compound_path()


def test_path_inside_compound_path(stmi: StatefulInterpreter):
    """Check that we are in a path in a compound path."""
    stmi.before.interpret(cp)
    stmi.before.interpret(m.example())
    assert stmi.state.is_creating_a_compound_path()
    assert stmi.state.is_creating_a_path()
    stmi.after.interpret(PathRender.example())
    assert stmi.state.is_creating_a_compound_path()
    assert not stmi.state.is_creating_a_path()
    stmi.after.interpret(cp)
    assert not stmi.state.is_creating_a_path()


def test_recursively_iterate_the_compound_path():
    """We should recursively iterate over the content."""
    pe = InkAIPathElement()
    c = CollectAIElements()
    (pe + c).generate_modifications_for(
        COMPOUND_PATH_WITH_TRANSPARENT_MIDDLE, is_creating_a_compound_path=True
    )
    print(c.ai_elements)
    assert len(c.ai_elements) == 11
