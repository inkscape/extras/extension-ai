# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the conversion of AI files to SVG files."""

from inkex import SvgDocumentElement
from inkai.util import to_pretty_xml
import pytest


@pytest.mark.filterwarnings("ignore::inkai.warnings.NotWorthImplementingWarning")
def test_convert_ai_file_to_svg_file(process):
    """Walk though processing the files step by step to see where we are at."""
    print(process)
    svg: SvgDocumentElement = process.svg
    assert svg.viewport_height
    assert svg.viewport_width
    with open(process.path + ".svg", "wb") as f:
        f.write(to_pretty_xml(svg.tostring()))


@pytest.mark.filterwarnings("ignore::inkai.warnings.NotWorthImplementingWarning")
def test_path_effects_have_different_ids(process):
    """The path effects need to have different ids."""
    svg: SvgDocumentElement = process.svg
    assert len({d.get_id() for d in svg.defs}) == len(svg.defs)
