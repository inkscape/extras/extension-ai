# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test the transforms.

See docs/specification_amendments/units.md
"""
from unittest.mock import MagicMock
import pytest
from inkai.svg.coordinates import SVGTransform
from collections import namedtuple
from inkex import SvgDocumentElement, Rectangle

Page = namedtuple("Page", ("left", "top"))
Point = namedtuple("Point", ("x", "y"))


class DocumentTestInfo:
    """A test/mock header."""

    def __init__(self, large_canvas_scale, ruler_x=0, ruler_y=0, units="px"):
        """Create a test header."""
        self.header = MagicMock()
        self.header.large_canvas_scale = large_canvas_scale
        self.drawing_first_page_position = Page(ruler_x, ruler_y)
        self.canvas_ruler_origin = Point(100, 200)
        self.units = units


@pytest.mark.parametrize(
    "lcs,x,y,tx,ty",
    [
        (1, 0, 0, 0, 0),
        (2, 1, 1, 2, -2),
        (10.3, 33, 12, 10.3 * 33, -10.3 * 12),
    ],
)
@pytest.mark.parametrize("c", [1, 2, 3])
def test_drawing_to_svg(lcs, x, y, tx, ty, c):
    """Test the drawing coordinate_transform.

    We also check the canvas transform.
    This uses the canvas_ruler_origin additionally.
    See test_document_information.py::test_location_of_canvas_ruler_origin
    """
    input = [x, y] * c
    expected = [tx, ty] * c
    output = SVGTransform(DocumentTestInfo(lcs)).from_drawing(*input)
    assert output == expected


@pytest.mark.parametrize(
    "lcs,x,y,tx,ty",
    [
        (1, 0, 0, -100, -200),
        (2, 1, 1, -198, -398),
        (10.3, 33, 12, -690.1, -1936.4),
    ],
)
@pytest.mark.parametrize("c", [1, 2, 3])
def test_drawing_to_svg(lcs, x, y, tx, ty, c):
    """Test the canvas coordinate_transform.

    This uses the canvas_ruler_origin.
    See test_document_information.py::test_location_of_canvas_ruler_origin
    """
    input = [x, y] * c
    expected = [tx, ty] * c
    canvas = SVGTransform(DocumentTestInfo(lcs)).from_canvas(*input)
    assert canvas == expected


def test_large_canvas_scale():
    """Check that the large canvase scale matches."""
    assert SVGTransform(DocumentTestInfo(10))._large_canvas_scale == 10
    assert SVGTransform(DocumentTestInfo(1))._large_canvas_scale == 1


def test_canvas_coordinates_compared_to_path(svgs):
    """Check that the canvas transform actually works with the rectangles
    so that they are on top of the path.

    b'<rect x="680.0" y="-419.0" width="699.0" height="370.0"/>'
    b'<g><path d="M 680.5 788.5 L 680.5 419.5 L 1378.5 419.5 L 1378.5 788.5 L 680.5 788.5 Z" sodipodi:nodetypes="sccccs" style="fill:#FFFFFF;stroke:#000000;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:0;stroke-width:1.0;stroke-miterlimit:10.0;fill-opacity:1"/><path d="M 680.5 788.5 L 680.5 419.5 L 1378.5 419.5 L 1378.5 788.5 L 680.5 788.5 Z M 1378 420 L 1378 788 L 681 788 L 681 420 L 1378 420 M 1379 419 L 680 419 L 680 789 L 1379 789 L 1379 419 L 1379 419 Z" sodipodi:nodetypes="sccccssccccscccccs" style="fill:#000000;stroke:#000000;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:0;stroke-width:1.0;stroke-miterlimit:10.0;fill-opacity:1"/></g>'
    """
    svg: SvgDocumentElement = svgs.simple_cs5
    rect: Rectangle = svg[-1].xpath("svg:rect")[0]
    print(rect.tostring())
    assert rect.get("x") == "680.0"
    assert rect.get("y") == "419.0"
