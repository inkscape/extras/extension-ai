# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This adds tests for recognizing rectangles.

See https://gitlab.com/inkscape/extras/extension-ai/-/issues/27
"""

import math
import pytest
import inkex
from conftest import assertAttributesAlmostEqual


from inkai.svg.live_shape.types import Rectangle
from inkai.svg.contextmanager import DocumentContextManager

simple_rect = {
    "Width": 50,
    "Height": 100,
    "CenterX": 7924,
    "CenterY": 7825,
    "Clockwise": 1,
    "Angle": math.pi / 3,
    "RoundingType::0": "Absolute",
    "RoundingType::1": "Absolute",
    "RoundingType::2": "Absolute",
    "RoundingType::3": "Absolute",
    "CornerType::0": "Chamfer",
    "CornerType::1": "Chamfer",
    "CornerType::2": "Chamfer",
    "CornerType::3": "Chamfer",
    "CornerRadius::0": 0,
    "CornerRadius::1": 0,
    "CornerRadius::2": 0,
    "CornerRadius::3": 0,
}
simple_rect_result = {
    "width": 50,
    "height": 100,
    "x": 6,
    "y": 5,
    "inkscape:path-effect": None,
    "transform": str(inkex.Transform(f"rotate(-60, {6 + 50 /2}, {5 + 100/2})")),
}


def rectdict(dict) -> Rectangle:
    """prefix every key, and return a Rectangle"""
    return Rectangle({f"ai::Rectangle::{key}": value for key, value in dict.items()})


@pytest.mark.parametrize(
    "dict, result",
    [
        (simple_rect, {**simple_rect_result, **{"ry": None}}),
        (
            {
                **simple_rect,
                **{f"CornerType::{i}": "Normal" for i in range(4)},
                **{f"CornerRadius::{i}": 2 for i in range(4)},
            },
            {**simple_rect_result, **{"ry": 2}},
        ),
    ],
)
def test_create_rectangle(documents, dict, result):
    """Add a rectangle in a known state"""
    dc = DocumentContextManager(documents.empty)
    rectangle = dc.lscm.create_rectangle(rectdict(dict))

    assert isinstance(rectangle, inkex.Rectangle)

    assertAttributesAlmostEqual(rectangle, result)


def test_relative_corner(documents):
    dc = DocumentContextManager(documents.empty)
    rectangle = dc.lscm.create_rectangle(
        rectdict({**simple_rect, "RoundingType::0": "Relative"})
    )
    assert rectangle is None


rect_with_unequal_corners = {
    **simple_rect,
    "CornerType::0": "Chamfer",
    "CornerType::1": "Normal",
    "CornerType::2": "Inverted",
    "CornerType::3": "Chamfer",
    "CornerRadius::0": 2,
    "CornerRadius::1": 3,
    "CornerRadius::2": 4,
    "CornerRadius::3": 0,
}

expected_lpe = "IF,0,0,1,0,4.000000,0,1 @ C,0,0,1,0,0.000000,0,1 @ C,0,0,1,0,2.000000,0,1 @ F,0,0,1,0,3.000000,0,1"


def test_unequal_corners(documents):
    """Add a rectangle in a known state"""
    dc = DocumentContextManager(documents.empty)
    rectangle = dc.lscm.create_rectangle(rectdict(rect_with_unequal_corners))
    lpe = dc.root_element.getElementById(rectangle.get("inkscape:path-effect"))

    assert isinstance(lpe, inkex.PathEffect)
    assertAttributesAlmostEqual(
        lpe, {"satellites_param": expected_lpe, "nodesatellites_param": expected_lpe}
    )
