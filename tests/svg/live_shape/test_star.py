# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This adds tests for recognizing stars."""

import inkex
import pytest
from conftest import assertAttributesAlmostEqual

from inkai.svg.contextmanager import DocumentContextManager
from inkai.svg.live_shape.types import Star

simple_star = {
    "NumPoints": 5,
    "IsRegular": True,
    "Clockwise": True,
    "CenterX": 100,
    "CenterY": 100,
    "Radius1": 100,
    "Radius2": 75,
    "Corner1Type": 0,
    "Corner2Type": 0,
    "Corner1Radius": 0,
    "Corner2Radius": 0,
    "RotationAngle": 1.5708,
    "Corner1RoundingType": 1,
    "Corner2RoundingType": 1,
}
simple_star_result = {
    "sodipodi:cx": -7793.0,
    "sodipodi:cy": -7670.0,
    "sodipodi:r1": 100.0,
    "sodipodi:r2": 75.0,
    "sodipodi:arg1": -1.5708,
    "sodipodi:arg2": -0.9424814692820414,
    "sodipodi:sides": 5,
    "inkscape:rounded": 0,
    "inkscape:flatsided": "false",
    "sodipodi:type": "star",
}


def stardict(dict) -> Star:
    """prefix every key, and return a Star"""
    return Star({f"ai::Star::{key}": value for key, value in dict.items()})


@pytest.mark.parametrize(
    "dict, result",
    [
        (simple_star, {**simple_star_result, **{"ry": None}}),
        (
            {
                **simple_star,
                **{f"Corner{i}Type": 2 for i in range(2)},
                **{f"Corner{i}Radius": 5 for i in range(2)},
            },
            {**simple_star_result},
        ),
    ],
)
def test_create_star(documents, dict, result):
    """Add a star in a known state"""
    dc = DocumentContextManager(documents.empty)
    star = dc.lscm.create_star(stardict(dict))

    assert isinstance(star, inkex.PathElement)

    assertAttributesAlmostEqual(star, result)


star_with_corners = {
    **simple_star,
    "Corner1Type": 1,
    "Corner2Type": 2,
    "Corner1Radius": 15,
    "Corner2Radius": 5,
}

expected_lpe = "F,0,0,1,0,13.380304,0,1 @ IF,0,0,1,0,0.502029,0,1 @ F,0,0,1,0,13.380304,0,1 @ IF,0,0,1,0,0.502029,0,1 @ F,0,0,1,0,13.380304,0,1 @ IF,0,0,1,0,0.502029,0,1 @ F,0,0,1,0,13.380304,0,1 @ IF,0,0,1,0,0.502029,0,1 @ F,0,0,1,0,13.380304,0,1 @ IF,0,0,1,0,0.502029,0,1"


def test_corners(documents):
    """Add a star in a known state"""
    dc = DocumentContextManager(documents.empty)
    star = dc.lscm.create_star(stardict(star_with_corners))
    lpe = dc.root_element.getElementById(star.get("inkscape:path-effect"))

    assert isinstance(lpe, inkex.PathEffect)
    assertAttributesAlmostEqual(
        lpe, {"satellites_param": expected_lpe, "nodesatellites_param": expected_lpe}
    )
