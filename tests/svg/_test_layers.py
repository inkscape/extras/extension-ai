# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests the layers.

Requirements:
- If there are no layers in the AIFile, Inkscape still likes to have one.
- Layers can be nested.
"""

import pytest
from inkai._svg.layer import Layer
import inkex

RECURSIVE_LAYERS = """%AI5_BeginLayer
(1) Ln
%AI5_BeginLayer
(1.1) Ln
%AI5_EndLayer--
%AI5_EndLayer--
"""


def test_layers_can_be_in_layers():
    """Check that we can add layers to other layers."""
    layer: inkex.Layer = Layer().generate_modifications_for(RECURSIVE_LAYERS).child
    assert layer.label == "1"
    assert len(layer)
    assert layer[0].label == "1.1"
