# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests combinig factories.

When factories are combined, they use the StatefulInterpreter.
"""
from inkai._svg.interpreter import ModificationFactory, on_element
from inkai.parser.objects.path import m
from inkai._svg.modification import NullModification
import pytest


class MyFactory(ModificationFactory):
    """Just for tests"""

    passed = False

    @on_element(m)
    def pass_element(self, ai_element: m):
        """pass the element"""
        self.passed = True
        self.submit_modification(NullModification())


def test_my_factory():
    """Check that we pass an element."""
    assert not MyFactory().passed
    assert MyFactory().generate_modifications_for(m.example_string).factory.passed


def test_combined_factories_pass_all_subscriptions():
    """Both factories pass the element."""
    m1 = MyFactory()
    m2 = MyFactory()
    combined = m1 + m2
    combined.generate_modifications_for(m.example_string)
    assert m1.passed
    assert m2.passed


@pytest.mark.parametrize(
    "factories,count",
    [
        (MyFactory() + MyFactory(), 2),
        (MyFactory() + MyFactory() + MyFactory(), 3),
        (MyFactory() + MyFactory() + MyFactory() + MyFactory(), 4),
        ((MyFactory() + MyFactory()) + (MyFactory() + MyFactory()), 4),
    ],
)
def test_combined_factories_send_out_all_modifications(factories, count):
    """The modifications must be there, too."""
    factories.generate_modifications_for(m.example_string).assert_matches(
        [NullModification] * count
    )
