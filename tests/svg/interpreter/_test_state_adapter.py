# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the adapter from STM to ElementInterpreter"""
from statemachine.exceptions import TransitionNotAllowed
from inkai._svg.interpreter.ai_state.adapter import AISTMAdapter
import pytest


class ExampleSTM:
    debug = True

    def __init__(self, mock):
        self.mock = mock

    def do(self, transition):
        raise TransitionNotAllowed(self.mock.event, self.mock.state)


def test_add_line_of_command_to_error_message(mock):
    """The line and element in which the transition was not allowed should be easily traceable."""
    a = AISTMAdapter(ExampleSTM(mock), {mock.selector.category: mock.transition})
    with pytest.raises(TransitionNotAllowed) as e:
        a.interpret_element(mock.selector, mock.element, mock.interpreter)
    print(str(e))
    assert repr(mock.event) in str(e), "given by library"
    assert repr(mock.state.name) in str(e), "given by library"
    assert repr(mock.element) in str(e)
