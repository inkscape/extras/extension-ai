# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test that the state classes work.

This does not test the state machine.
"""
from inkai._svg.interpreter.ai_state.invalid import InvalidAIState
from inkai._svg.interpreter.ai_state.static import StaticState
from inkai._svg.interfaces import IAIState
import pytest

ATTRS = list(IAIState.__abstractmethods__)


@pytest.mark.parametrize("attr", ATTRS)
@pytest.mark.parametrize("State", [InvalidAIState, lambda: StaticState({})])
def test_invalid_states_raises_error(attr, State):
    """We want the invalid state to make sure that these methods cannot be used."""
    with pytest.raises(RuntimeError) as e:
        getattr(State(), attr)()
    assert attr in str(e.errisinstance)


@pytest.mark.parametrize("attr", ATTRS)
@pytest.mark.parametrize("value", [True, False])
def test_can_override_value(attr, value):
    """The StaticState can chaneg the value of methods."""
    state = StaticState({attr: value})
    method = getattr(state, attr)
    assert method() == value


def test_cannot_use_unused_attribute():
    """We cannot use every possible attribute.

    This prevents typing mistakes.
    """
    with pytest.raises(ValueError):
        StaticState({"is_nanana": False})
