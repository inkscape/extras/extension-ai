# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test if duplicate objects are removed."""
import pytest
from inkex import (
    Circle,
    Color,
    Ellipse,
    Group,
    PathElement,
    Rectangle,
    SvgDocumentElement,
)


@pytest.mark.parametrize(
    "file, element",
    [
        ("rectangle", Rectangle),
        ("circle", Circle),
        ("rectangle_with_rounded_corners", Rectangle),
    ],
)
def test_duplicates_are_removed(svgs, file, element):
    """Check if duplicate objects are removed."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]
    assert (
        len(layer) == 1
    ), f"Expected only one object in the layer, but found {len(layer)}."
    assert isinstance(
        layer[0], element
    ), f"Expected an instance of {element.__name__}, but found {type(layer[0]).__name__}."


@pytest.mark.parametrize(
    "file",
    [
        "fill_stroke_test.ai",
        "fill_stroke_test_cc.ai",
        "fill_stroke_test_cs.ai",
    ],
)
def test_duplicates(svgs, file):
    """Check if duplicate objects are removed."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]
    is_cs = "_cs.ai" in file

    # check objects
    assert (
        len(layer) == 20
    ), f"Expected 20 objects in the layer, but found {len(layer)}."

    # Rectangles
    e_types = [Rectangle] * 4 + ([Group] * 4 if is_cs else [Rectangle] * 4)
    assert all(
        isinstance(r, t) for r, t in zip(layer[:8], e_types)
    ), f"Expected {e_types}, but found {', '.join([type(r).__name__ for r in layer[:8]])}."

    # Paths
    e_types = [PathElement] * 8 + (
        [Group, PathElement] * 2 if is_cs else [PathElement] * 12
    )
    assert all(
        isinstance(p, t) for p, t in zip(layer[8:], e_types)
    ), f"Expected {e_types}, but found {', '.join([type(p).__name__ for p in layer[8:]])}."


@pytest.mark.parametrize(
    "file",
    [
        "fill_stroke_test.ai",
        "fill_stroke_test_cc.ai",
        "fill_stroke_test_cs.ai",
    ],
)
def test_fill_and_stroke(svgs, file):
    """Check if duplicate objects are removed and object properties are retained/added correctly."""

    # check if the colors are correct
    def assert_styles(objects, fill, stroke) -> None:
        for o, f, s in zip(objects, fill, stroke):
            assert o.style("stroke") == s
            assert o.style("fill") == f

    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]
    is_cs = "_cs.ai" in file

    assert_styles(
        layer[:4] + layer[8:16],
        [Color((1.0, 0, 0)), Color((1.0, 0, 0)), None, None] * 3,
        [Color((0, 0, 0)), None, Color((0, 0, 0)), None] * 3,
    )

    if not is_cs:
        assert_styles(
            layer[4:8],
            [Color((1.0, 0, 0)), Color((1.0, 0, 0)), None, None],
            [Color((0, 0, 0)), None, Color((0, 0, 0)), None],
        )
        assert_styles(
            layer[16:],
            [Color((1.0, 0, 0)), Color((1.0, 0, 0)), None, None],
            [Color((0, 0, 0)), None, Color((0, 0, 0)), None],
        )
    else:
        assert_styles(
            [i[0] for i in layer[4:8]],
            [Color((1.0, 0, 0)), Color((1.0, 0, 0)), Color((0, 0, 0)), None],
            [None] * 4,
        )
        assert_styles(layer[4][1], [Color((0, 0, 0))], [None])

        assert_styles(
            [i[0] for i in layer[4:6]],
            [Color((1.0, 0, 0)), Color((1.0, 0, 0))],
            [None] * 2,
        )
        assert_styles(
            [i[-1] for i in layer[4:8]],
            [Color((0, 0, 0)), Color((1.0, 0, 0)), Color((0, 0, 0)), None],
            [None] * 4,
        )

        assert_styles([layer[16][0][0]], [Color((1.0, 0, 0))], [None])
        assert_styles([layer[16][0][1]], [Color((0, 0, 0))], [None])
        assert_styles([layer[18][0][0]], [Color((0, 0, 0))], [None])


@pytest.mark.parametrize(
    "file",
    [
        "fill_stroke_test.ai",
        "fill_stroke_test_cc.ai",
        pytest.param(
            "fill_stroke_test_cs.ai",
            marks=pytest.mark.xfail(reason="Known styles not present."),
        ),
    ],
)
def test_stroke_width(svgs, file):
    """Check if the stroke width after rmoving duplicates is correct."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]
    is_cs = file == "fill_stroke_test_cs.ai"

    def assert_stroke_width(objects, width):
        for o in objects:
            assert float(o.style("stroke-width")) == width

    assert_stroke_width([layer[i * 4] for i in range(4 if is_cs else 5)], 10)
    assert_stroke_width([layer[i * 4 + 2] for i in range(4 if is_cs else 5)], 10)


@pytest.mark.parametrize(
    "file",
    [
        "simple_shapes.ai",
        "simple_shapes_cc.ai",
        "simple_shapes_cs.ai",
    ],
)
def test_simple_shapes(svgs, file):
    """Check if duplicate shapes are removed."""
    svg: SvgDocumentElement = svgs[file]
    layer = svg[-1]
    is_cs = "_cs.ai" in file
    assert len(layer) == 6

    if is_cs:
        assert all(
            isinstance(i, Group) for i in layer[:4]
        ), f"Expected Groups, but found {', '.join([type(i).__name__ for i in layer[:4]])}."
        assert all(len(i) == 2 for i in layer[:4]), [len(i) for i in layer[:4]]
        assert all(
            isinstance(i, PathElement) for i in layer[4:]
        ), f"Expected PathElements, but found {', '.join([type(i).__name__ for i in layer[4:]])}."
    else:
        assert isinstance(layer[0], Rectangle), type(layer[0]).__name__
        assert isinstance(layer[1], Ellipse), type(layer[1]).__name__
        assert all(
            isinstance(i, PathElement) for i in layer[2:]
        ), f"Expected PathElements, but found {', '.join([type(i).__name__ for i in layer[2:]])}."

    # check styles
    for o in layer[:4]:
        if is_cs:
            assert o[0].style("stroke") is None
            assert o[0].style("fill") == Color((1.0, 1.0, 1.0))
            assert o[1].style("stroke") is None
            assert o[1].style("fill") == Color((0, 0, 0))
            continue
        assert o.style("stroke") == Color((0, 0, 0))
        assert o.style("fill") == Color((1.0, 1.0, 1.0))

    for o in layer[4:]:
        assert o.style("stroke") == Color((0, 0, 0))
        assert o.style("fill") is None
