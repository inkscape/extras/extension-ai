# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test certain documents for the values in them and their parsing."""

from inkai.parser.document import AIDocument
from inkai.parser.objects.layer import Layer
from inkai.parser.setup.procset_exec import ProcsetExec


def test_document_has_header_comments(documents):
    """Check the header fields."""
    document: AIDocument = documents.rgb_rectangle
    comments = document.header_comments
    assert comments.title == "cmyk_rectangle.ai"
    assert comments.file_format == "14.0"
    assert comments.bounding_box == [202, 412, 384, 625]


def test_document_has_setup(documents):
    """Check that the setup contains the right values and is properly parsed."""
    document: AIDocument = documents.rgb_rectangle
    print(document.setup.parsed_content[:100])
    setup = document.setup
    assert len(setup.plugin_group_infos) == 16
    assert len(setup.art_styles) == 1
    assert setup.document_data.parsed_content


def test_document_trailer(documents):
    """Check the trailer."""
    document: AIDocument = documents.rgb_rectangle
    assert document.document_trailer.children == []
    assert document.page_trailer.children == []
    assert document.page_trailer.raw_content == "gsave annotatepage grestore showpage"


def test_procset_in_trailer(documents):
    """Check that the procsets are in the trailer."""
    document: AIDocument = documents.inkscape_minimized_ai3
    trailer = document.document_trailer
    assert len(trailer.children) == 5
    assert all(isinstance(procset, ProcsetExec) for procset in trailer.children)


def test_objects_in_the_document(documents):
    """Check if we actually see the layer."""
    document: AIDocument = documents.rgb_rectangle
    assert len(document.objects) == 1
    layer = document.objects[0]
    assert isinstance(layer, Layer)
