# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test parsing gradient definitions in the setup part of the document.

See AI Spec page 38+
"""

import pytest
from inkai.parser.setup.gradient import Bn, Bd, Gradient, Br, Bs, BrBs
from inkai.parser.setup import Setup
from inkai.parser.lazy import UnidentifiedLine
from inkai.parser.document import AIDocument


def parse_with_setup(operator):
    """Check setup and direct parsing."""
    return pytest.mark.parametrize(
        "parse",
        [
            operator.from_string,
            lambda s: Setup.from_string(f"%%BeginSetup\n{s}\n%%EndSetup\n").children[0],
        ],
    )


@pytest.mark.parametrize("n", [1, 8])
@parse_with_setup(Bn)
def test_Bn(n, parse):
    """Parse Bn"""
    bn = parse(f"{n} Bn")
    print(bn)
    assert bn.nblends == n


@pytest.mark.parametrize("name", ["(Golden Ring)", "(Red & Yellow)"])
@parse_with_setup(Gradient)
def test_name_of_gradient(name, parse):
    """Check the name."""
    gradient: Gradient = parse(f"%AI5_BeginGradient: {name}\n%AI5_EndGradient")
    assert gradient.name == name[1:-1]


def parse_with_gradient(operator):
    """Check Gradient and direct parsing."""
    return pytest.mark.parametrize(
        "parse",
        [
            operator.from_string,
            lambda s: Gradient.from_string(
                f"%AI5_BeginGradient: (test)\n{s}\n%AI5_EndGradient\n"
            ).children[0],
        ],
    )


@pytest.mark.parametrize(
    "name,type,colors",
    [
        ("name", 0, 10),
        ("name 2", 1, 2),
        ("name 3", 0, 2),
        ("lalaal", 1, 1),
    ],
)
@parse_with_gradient(Bd)
def test_Bd(parse, name, type, colors):
    """Test the Bd operator."""
    bd = parse(f"({name}) {type} {colors} Bd")
    assert bd.name == name
    assert bd.nColors == colors
    assert bd.type == type
    assert bd.is_radial() == (type == 1)
    assert bd.is_linear() == (type == 0)


BR_WITH_HEX_DATA = """[
0
<
000000000202040305050606070709090A0A090B0A0C0C0E0E101012111112121313151516161617
1719191A1A1A1B1B1C1C1E1E1D1F1F202021212122222323232424252527272729292A2A2A2C2B2B
2D2D2E2E2E2F2F313130323233333334343436363537373838383938383A3A393B3B3C3C3C3D3D3D
3E3E3E4040404141414242424444444545454747474848484A49494B4B4A4C4C4C4D4D4D4D4E4E4E
4F4F4F505050515151515252525353535354545456565656575757585858585A5959595B5B5B5B5C
5C5C5D5D5D5D5E5E5E5E5F5F5F5F6161606061616161626262626264646464666565656767676766
6868686869696969696A6A6A6A6A6B6B6B6B6B6D6D6D6C6C6E6E6E6E6E6F6F6F6F6F707070707070
71717171717173737272727474747474747475757575757476767676767676777777777777787878
7878787878797979797878787979797979797979797A7A7A7A7A7A7A7A7C7B7B7B7B7B7B7B7B7B7D
7D7D7D7D7D7D7D7D7D7E7E7E7E7E7E7E7E7E7E7E7E7F7F7F7F7F7F7F7F7F7F7F7F7F7F8181818181
80808080808080808080808081818181818181818181818181818181818181818181818181828282
8282828282828282828282828282828282828282828282828282828282828282828282
>
<
0000010304050608090A0B0D0E0F1012121315161718181A1B1C1D1E1F2121232325252727272929
2B2B2D2E2F3031313234343638383A3B3B3B3D3E3F404141424344454748484A4A4C4D4E4E4F5052
5252535455565857585A5B5C5C5D5F5F5F61616264636465676767696A6A6B6B6C6C6D6E6F6F7071
707173747476777778797A7A7B7D7D7D7F7F8081828283848485868687888889898A8B8B8C8D8D8F
8F8F909191939293959595959697979999999A9A9B9B9D9E9D9E9E9FA0A0A1A1A2A2A2A2A3A3A3A4
A4A5A4A6A6A7A6A7A9A9AAAAABABACACADADAEAEAFAFB0B0B2B2B3B3B4B4B5B5B5B5B5B6B6B7B7B8
B8B9B9BAB9BABABABCBBBDBDBEBEBEBFBFC0C0C1C1C1C2C2C3C3C3C4C4C5C5C5C5C5C6C6C6C7C7C7
C7C7C8C8C8C9C8C8CACACACACACACCCCCDCDCDCECECECED0CFCFD0D0D0D1D1D1D2D2D2D2D2D2D2D3
D3D3D4D4D4D4D4D4D4D5D5D5D5D6D6D6D6D6D6D6D6D7D7D7D7D8D8D8D8D8D9D9D9D9DADADADADADB
DBDBDBDBDCDCDCDCDCDCDCDCDCDCDDDDDDDDDDDDDEDDDDDDDDDDDEDEDEDEDEDEDEDEDEDEDEDEDEDE
DEDEDEDEDEDEDEDEDEDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFDFE0E0E0E0E0E0E0E0E0E0
E0E0E0E0E0E0E0E0E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1
>
0
1
<
FFFFFEFEFDFDFCFCFBFBFAFAF9F9F8F8F7F7F7F6F6F5F5F4F4F3F3F2F2F2F1F1F0F0EFEFEEEEEEED
EDECECEBEBEBEAEAE9E9E8E8E8E7E7E6E6E5E5E5E4E4E3E3E3E2E2E1E1E0E0E0DFDFDEDEDEDDDDDD
DCDCDBDBDBDADAD9D9D9D8D8D7D7D7D6D6D6D5D5D5D4D4D3D3D3D2D2D2D1D1D1D0D0CFCFCFCECECE
CDCDCDCCCCCCCBCBCBCACACAC9C9C9C8C8C8C7C7C7C6C6C6C5C5C5C4C4C4C3C3C3C2C2C2C2C1C1C1
C0C0C0BFBFBFBEBEBEBEBDBDBDBCBCBCBCBBBBBBBABABABAB9B9B9B8B8B8B8B7B7B7B7B6B6B6B6B5
B5B5B4B4B4B4B3B3B3B3B2B2B2B2B1B1B1B1B0B0B0B0AFAFAFAFAFAEAEAEAEADADADADACACACACAC
ABABABABAAAAAAAAAAA9A9A9A9A9A8A8A8A8A8A7A7A7A7A7A6A6A6A6A6A5A5A5A5A5A4A4A4A4A4A4
A3A3A3A3A3A3A2A2A2A2A2A1A1A1A1A1A1A1A0A0A0A0A0A09F9F9F9F9F9F9F9E9E9E9E9E9E9D9D9D
9D9D9D9D9D9C9C9C9C9C9C9C9B9B9B9B9B9B9B9B9B9A9A9A9A9A9A9A9A9999999999999999999998
98989898989898989897979797979797979797979796969696969696969696969696969595959595
95959595959595959595959594949494949494949494949494949494949494949494949494939393
9393939393939393939393939393939393939393939393939393939393939393939393
>
<
FFFEFDFCFBFAF9F8F7F6F5F4F3F2F1F0EFEEEDECEBEAEAE9E8E7E6E5E4E3E2E1E0DFDEDDDCDCDBDA
D9D8D7D6D5D4D3D3D2D1D0CFCECDCCCBCBCAC9C8C7C6C5C5C4C3C2C1C0BFBFBEBDBCBBBABAB9B8B7
B6B6B5B4B3B2B1B1B0AFAEADADACABAAAAA9A8A7A6A6A5A4A3A3A2A1A0A09F9E9D9D9C9B9A9A9998
9897969595949393929190908F8E8E8D8C8C8B8A898988878786858584838382828180807F7E7E7D
7C7C7B7A7A79797877777676757474737372717170706F6E6E6D6D6C6B6B6A6A6969686867666665
656464636362626160605F5F5E5E5D5D5C5C5B5B5A5A595958585757565655555454545353525251
5150504F4F4E4E4E4D4D4C4C4B4B4B4A4A4949484848474746464645454444444343424242414141
40403F3F3F3E3E3E3D3D3D3C3C3C3B3B3A3A3A393939393838383737373636363535353434343433
333332323232313131303030302F2F2F2F2E2E2E2E2D2D2D2D2C2C2C2C2C2B2B2B2B2A2A2A2A2A29
29292929282828282827272727272626262626262525252525252424242424242423232323232323
2222222222222222222121212121212121212020202020202020202020201F1F1F1F1F1F1F1F1F1F
1F1F1F1F1F1F1F1F1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E1E
>
4 %_Br
"""

BR_SIMPLE = """[
0
0.654597
0.033906
0
1
0.482353
0.67451
4 %_Br
"""


def test_simple_Br():
    """Check that the"""


@pytest.mark.parametrize(
    "string,type",
    [
        (BR_SIMPLE, Br),
        (BR_WITH_HEX_DATA, Br),
        ("[\n0 0.654597 0.033906 0 1 0.482353 0.67451 2 50 100 %_BS", Bs),
        ("[\n%_0 0 0 0 1 1 1 2 28 0 Bs", Bs),
    ],
)
@parse_with_gradient(BrBs)
def test_br_and_bs_choice(string, type, parse):
    """Check that we ignore lines that are not Br and uselessly empty in front if BS."""
    element = parse(string)
    assert isinstance(element, type), element


@pytest.mark.parametrize(
    "string",
    [
        ("0 0.654597 0.033906 0 1 0.482353 0.67451 2 50 100 %_BS"),
        ("%_0 0 0 0 1 1 1 2 28 0 Bs"),
    ],
)
@parse_with_gradient(Bs)
def test_Bs_one_line_command(string, parse):
    """Check that we ignore lines that are not Br and uselessly empty in front if BS."""
    bs = parse(string)
    assert isinstance(bs, Bs)


BS_SPECS = [
    "{} %_BS",  # used in simple_v8.ai
    "%_{} Bs",  # used in simple_v8.ai
    "{} %_Bs",  # AI Spec page 41
]


@pytest.mark.parametrize("bs", BS_SPECS)
@pytest.mark.parametrize(
    "note,string,gray,rgb,cmyk,name,alpha,mid_point,ramp_point",
    [
        ("cmyk", "0 1 0.6 0 1 50 100", [], [], [0, 1, 0.6, 0], "", 1, 50, 100),
        ("gray", "1 0 20 30", [1], [], [], "", 1, 20, 30),
        ("gray", "0.1 0 20 30", [0.1], [], [], "", 1, 20, 30),
        (
            "rgb",
            "0.1 0.2 0.3 0.4 0.5 0.6 0.7 2 1 2",
            [],
            [0.5, 0.6, 0.7],
            [0.1, 0.2, 0.3, 0.4],
            "",
            1,
            1,
            2,
        ),
        (
            "rgb custom",
            "0.11 0.21 0.31 0.41 0.51 0.61 0.71 (color) 0.1 1 4 22 23",
            [],
            [0.51, 0.61, 0.71],
            [0.11, 0.21, 0.31, 0.41],
            "color",
            0.9,
            22,
            23,
        ),
        (
            "cmyk custom",
            "0.13 0.23 0.33 0.43 (custom cmyk) 0.3 3 10 33",
            [],
            [],
            [0.13, 0.23, 0.33, 0.43],
            "custom cmyk",
            0.7,
            10,
            33,
        ),
        (
            "colortype=5",
            "0.11 0.21 0.31 0.41 0.51 0.61 0.71 (color) 0.1 1 5 22 23",
            [],
            [0.51, 0.61, 0.71],
            [0.11, 0.21, 0.31, 0.41],
            "color",
            0.9,
            22,
            23,
        ),
    ],
)
@pytest.mark.parametrize(
    "subtype_6_modification,new_alpha",
    [
        (False, 0),
        (True, 1),
        (True, 0.5),
    ],
)
def test_color_stop_values(
    bs,
    note,
    string,
    gray,
    rgb,
    cmyk,
    name,
    alpha,
    mid_point,
    ramp_point,
    subtype_6_modification,
    new_alpha,
):
    """Check the color value of the BS command."""
    if subtype_6_modification:
        # this command gets modified for the subtype 6
        # that specifies the tint value additionally
        alpha = new_alpha
        split_string = string.split()
        # inject the subtype and tint
        split_string = split_string[:-2] + [str(1 - alpha), "6"] + split_string[-2:]
        string = " ".join(split_string)
        print("modified for type 6: " + string)
    ai_string = bs.format(string)
    print(f"{note}: {ai_string}")
    bs: Bs = Bs.from_string(ai_string)
    print("tokens:", bs.tokens)
    print(f"color_style: {bs.color_style}")
    assert bs.gray == gray
    assert bs.rgb == rgb
    assert bs.cmyk == cmyk
    assert bs.name == name
    assert bs.alpha == alpha
    assert bs.mid_point == mid_point
    assert bs.ramp_point == ramp_point


@pytest.mark.parametrize(
    "string,rgb",
    [
        ("1 0 20 30 Bs", "#000000"),
        ("0 0.3 0.4 0.5 1 20 30 Bs", "#80594C"),
        ("0 0 0 0 1 0.5 0 2 20 30 Bs", "#FF8000"),
    ],
)
def test_bs_to_rgb_string(string, rgb):
    """Convert color stops to RGB strings."""
    bs: Bs = Bs.from_string(string)
    print(bs.tokens)
    print(string, bs.rgb, bs.cmyk, bs.gray, bs.color_style)
    assert bs.rgb_string == rgb


@parse_with_gradient(BrBs)
def test_with_bracket(parse):
    """The Bs commands can start with a bracket."""
    string = "1 0 20 30 Bs"
    bs1 = Bs.from_string(string)
    bs2 = parse("[\n" + string)
    print(bs1, bs2)
    assert bs1.tokens == bs2.tokens


GRADIENT_EXAMPLE_AI_SPEC_PAGE_42 = """%AI5_BeginGradient: (White & Purple Radial)
(White & Purple Radial) 1 2 Bd
[
0.55 1 0 0 1 50 10 %_Bs
0 0 0 0 1 50 100 %_Bs
BD
%AI5_EndGradient"""

GRADIENT_EXAMPLE_SIMPLE_V8 = """%AI5_BeginGradient: (Summer)
(Summer) 1 2 Bd
[
0 0 0 0 1 1 1 2 28 0 %_BS
%_0 0 0 0 1 1 1 2 28 0 Bs
0 0.510887 0.881346 0 1 0.576471 0.117647 2 50 100 %_BS
%_0 0.510887 0.881346 0 1 0.576471 0.117647 2 50 100 Bs
BD
%AI5_EndGradient
"""

GRADIENT_EXAMPLE_SIMPLE_V8_REVERSED_ORDER = """%AI5_BeginGradient: (Summer)
(Summer) 1 2 Bd
[
%_0 0 0 0 1 1 1 2 28 0 Bs
0 0 0 0 1 1 1 2 28 0 %_BS
%_0 0.510887 0.881346 0 1 0.576471 0.117647 2 50 100 Bs
0 0.510887 0.881346 0 1 0.576471 0.117647 2 50 100 %_BS
BD
%AI5_EndGradient
"""


@pytest.mark.parametrize(
    "string,ramp_points",
    [
        (GRADIENT_EXAMPLE_AI_SPEC_PAGE_42, [10, 100]),
        (GRADIENT_EXAMPLE_SIMPLE_V8, [0, 100]),
        (GRADIENT_EXAMPLE_SIMPLE_V8_REVERSED_ORDER, [0, 100]),
    ],
)
def test_color_stops_of_gradients(string, ramp_points):
    """Test the access to the color stops."""
    gradient: Gradient = Gradient.from_string(string)
    assert len(gradient.color_stops) == len(ramp_points)
    for color_stop, ramp_point in zip(gradient.color_stops, ramp_points):
        color_stop: Bs
        assert color_stop.ramp_point == ramp_point


@pytest.mark.parametrize(
    "string,name",
    [
        (GRADIENT_EXAMPLE_AI_SPEC_PAGE_42, "White & Purple Radial"),
        (GRADIENT_EXAMPLE_SIMPLE_V8, "Summer"),
    ],
)
def test_gradient_definition(string, name):
    """Check the fast access to the definition."""
    gradient: Gradient = Gradient.from_string(string)
    assert isinstance(gradient.definition, Bd)
    assert gradient.definition.name == name


@pytest.mark.parametrize(
    "string",
    [
        GRADIENT_EXAMPLE_AI_SPEC_PAGE_42,
        GRADIENT_EXAMPLE_SIMPLE_V8,
    ],
)
def test_gradient_is_fully_parsed(string):
    """We know all commands inside."""
    gradient: Gradient = Gradient.from_string(string)
    assert gradient.categories[UnidentifiedLine] == []


def test_access_gradients_from_document(documents):
    """We would like to access the gradients in an easy way."""
    doc: AIDocument = documents.simple_v8
    assert "Summer" in doc.setup.gradients
    summer = doc.setup.gradients["Summer"]
    assert summer.color_stops[0].ramp_point == 0
    assert summer.color_stops[1].ramp_point == 100


def test_Bs_is_preferred_over_BS(documents):
    """Bs takes precedence over %_BS."""
    doc: AIDocument = documents.simple_v8
    assert "Summer" in doc.setup.gradients
    summer = doc.setup.gradients["Summer"]
    for color_stop in summer.color_stops:
        assert color_stop.first_line.split()[-1] == "Bs"
