# SPDX-FileCopyrightText: 2023 Jonathan Neuhauser <jonathan.neuhauser@outlook.com>
#
# SPDX-License-Identifier: GPL-2.0-or-later

from io import BytesIO
import pytest
from inkai.parser.streamreader import StreamWrapper
from inkai.parser.text import TextDocumentParser

from pyparsing import (
    ParseResults,
    ParserElement,
    Regex,
    Forward,
    ZeroOrMore,
    QuotedString,
    Literal,
    pyparsing_common,
    Suppress,
)


class PyparsingTextDocumentParser:
    """This parser is the reference implementation for text parsing.
    It's slow, so we only use it for unit testing."""

    def __init__(self):
        ParserElement.set_default_whitespace_chars("\r ")
        self.flag = Regex(r"/(\d\d?|[\w\d]+)").add_parse_action(
            TextElementObjectification.parse_flag
        )
        object = Forward()
        value = Forward()
        self.list = (
            Suppress("[") + ZeroOrMore(value) + Suppress("]")
        ).add_parse_action(TextElementObjectification.parse_list)
        self.subobject = (Suppress("<<") + object + Suppress(">>")).add_parse_action(
            TextElementObjectification.parse_subobject
        )
        self.text = QuotedString(
            "(", end_quote_char=")", esc_char="\\", multiline=True
        ).add_parse_action(TextElementObjectification.parse_text)
        self.datatype = Regex(r"/[\w\d]+")
        value <<= (
            (pyparsing_common.number)
            | self.text
            | self.datatype
            | self.subobject
            | self.list
            | ((Literal("false") | "true")).add_parse_action(lambda x: x[0] == "true")
        )
        object <<= ZeroOrMore(self.flag + value)
        self.textdocument = (Suppress(Regex(r"\s*")) + object).add_parse_action(
            TextElementObjectification.parse_subobject
        )
        self.object = object
        self.value = value


class TextElementObjectification:
    @staticmethod
    def parse_flag(p: ParseResults):
        return p[0]

    @staticmethod
    def parse_subobject(p: ParseResults):
        """Process a parsed object into a dictionary"""
        it = iter(p)
        result = {}
        for x in it:
            result[x] = next(it)
        return result

    @staticmethod
    def parse_textdocument(p: ParseResults):
        return TextElementObjectification.parse_subobject(p)

    @staticmethod
    def parse_list(p: ParseResults):
        return [list(p)]

    @staticmethod
    def parse_text(p: ParseResults):
        """Encode parsed text back to bytes."""
        return p


@pytest.fixture()
def textdoc(data) -> bytes:
    return data.grammar["extracted_text_document"].read(binary=True)


@pytest.fixture()
def reference_result(textdoc):
    parser = PyparsingTextDocumentParser()
    return parser.textdocument.parse_string(textdoc.decode("latin-1"), parse_all=True)[
        0
    ]


@pytest.fixture()
def parse_result(textdoc):
    return TextDocumentParser.read_object(StreamWrapper(BytesIO(textdoc)))


def test_parser_completes(parse_result):
    """Check that the implemented parser completes."""
    assert len(parse_result) == 3
    assert "/1" in parse_result
    assert len(parse_result["/1"]) == 4


def test_reference_parser(reference_result):
    """Check that the reference parser completes."""
    assert len(reference_result) == 3
    assert "/1" in reference_result
    assert len(reference_result["/1"]) == 4


def test_compare_parsers(reference_result, parse_result):
    """Check that both parsers give the same result."""

    def dict_compare(this, other):
        assert isinstance(other, dict)
        assert isinstance(this, dict)
        assert len(this) == len(other)
        assert set(this.keys()) == set(other.keys())
        for key in this:
            value = this[key]
            otherval = other[key]
            value_compare(value, otherval)

    def list_compare(this, other):
        assert isinstance(other, list)
        assert isinstance(this, list)
        assert len(this) == len(other)
        for value, otherval in zip(this, other):
            value_compare(value, otherval)

    def value_compare(value, otherval):
        if isinstance(value, dict):
            dict_compare(value, otherval)  # compare dicts
        elif isinstance(value, list):
            list_compare(value, otherval)
        else:
            # strings might differ (pyparsing removes escape chars)
            if not (isinstance(value, str)):
                assert value == otherval

    dict_compare(reference_result, parse_result)
