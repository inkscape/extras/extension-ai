# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This tests the layer and the elements in it."""

from inkai.parser.objects.layer import Layer


def test_objects_in_layer(documents):
    """Check the content of the layer."""
    print(documents.rgb_rectangle.objects)
    layer: Layer = documents.rgb_rectangle.first(Layer)
    print(layer.parsed_content)
    print(layer.categories)
    assert layer.name == "Layer 1"
    assert layer.id == "Layer_1"


def test_layer_contains_objects(documents):
    """Make sure we find the content in the layer."""
    layer: Layer = documents.rgb_rectangle.first(Layer)
    assert layer.children == layer.parsed_content


RECURSIVE_LAYERS = """%AI5_BeginLayer
(1) Ln
%AI5_BeginLayer
(1.1) Ln
%AI5_BeginLayer
(1.1.1) Ln
%AI5_EndLayer--
%AI5_BeginLayer
(1.1.2) Ln
%AI5_EndLayer--
%AI5_EndLayer--
%AI5_BeginLayer
(1.2) Ln
%AI5_EndLayer--
%AI5_EndLayer--
"""


def test_layer_in_layer():
    """Check recursive layers."""
    layer_1: Layer = Layer.from_string(RECURSIVE_LAYERS)
    assert layer_1.name == "1"
    assert len(layer_1.categories[Layer]) == 2
    layer_1_1 = layer_1.categories[Layer][0]
    assert layer_1_1.name == "1.1"
    assert len(layer_1_1.categories[Layer]) == 2
    layer_1_1_1 = layer_1_1.categories[Layer][0]
    assert layer_1_1_1.name == "1.1.1"
    assert len(layer_1_1_1.categories[Layer]) == 0
    layer_1_1_2 = layer_1_1.categories[Layer][1]
    assert layer_1_1_2.name == "1.1.2"
    assert len(layer_1_1_2.categories[Layer]) == 0
    layer_1_2 = layer_1.categories[Layer][1]
    assert layer_1_2.name == "1.2"
    assert len(layer_1_2.categories[Layer]) == 0
