# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Tests for the interfaces."""

import pytest
from inkai.parser.lazy import (
    ParsingSpecificationInterface,
    AIElement,
    CollectedSection,
    EndOfLine,
    StartOfLine,
)


class X(AIElement):
    def __init__(self):
        pass


class Y(ParsingSpecificationInterface):
    pass


class Z:
    def add_to(self, parser):
        pass


@pytest.mark.parametrize("o", [X, Y()])
def test_isinstance(o):
    """Check that this adhered to the interface."""
    assert ParsingSpecificationInterface.is_spec(o)


@pytest.mark.parametrize("o", [object(), Z, Z(), Y, X()])
def test_not_isinstance(o):
    """Check this is not the interface."""
    assert not ParsingSpecificationInterface.is_spec(o)


def test_parser_is_cached():
    """Check that the parser is cached.

    This makes it faster to parse sections.
    """
    assert CollectedSection.get_parser() is CollectedSection.get_parser()


def test_until_of_end():
    assert EndOfLine("asd", "x").until("asd")
    assert EndOfLine("asd", "x").until("1 asd")
    assert EndOfLine("asd", "x").until("2 x")
    assert not EndOfLine("asd", "x").until("asd 1")


def test_until_of_start():
    assert StartOfLine("asd", "x").until("asd")
    assert StartOfLine("asd", "x").until("asd 1")
    assert StartOfLine("asd", "x").until("x 2")
    assert not StartOfLine("asd", "x").until("1 asd")
