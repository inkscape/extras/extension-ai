# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later
"""Test the parsing of AI11Texts and their connected information

- document.setup.text_document
- document.objects -> category[AI11Text]

This is reverse-engineered.
"""
import pytest
from inkai.parser.consts import AIDocument
from inkai.parser.fileparser import FileParser
from tests.conftest import parse_string
from inkai.parser.operators import Operator

SIMPLE_AI_11 = """/AI11Text :
0 /FreeUndo ,
123 /FrameIndex ,
33 /StoryIndex ,
2 /TextAntialiasing ,
;

"""

COMPLEX_TEXT_AI_11 = f"""/AI11Text :
0 /FreeUndo ,
0 /FrameIndex ,
3 /StoryIndex ,
/Art :
X=
1 Ap
8 As
976 -667 m
694 -667 l
n
%_/ArtDictionary :
%_;
%_
X+
; /ConfiningPath ,
2 /TextAntialiasing ,
;

"""


@pytest.mark.parametrize(
    "text,expected_value",
    [
        (
            SIMPLE_AI_11,
            {
                "FreeUndo": 0,
                "FrameIndex": 123,
                "StoryIndex": 33,
                "TextAntialiasing": 2,
                "ConfiningPath": None,
                "_type": "AI11Text",
            },
        ),
        (
            COMPLEX_TEXT_AI_11,
            {
                "FreeUndo": 0,
                "FrameIndex": 0,
                "StoryIndex": 3,
                "ConfiningPath": None,
                "TextAntialiasing": 2,
                "_type": "AI11Text",
            },
        ),
    ],
)
def test_parse_string_only_with_data_hierarchy_parser(text, expected_value):
    """Make sure that the DataHierarchyParser understands the text."""
    ai11_text = parse_string(text, FileParser.read_many_objects)[0]
    print(ai11_text)
    print(expected_value)
    if "/Art" in text:
        cp = ai11_text.ConfiningPath
        ai11_text.ConfiningPath = None
        # There needs to be an art dictionary in there, as well as 7 commands
        assert sum(1 for i in cp.Data if isinstance(i, Operator)) == 7
        assert cp.Data[6]["_type"] == "ArtDictionary"
    assert ai11_text == expected_value


@pytest.mark.parametrize(
    "document",
    ["simple_v2020", "simple_cs"],
)
def test_parse_document(documents, document):
    """Access the text definitions."""
    document: AIDocument = documents[document]
    text_document = document.setup.text_document
    story = text_document.DocumentObjects.TextObjects[0]
    assert "Hellow mom\r" == story.text, '["/0"]["/0"]'
