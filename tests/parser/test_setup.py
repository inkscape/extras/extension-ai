# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test the grammar of the Adobe Illustrator format for the lazy parser."""


from inkai.parser.consts import Setup
from inkai.parser.fileparser import FileParser
from tests.conftest import parse_string


SETUP_PGI_1 = """%%BeginSetup
%AI5_Begin_NonPrinting
Np
%AI8_PluginGroupInfo
(Adobe Vectorized Object) (Image Tracing) (Vectorize.aip)
%AI5_End_NonPrinting--
%%EndSetup
"""

ARTSTYLE_1_CONTENT = """/KnownStyle :
([Default]) /Name ,
/SimpleStyle :
0 O
0 0 0 0 1 1 1 Xa
0 R
0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 0 0 0 XA
0 1 0 0 0 Xy
0 J 0 j 1 w 10 M []0 d
0 XR
1 1 Xd
/Paint ;
 /Def ;"""

ARTSTYLE_1 = f"""%%BeginSetup
%AI9_BeginArtStyles
{ARTSTYLE_1_CONTENT}
%AI9_EndArtStyles
%%EndSetup
"""

EMPTY_SETUP = "%%BeginSetup\n%%EndSetup\n"

PALETTE_SETUP = """%%BeginSetup
%AI5_BeginPalette
0 0 Pb
0.749721467494965 0.679194271564484 0.670496642589569 0.901457190513611 0.000136697562994 0.000626976019703 0.000423457007855 ([Registration]) 0 1 Xz
([Registration])
Pc
PB
%AI5_EndPalette
%%EndSetup
"""


def test_parser_default_attributes():
    """Empty setup attributes."""
    setup = parse_string(EMPTY_SETUP, FileParser.read_setup)
    assert setup.palettes == {}
    assert setup.art_styles == {}
    assert len(setup.children) == 0
    assert len(setup.arguments) == 0


def test_plugin_group_infos():
    """Check the plugin infos."""
    setup = parse_string(SETUP_PGI_1, FileParser.read_setup)
    assert len(setup.children[0].children) == 2
    pg = setup.children[0].children[1]
    assert pg[1] == ["Adobe Vectorized Object", "Image Tracing", "Vectorize.aip"]


def test_art_styles():
    """ "Check the art styles."""
    setup = parse_string(ARTSTYLE_1, FileParser.read_setup)
    assert "[Default]" in setup.art_styles


def test_palettes():
    """Check that all palettes are included."""
    setup: Setup = parse_string(PALETTE_SETUP, FileParser.read_setup)
    assert len(setup.palettes) == 1
    assert "[Registration]" in setup.palettes


PROCSET_SETUP_AI3 = """%%BeginSetup
Adobe_cmykcolor /initialize get exec
Adobe_cshow /initialize get exec
Adobe_customcolor /initialize get exec
Adobe_IllustratorA_AI3 /initialize get exec
%%EndSetup
"""


def test_procset_exec():
    """Check that the procset is recognized."""
    setup = parse_string(PROCSET_SETUP_AI3, FileParser.read_setup)
    assert len(setup.children) == 4


def test_prolog_nostart():
    """Some files have %%EndProlog without %%BeginProlog."""

    data = """%!PS-Adobe-3.0
%%DocumentProcSets: Adobe_Illustrator_1.1 0 0
%%BoundingBox: 0 0 799 599
%%HiResBoundingBox: 0 0 799 599
%AI3_Cropmarks: 0 0 799 599
%%DocumentPreview: None
%%EndComments
%%EndProlog
%%BeginSetup
Adobe_Illustrator_1.1 begin
n
%%EndSetup"""
    setup = parse_string(data, FileParser.read_document)

    assert setup.prolog
    assert not setup.prolog.children


def test_no_endcomments():
    """Some files are missing the %%EndComments statement"""
    data = """%!PS-Adobe-3.0
%%Creator:Adobe Illustrator (TM) 15.000000 Exported from CorelDRAW 2020 (64-Bit)
%%CreationDate:
%%Canvassize:16383
%%BoundingBox:163 205 452 590
%%DocumentProcessColors: Cyan Magenta Yellow Black
%AI3_ColorUsage: Color
%%BeginProlog
%%EndProlog
%%BeginSetup
%%EndSetup"""
    setup = parse_string(data, FileParser.read_document)

    assert setup.prolog
    assert not setup.prolog.children
