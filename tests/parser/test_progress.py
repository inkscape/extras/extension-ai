# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""This checks all AI files in the project if they can be used 100%.

These tests operate on module scope.
"""
import pytest


def test_there_are_no_duplicated_file_names(duplicated_files):
    """We want to make sure that we do not forget a file."""
    assert (
        not duplicated_files
    ), f"There are two files with these names: {', '.join(duplicated_files)}"


def test_1_process_document_throgh_all_steps(process, processing_step):
    """Walk though processing the files step by step to see where we are at."""
    print(process)
    there_was_an_error_in_this_step = False
    try:
        process.run(processing_step)
    except:
        # Catch the error to check if we know that this is expected at this point.
        there_was_an_error_in_this_step = True
    print(process)
    expected_steps = process.expected_step_to_get_stuck_at
    assert (
        expected_steps != -1
    ), f"{process.name} is not recorded, yet. Please update EXPECTED_STATUS by running all tests and copying it in from the last test."
    assert (
        expected_steps >= process.steps
    ), f"Congratulations! You made more files work. Please update the EXPECTED_STATUS to include {process.name}."
    assert (
        expected_steps <= process.steps
        or process.processing_steps - 1 != processing_step
    ), f"Your changes break compatibility. {process.name} breaks now: {process}"
    if there_was_an_error_in_this_step:
        # only files that are without error should show up as green
        pytest.skip(str(process))


def test_2_print_current_status(last_process):
    """Print the current status."""
    print("Please update conftest.py -> EXPECTED_STATUS")
    assert (
        not last_process.print_current_status_if_it_differs()
    ), "You might only want to run this test with all the other ones."
