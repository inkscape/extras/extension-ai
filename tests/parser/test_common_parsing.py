# SPDX-FileCopyrightText: 2023 Software Freedom Conservancy <info@sfconservancy.org>
#
# SPDX-License-Identifier: GPL-2.0-or-later

"""Test common parser functions."""
from inkai.parser.parse import bool


def test_flag():
    """Test the flag parser."""
    assert bool(b"0") == False
    assert bool(b"1") == True
